/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jebssrps;

import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.DeviceFactory;
import fr.esrf.tangoatk.core.IDevice;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.SettingsManagerProxy;
import java.awt.GridBagConstraints;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import jdoorWrapViewer.JDoorwMsgFrame;

/**
 *
 * @author kramer
 */
public class HscFrame extends javax.swing.JFrame
{

    private String                 hscDevName = null;
    private String                 cellNumber = null;
    private JDoorwMsgFrame         jdoorwLogWindow = null;
    private SrpsChannelsWindow     pscFrame = null;
    
    /**
     * Creates new form HscFrame
     */
    public HscFrame(String[] cellArgs) {
        initComponents();
                
        if (cellArgs.length > 0)
        {
            cellNumber = cellArgs[0];
        }
        else
            cellNumber = javax.swing.JOptionPane.showInputDialog (null, "Please enter Cell number: ");
                
        if (cellNumber == null) // cancel
            System.exit(0);
                
        int cellNb = -1;
                
        try
        {
                cellNb = Integer.parseInt(cellNumber);   
        }
        catch(NumberFormatException nfe)
        {
            JOptionPane.showMessageDialog(null,
                                          "Please enter a Valid cell number",
                                          "Input Exception",
                                          JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
   
        if ((cellNb <1 || cellNb >32) && (cellNb != 37))
        {
            JOptionPane.showMessageDialog(null,
                                          "Please enter a Valid cell number: [1 to 32, or 37]",
                                          "Invalid Cell",
                                          JOptionPane.WARNING_MESSAGE);
            System.exit(-1);
        }
                

        
        String  cellNbStr = String.format("%02d", cellNb);
        if (cellNb == 37)
            JEbsSrPsConstants.HSC_FRAME_SETTINGS_MANAGER = "sys/settings/hsc-chartreuse";
        else
            JEbsSrPsConstants.HSC_FRAME_SETTINGS_MANAGER = "sys/settings/hsc-c"+cellNbStr;

        hscDevName = "srmag/ps-main/c".concat(cellNbStr);
            
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle(" HOT SWAP CUBICLE ");
        
        initHscFrame();
        
        pscFrame = new SrpsChannelsWindow(cellNbStr);
        pscFrame.setTitle("ALL PS Viewer CELL " + cellNbStr);
//        pscFrame.setVisible(false);
//        pscFrame.dispose();
        
        if (JEbsSrPsConstants.JDOOR_WRAPPER_PROXY != null)
        {
            jdoorwLogWindow = new JDoorwMsgFrame(JEbsSrPsConstants.JDOOR_WRAPPER_PROXY);
            jdoorwLogWindow.setTitle("Sequence Log Messages");
        }
        
        GridBagConstraints  gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.insets = new java.awt.Insets(5, 5, 5, 5);
        
        HscPanel  hscp = new HscPanel(hscDevName);
        hscpanelPlaceHolder.add(hscp, gbc);
        
        pack();
    }
    
    
    private void connectToSettingsManager()
    {
        try
        {
            IDevice settingsManagerDevice = DeviceFactory.getInstance().getDevice(JEbsSrPsConstants.HSC_FRAME_SETTINGS_MANAGER);
        }
        catch (Exception ex)
        {
            JEbsSrPsConstants.SMP = null;
            return;
        }
        
        try
        {
            JEbsSrPsConstants.SMP = new SettingsManagerProxy(JEbsSrPsConstants.HSC_FRAME_SETTINGS_MANAGER);
        }
        catch (Exception ex)
        {
            JEbsSrPsConstants.SMP = null;
            return;
        }
//        System.out.println("coucou");
    }
    
    void initHscFrame()
    {
        connectToSettingsManager();

        if (JEbsSrPsConstants.SMP == null)
        {
            smFileNamePlaceHolder.setVisible(false);
            loadMenuItem.setVisible(false);
            saveMenuItem.setVisible(false);
            previewMenuItem.setVisible(false);
            jSeparator1.setVisible(false);
            return;
        }
        // connect and intialize the Settings Manager
        JEbsSrPsConstants.SMP.setErrorHistoryWindow(JEbsSrPsConstants.ERR_H);
        String username = System.getProperty("user.name");
        
        String author;
        if (JEbsSrPsConstants.HSC_FRAME_SETTINGS_MANAGER.toLowerCase().contains("chartreuse"))
            author= "HSC Panel Chartreuse ("+ username + ")";
        else
            author= "HSC Panel cell "+ cellNumber + " (" + username + ")";
        
        //        System.out.println(author);
        JEbsSrPsConstants.SMP.setFileAuthor(author);
        JPanel settingsFilePanel = JEbsSrPsConstants.SMP.getSettingsPanel();
        if (settingsFilePanel != null)
        {
            JEbsSrPsConstants.SMP.settingsPanelHideChild(SettingsManagerProxy.FILE_LABEL);
            //            JEbsSrPsConstants.SMP.settingsPanelHideChild(SettingsManagerProxy.STATUS_BUTTON);
            JEbsSrPsConstants.SMP.settingsPanelHideChild(SettingsManagerProxy.LOAD_BUTTON);
            JEbsSrPsConstants.SMP.settingsPanelHideChild(SettingsManagerProxy.SAVE_BUTTON);
            JEbsSrPsConstants.SMP.settingsPanelHideChild(SettingsManagerProxy.PREVIEW_BUTTON);

            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.fill = java.awt.GridBagConstraints.BOTH;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            gbc.insets = new java.awt.Insets(5, 5, 5, 5);

            smFileNamePlaceHolder.add(settingsFilePanel, gbc);
        }
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        smFileNamePlaceHolder = new javax.swing.JPanel();
        hscpanelPlaceHolder = new javax.swing.JPanel();
        hscMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        loadMenuItem = new javax.swing.JMenuItem();
        saveMenuItem = new javax.swing.JMenuItem();
        previewMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        exitJMenuItem = new javax.swing.JMenuItem();
        viewMenu = new javax.swing.JMenu();
        allPSViewerMenuItem = new javax.swing.JMenuItem();
        seqLogJMenuItem = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        errorsMenuItem = new javax.swing.JMenuItem();
        diagMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        smFileNamePlaceHolder.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Settings File"));
        smFileNamePlaceHolder.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        getContentPane().add(smFileNamePlaceHolder, gridBagConstraints);

        hscpanelPlaceHolder.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
        getContentPane().add(hscpanelPlaceHolder, gridBagConstraints);

        fileMenu.setText("File");

        loadMenuItem.setText("Load  ...");
        loadMenuItem.setToolTipText("");
        loadMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                loadMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(loadMenuItem);

        saveMenuItem.setText("Save  ...");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                saveMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuItem);

        previewMenuItem.setText("Preview ...");
        previewMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                previewMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(previewMenuItem);
        fileMenu.add(jSeparator1);

        exitJMenuItem.setText("Quit");
        exitJMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                exitJMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitJMenuItem);

        hscMenuBar.add(fileMenu);

        viewMenu.setText("View");

        allPSViewerMenuItem.setText("All Channels ...");
        allPSViewerMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                allPSViewerMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(allPSViewerMenuItem);

        seqLogJMenuItem.setText("Sequence Logs ...");
        seqLogJMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                seqLogJMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(seqLogJMenuItem);
        viewMenu.add(jSeparator2);

        errorsMenuItem.setText("Error History ...");
        errorsMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                errorsMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(errorsMenuItem);

        diagMenuItem.setText("Diagnostics...");
        diagMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                diagMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(diagMenuItem);

        hscMenuBar.add(viewMenu);

        setJMenuBar(hscMenuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitJMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitJMenuItemActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_exitJMenuItemActionPerformed

    private void errorsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_errorsMenuItemActionPerformed
        // TODO add your handling code here:
        ATKGraphicsUtils.centerFrame(rootPane, JEbsSrPsConstants.ERR_H);
        JEbsSrPsConstants.ERR_H.setVisible(true);
    }//GEN-LAST:event_errorsMenuItemActionPerformed

    private void diagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diagMenuItemActionPerformed
        // TODO add your handling code here:
        fr.esrf.tangoatk.widget.util.ATKDiagnostic.showDiagnostic();
    }//GEN-LAST:event_diagMenuItemActionPerformed

    private void loadMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_loadMenuItemActionPerformed
    {//GEN-HEADEREND:event_loadMenuItemActionPerformed
        // TODO add your handling code here:
        JEbsSrPsConstants.SMP.loadSettingsFile();

    }//GEN-LAST:event_loadMenuItemActionPerformed

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_saveMenuItemActionPerformed
    {//GEN-HEADEREND:event_saveMenuItemActionPerformed
        // TODO add your handling code here:
        JEbsSrPsConstants.SMP.saveSettingsFile();

    }//GEN-LAST:event_saveMenuItemActionPerformed

    private void previewMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_previewMenuItemActionPerformed
    {//GEN-HEADEREND:event_previewMenuItemActionPerformed
        // TODO add your handling code here:
        JEbsSrPsConstants.SMP.previewSettingsFile();
    }//GEN-LAST:event_previewMenuItemActionPerformed

    private void seqLogJMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_seqLogJMenuItemActionPerformed
    {//GEN-HEADEREND:event_seqLogJMenuItemActionPerformed
        // TODO add your handling code here:
        if (jdoorwLogWindow != null)
        {
            ATKGraphicsUtils.centerFrame(this.getRootPane(), jdoorwLogWindow);
            jdoorwLogWindow.setVisible(true);
        }

    }//GEN-LAST:event_seqLogJMenuItemActionPerformed

    private void allPSViewerMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allPSViewerMenuItemActionPerformed
        // TODO add your handling code here:
        if (pscFrame != null)
        {
            ATKGraphicsUtils.centerFrame(this.getRootPane(), pscFrame);
            pscFrame.setVisible(true);
        }
    }//GEN-LAST:event_allPSViewerMenuItemActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(final String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(HscFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(HscFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(HscFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(HscFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        final String[] cellArgs;
        cellArgs = args;
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new HscFrame(cellArgs).setVisible(true);;
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem allPSViewerMenuItem;
    private javax.swing.JMenuItem diagMenuItem;
    private javax.swing.JMenuItem errorsMenuItem;
    private javax.swing.JMenuItem exitJMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenuBar hscMenuBar;
    private javax.swing.JPanel hscpanelPlaceHolder;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JMenuItem loadMenuItem;
    private javax.swing.JMenuItem previewMenuItem;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JMenuItem seqLogJMenuItem;
    private javax.swing.JPanel smFileNamePlaceHolder;
    private javax.swing.JMenu viewMenu;
    // End of variables declaration//GEN-END:variables
}
