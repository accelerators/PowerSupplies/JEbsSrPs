/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jebssrps;

import fr.esrf.tangoatk.core.ATKException;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.Device;
import fr.esrf.tangoatk.core.ICommand;
import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.jdraw.TangoSynopticHandler;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import java.awt.GridBagConstraints;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.MissingResourceException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.border.TitledBorder;
import viewers.SrpsSynopticFileViewer;

/**
 *
 * @author poncet
 */
public class HscPanel extends javax.swing.JPanel
{

    private String          devName = null;
    private String          cellNbStr = null;
    private String          synopticTemplateFile = null;
    
    private AttributeList   attl = null;
    private CommandList     cmdl;
    private ICommand        onCmd = null, offCmd = null, resetCmd = null, resetVoltageCmd = null;
    private ICommand        racksResetCmd = null;
        
    /**
     * Creates new form HscPanel
     */
    public HscPanel(String  deviceName)
    {
        initComponents();
        
        devName = deviceName;
        int  index = devName.lastIndexOf("/");
        cellNbStr = devName.substring(index + 2);
        
        int  cellNb = -1;
        
        cellNb = Integer.parseInt(cellNbStr);
        
        if (cellNb == 37)
        {
            // Adapt to the selected configuration
            synopticTemplateFile = JEbsSrPsConstants.CHARTREUSE_SYNOPTIC_FILE_NAME;            
        }
        else
        {
            if (cellNb == 3)
            {
                synopticTemplateFile = JEbsSrPsConstants.C3_SYNOPTIC_FILE_NAME;
            }
            else 
                if (cellNb == 4)
                {
                    synopticTemplateFile = JEbsSrPsConstants.C4_SYNOPTIC_FILE_NAME;
                }
                else
                {
                    synopticTemplateFile = JEbsSrPsConstants.EBS_SYNOPTIC_FILE_NAME;
                }
        }
        
        addSynoptic();
        
        
        attl = new AttributeList();
        attl.addErrorListener(JEbsSrPsConstants.ERR_H);
        attl.addSetErrorListener(ErrorPopup.getInstance());
        
        cmdl = new CommandList();
        cmdl.addErrorListener(JEbsSrPsConstants.ERR_H);
        cmdl.addErrorListener(ErrorPopup.getInstance());
        
        connectToCommandsAndAttributes();
        
//        if (!JEbsSrPsConstants.CHARTREUSE_CONFIGURATION)
//            changeBorderTitle();
        
        allPSChannelsOnCommandViewer.setModel(onCmd);
        allPSChannelsOffCommandViewer.setModel(offCmd);
        allPSChannelsResetCommandViewer.setModel(resetCmd);
        if (resetVoltageCmd != null)
            allPSChannelsResetVoltageCommandViewer.setModel(resetVoltageCmd);
        else
            allPSChannelsResetVoltageCommandViewer.setVisible(false);
                
        allRacksResetCommandViewer.setModel(racksResetCmd);
    }
    
    private void changeBorderTitle()
    {  
        TitledBorder tb = (TitledBorder) this.allChannelsJPanel.getBorder();
        String title = tb.getTitle().concat(" Cell ");
        String newTitle = title.concat(cellNbStr);
        
        tb.setTitle(newTitle);
        this.allChannelsJPanel.setBorder(tb);
    }
    
    
    private void addSynoptic()
    {       
        SrpsSynopticFileViewer  synoptViewer = new SrpsSynopticFileViewer(SrpsSynopticFileViewer.TYPE_HSC);
        
        // Load the synoptic file                                
        try
        {
            synoptViewer.setErrorHistoryWindow(jebssrps.JEbsSrPsConstants.ERR_H);
            synoptViewer.setAutoZoom(true);
        }
        catch (Exception setErrwExcept)
        {
            System.out.println("Cannot set Error History Window for Synoptic");
        }

        InputStream jdFileInStream;
        jdFileInStream = jebssrps.JEbsSrPsConstants.class.getClassLoader().getResourceAsStream(synopticTemplateFile);

        if (jdFileInStream == null)
        {
            javax.swing.JOptionPane.showMessageDialog(
                    null, "Failed to get the inputStream for the synoptic file resource \n",
                    "Resource error",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
        
        java.io.InputStreamReader inStrReader = new InputStreamReader(jdFileInStream);
        try
        {
            synoptViewer.loadTemplateSynopticFromStream(inStrReader, cellNbStr);
            synoptViewer.setToolTipMode(TangoSynopticHandler.TOOL_TIP_NAME);
        }
        catch (IOException ioex)
        {
            javax.swing.JOptionPane.showMessageDialog(
                        null, "Cannot find load the synoptic input stream reader.\n"
                        + " Application will abort ...\n"
                        + ioex,
                        "Resource access failed",
                        javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
        catch (MissingResourceException mrEx)
        {
            javax.swing.JOptionPane.showMessageDialog(
                        null, "Cannot parse the synoptic file.\n"
                        + " Application will abort ...\n"
                        + mrEx,
                        "Cannot parse the file",
                        javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }                       
        
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.insets = new java.awt.Insets(5, 5, 5, 5);
        synopticPlaceHolder.add(synoptViewer, gbc);
        
    }
    
    
    private void connectToCommandsAndAttributes() {
        String  msgatt = new String("Cannot connect to the attribute: ");
        String  msgcmd = new String("Cannot connect to command: ");
        String  msg    = null;
        String  entityname = null;
        IEntity oneEntity  = null;
        
        /* Connect to all attributes and commands */        
                
        try
        {
            /* Connect to AllPSChannels commands */
            
            entityname = "srmag/ps-main/c" + cellNbStr + "/On";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand) {
                onCmd = (ICommand) oneEntity;
            }
            
            entityname = "srmag/ps-main/c" + cellNbStr + "/Off";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand) {
                offCmd = (ICommand) oneEntity;
                // Change the device proxy timeout with the PSM_DEV_PROXY_TIMEOUT_MS
//                System.out.println("The timeout for the device "+ offCmd.getDevice().getName() + " changed to = "
//                                                                + JEbsSrPsConstants.PSM_DEV_PROXY_TIMEOUT_MS + " ms");
                Device d = (Device) offCmd.getDevice();
                d.setDevTimeout(JEbsSrPsConstants.PSM_DEV_PROXY_TIMEOUT_MS);
            }
            
            entityname = "srmag/ps-main/c" + cellNbStr + "/Reset";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand) {
                resetCmd = (ICommand) oneEntity;
            }
        }
        catch (Exception e)
        {
            String str = null;
            
            if (e instanceof ATKException) {
                ATKException  atkex = (ATKException) e;
                str = atkex.getDescription();
            } else
                str = e.toString();
            
            javax.swing.JOptionPane.showMessageDialog(
                    null, msg
                    + ".\n\n"
                    + "Connection Exception : " + str,
                    "Connection to " + entityname,
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        }
        
        
        
        try
        {
            /* Connect to AllPSChannels ResetVoltage command and don't poupp error if does not exist */
            
            entityname = "srmag/ps-main/c" + cellNbStr + "/ResetVoltage";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand) {
                resetVoltageCmd = (ICommand) oneEntity;
            }
        }
        catch (Exception e) {}
                
                
        try
        {
            /* Connect to All PS Racks commands */
            entityname = "srmag/ps-conv-rack/c" + cellNbStr + "/Reset";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand) {
                racksResetCmd = (ICommand) oneEntity;
            }
        }
        catch (Exception e) {
            String str = null;
            
            if (e instanceof ATKException) {
                ATKException  atkex = (ATKException) e;
                str = atkex.getDescription();
            } else
                str = e.toString();
            
            javax.swing.JOptionPane.showMessageDialog(
                    null, msg
                    + ".\n\n"
                    + "Connection Exception : " + str,
                    "Connection to " + entityname,
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        synopticPlaceHolder = new javax.swing.JPanel();
        allChannelsJPanel = new javax.swing.JPanel();
        allChannelsCmdsJPanel = new javax.swing.JPanel();
        allPSChannelsOnCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
        allPSChannelsOffCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
        allPSChannelsResetCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
        allPSChannelsResetVoltageCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
        allRacksJPanel = new javax.swing.JPanel();
        allRacksCmdsJPanel = new javax.swing.JPanel();
        allRacksResetCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();

        setLayout(new java.awt.GridBagLayout());

        synopticPlaceHolder.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(synopticPlaceHolder, gridBagConstraints);

        allChannelsJPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "All  Channels"));
        allChannelsJPanel.setLayout(new java.awt.GridBagLayout());

        allChannelsCmdsJPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 9, 5, 9);
        allChannelsCmdsJPanel.add(allPSChannelsOnCommandViewer, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 9, 5, 9);
        allChannelsCmdsJPanel.add(allPSChannelsOffCommandViewer, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 9, 5, 9);
        allChannelsCmdsJPanel.add(allPSChannelsResetCommandViewer, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.insets = new java.awt.Insets(5, 9, 5, 9);
        allChannelsCmdsJPanel.add(allPSChannelsResetVoltageCommandViewer, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        allChannelsJPanel.add(allChannelsCmdsJPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(allChannelsJPanel, gridBagConstraints);

        allRacksJPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "All  Racks"));
        allRacksJPanel.setLayout(new java.awt.GridBagLayout());

        allRacksCmdsJPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 9, 5, 9);
        allRacksCmdsJPanel.add(allRacksResetCommandViewer, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        allRacksJPanel.add(allRacksCmdsJPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(allRacksJPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel allChannelsCmdsJPanel;
    private javax.swing.JPanel allChannelsJPanel;
    private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer allPSChannelsOffCommandViewer;
    private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer allPSChannelsOnCommandViewer;
    private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer allPSChannelsResetCommandViewer;
    private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer allPSChannelsResetVoltageCommandViewer;
    private javax.swing.JPanel allRacksCmdsJPanel;
    private javax.swing.JPanel allRacksJPanel;
    private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer allRacksResetCommandViewer;
    private javax.swing.JPanel synopticPlaceHolder;
    // End of variables declaration//GEN-END:variables

    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        final String[] cellArgs;
        cellArgs = args;
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                String devName = null;
                String cellNumber = null;
                
                if (cellArgs.length > 0)
                {
                    cellNumber = cellArgs[0];
                }
                else
                {
//                    if (JEbsSrPsConstants.CHARTREUSE_CONFIGURATION)
//                        cellNumber = "37";
//                    else
                        cellNumber = javax.swing.JOptionPane.showInputDialog (null, "Please enter Cell number: ");
                }
                
                if (cellNumber == null) // cancel
                    System.exit(0);
                
                int cellNb = -1;
                
                try
                {
                    cellNb = Integer.parseInt(cellNumber);
                    
                }
                catch(NumberFormatException nfe)
                {
                    JOptionPane.showMessageDialog(null,
                                                  "Please enter a Valid cell number",
                                                  "Input Exception",
                                                  JOptionPane.ERROR_MESSAGE);
                    System.exit(-1);
                }
                
                
                if ((cellNumber.length() < 2 || (cellNb < 1 || cellNb > 32)) && (cellNb != 37))
                {
                    JOptionPane.showMessageDialog(null,
                                                  "Please enter a Valid cell number",
                                                  "Invalid Cell",
                                                  JOptionPane.WARNING_MESSAGE);
                    System.exit(-1);
                }
         
                devName = "srmag/hsm/c".concat(cellNumber);
                
                JFrame  jf = new JFrame();
                ATKGraphicsUtils.centerFrameOnScreen(jf);
                jf.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                HscPanel  hscp = new HscPanel(devName);
                jf.setTitle(" HOT SWAP CUBICLE ");
                jf.setContentPane(hscp);
                jf.pack();
                jf.setVisible(true);
            }
        });
    }

}
