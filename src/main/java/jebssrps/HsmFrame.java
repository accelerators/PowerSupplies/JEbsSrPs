/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jebssrps;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.ICommand;
import fr.esrf.tangoatk.core.IDevStateScalar;
import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.core.IEnumScalar;
import fr.esrf.tangoatk.core.IStringScalar;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.util.ErrorPopup;


/**
 *
 * @author kramer
 */
public class HsmFrame extends javax.swing.JFrame
{    
    private String             hsmDevName = null;
    private String             cellNbStr = "";
    private AttributeList      attl;
    private AttributeList      hsmAttl = null;
    private CommandList        cmdl;
    
    IDevStateScalar            hsmStateAtt = null;
    IStringScalar              hsmStatusAtt = null;
    IEnumScalar                hsmModeAtt = null;
    ICommand                   hsmResetCmd = null;


    /**
     * Creates new form HSMJFrame
     */
    public HsmFrame(String devName) throws ConnectionException, IllegalArgumentException
    {        
        hsmDevName = devName;
    
        int  index = hsmDevName.lastIndexOf("/");
        cellNbStr = hsmDevName.substring(index + 2);
    
        attl = new AttributeList();
        attl.addErrorListener(JEbsSrPsConstants.ERR_H);
        attl.addErrorListener(ErrorPopup.getInstance());
        
        hsmAttl = new AttributeList();
        hsmAttl.addErrorListener(JEbsSrPsConstants.ERR_H);
        hsmAttl.addSetErrorListener(JEbsSrPsConstants.ERR_H);
        hsmAttl.addSetErrorListener(ErrorPopup.getInstance());
        
        cmdl = new CommandList();
        cmdl.addErrorListener(JEbsSrPsConstants.ERR_H);
        cmdl.addErrorListener(ErrorPopup.getInstance());
                
        initComponents();
                
        connectToCommandsAndAttributes();
        
        attl.startRefresher();
                
        setTitle(hsmDevName);

        pack();
        setVisible(true);
        toFront();
    }

    
    private void connectToCommandsAndAttributes()
    {
        String entityname = null;
        IEntity oneEntity = null;
        String msgatt = "Cannot connect to the attribute : ";
        String msgcmd = "Cannot connect to the command : ";
        String msg = null;

        // Connect to the Hsm device attributes
        try
        {
            entityname = hsmDevName + "/State";
            msg = msgatt + entityname;
            oneEntity = attl.add(entityname);
            if (oneEntity instanceof IDevStateScalar)
            {
                hsmStateAtt = (IDevStateScalar) oneEntity;
                hsmStateViewer.setModel(hsmStateAtt);
            }

            entityname = hsmDevName + "/Status";
            msg = msgatt + entityname;
            oneEntity = attl.add(entityname);
            if (oneEntity instanceof IStringScalar)
            {
                hsmStatusAtt = (IStringScalar) oneEntity;
                hsmStatusViewer.setModel(hsmStatusAtt);
            }

            entityname = hsmDevName + "/NoHsm";
            msg = msgatt + entityname;
            oneEntity = attl.add(entityname);
            hsmAttl.add(oneEntity);

            entityname = hsmDevName + "/Q_Mode";
            msg = msgatt + entityname;
            oneEntity = attl.add(entityname);
            hsmAttl.add(oneEntity);

            entityname = hsmDevName + "/DQ_Mode";
            msg = msgatt + entityname;
            oneEntity = attl.add(entityname);
            hsmAttl.add(oneEntity);

            entityname = hsmDevName + "/SO_Mode";
            msg = msgatt + entityname;
            oneEntity = attl.add(entityname);
            hsmAttl.add(oneEntity);

            if (!hsmAttl.isEmpty())
            {
                hsmScalarListViewer.setModel(hsmAttl);
            }
        }
        catch (ConnectionException ex)
        {
            javax.swing.JOptionPane.showMessageDialog(null, msg + " .\n"
                    + "Check if the device " + hsmDevName + " is responding?\n",
                    "HsmFrame : Attribute connection",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        }


        // Connect to the HSM device commands
        try
        {
            entityname = hsmDevName + "/Reset";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand)
            {
                hsmResetCmd = (ICommand) oneEntity;
                hsmResetVVcmdV.setModel(hsmResetCmd);
            }
        }
        catch (ConnectionException ex)
        {
            javax.swing.JOptionPane.showMessageDialog(null, msg + " .\n"
                    + "Check if the device " + hsmDevName + " is responding?\n",
                    "HsmFrame : Command connection",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        hsmJPanel = new javax.swing.JPanel();
        hsmStateViewer = new fr.esrf.tangoatk.widget.attribute.StateViewer();
        hsmStatusViewer = new fr.esrf.tangoatk.widget.attribute.StatusViewer();
        cmdJPanel = new javax.swing.JPanel();
        hsmResetVVcmdV = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
        hsmScalarListViewer = new fr.esrf.tangoatk.widget.attribute.ScalarListViewer();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        hsmJPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        hsmJPanel.add(hsmStateViewer, gridBagConstraints);

        hsmStatusViewer.setColumns(20);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        hsmJPanel.add(hsmStatusViewer, gridBagConstraints);

        cmdJPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        cmdJPanel.add(hsmResetVVcmdV, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        hsmJPanel.add(cmdJPanel, gridBagConstraints);

        hsmScalarListViewer.setPropertyButtonVisible(false);
        hsmScalarListViewer.setTheFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        hsmJPanel.add(hsmScalarListViewer, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        getContentPane().add(hsmJPanel, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        endFrame();
    }//GEN-LAST:event_formWindowClosing

    
    private void endFrame()
    {
        hsmAttl.stopRefresher();
        attl.stopRefresher();

        hsmAttl.removeErrorListener(JEbsSrPsConstants.ERR_H);
        hsmStateViewer.clearModel();
        hsmStatusViewer.clearModel();
        hsmAttl.clear();
        
        attl.clear();
        hsmAttl.clear();
        
        dispose();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(HsmFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(HsmFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(HsmFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(HsmFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new HsmFrame("srmag/hsm/c12").setVisible(true);
                } catch (Exception ex) {
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel cmdJPanel;
    private javax.swing.JPanel hsmJPanel;
    private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer hsmResetVVcmdV;
    private fr.esrf.tangoatk.widget.attribute.ScalarListViewer hsmScalarListViewer;
    private fr.esrf.tangoatk.widget.attribute.StateViewer hsmStateViewer;
    private fr.esrf.tangoatk.widget.attribute.StatusViewer hsmStatusViewer;
    // End of variables declaration//GEN-END:variables

}
