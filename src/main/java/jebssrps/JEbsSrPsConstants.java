/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jebssrps;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.Device;
import fr.esrf.tangoatk.core.DeviceFactory;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.SettingsManagerProxy;
import java.awt.Color;
import jdoorWrapViewer.JDoorWrapperProxy;

/**
 *
 * @author poncet
 */
public abstract class JEbsSrPsConstants
{

    static public final ErrorHistory            ERR_H = new ErrorHistory();
    
    static public final int                     ATK_TRACE_LEVEL = DeviceFactory.TRACE_SUCCESS |
                                                              DeviceFactory.TRACE_FAIL |
                                                              DeviceFactory.TRACE_COMMAND |
                                                              DeviceFactory.TRACE_CHANGE_EVENT;
    
//    static public boolean                       CHARTREUSE_CONFIGURATION = false;
    
    static public final String                  CHARTREUSE_SYNOPTIC_FILE_NAME = "HSC-Chartreuse-template.jdw";
    static public final String                  EBS_SYNOPTIC_FILE_NAME        = "HSC-template.jdw";
    static public final String                  C3_SYNOPTIC_FILE_NAME         = "HSC-C3-template.jdw";
    static public final String                  C4_SYNOPTIC_FILE_NAME         = "HSC-C4-template.jdw";
    
    static public final String                  SWAP_INHIB_SYNOPTIC_FILE_NAME    = "HSM-SwapInhibition-template.jdw";
    static public final String                  SWAP_INHIB_C3_SYNOPTIC_FILE_NAME = "HSM-SwapInhibition-C3-template.jdw";
    static public final String                  SWAP_INHIB_C4_SYNOPTIC_FILE_NAME = "HSM-SwapInhibition-C4-template.jdw";
    
    static public final String                  EBSSRPS_SYNOPTIC_FILE_NAME    = "EbsSrPsMain.jdw";
    
    static public final String                  ITLK_SYNOPTIC_FILE_NAME = "EbsSrPsIterlocks.jdw";
    
    static public final String                  APPLI_VERSION_TAG;
    
    private static final String                 JEBSSRPS_FREE_PROP = "JEbsSrPs";
       
    private static final String                 PSM_DEV_PROXY_TIMEOUT_MS_PROP = "PsMainTimeout";
    public static  final int                    DEFAULT_PSM_DEV_PROXY_TIMEOUT_MS = 5000;
    public static  final int                    PSM_DEV_PROXY_TIMEOUT_MS;
       
    private static final String                 MINI_BETA_PROP = "MiniBeta";
    public  static final boolean                MINI_BETA;
    
    static public String                        HSC_FRAME_SETTINGS_MANAGER = "sys/settings/hsc-chartreuse";
    public static SettingsManagerProxy          SMP;
    
    public static String                        ITLK_SUMMARY_DEVNAME = "srmag/itlk/summary";
    public static String                        HSC_ITLK_DEVNAME_PATTERN = "srmag/hsc-itlk/c";
    static public final String                  HSC_ITLK_RESET_CMD = "Reset";
    static public final String                  HSC_ITLK_RESET_PSS_CMD = "ResetPssDQ1D";
    
    public static String                        MAG_ITLK_DEVNAME_PATTERN = "srmag/fl-temp-itlk/c";
    static public final String                  MAG_ITLK_RESET_CMD = "Reset";
    
    static public final String[]                EBS_PS_CHANNEL_DEV_NAMES = {
                                                    "srmag/ps-qf6/c##-b",
                                                    "srmag/ps-qf1/c##-a",
                                                    "srmag/ps-qd2/c##-a",                                                    
                                                    "srmag/ps-qf8/c##-b",
                                                    "srmag/ps-qd3/c##-a",
                                                    "srmag/ps-qf4/c##-a",                                                    
                                                    "srmag/ps-qf8/c##-d",
                                                    "srmag/ps-qf4/c##-b",
                                                    "srmag/ps-qd5/c##-b",                                                    
                                                    "srmag/ps-qf6/c##-d",
                                                    "srmag/ps-qd5/c##-d",
                                                    "srmag/ps-qf4/c##-d",                                                    
                                                    "srmag/ps-qspare/c##-1",
                                                    "srmag/ps-qf4/c##-e",
                                                    "srmag/ps-qd3/c##-e",                                                   
                                                    "srmag/ps-qspare/c##-2",
                                                    "srmag/ps-qd2/c##-e",
                                                    "srmag/ps-qf1/c##-e",                                                    
                                                    "srmag/ps-qspare/c##-3",
                                                    "srmag/ps-qspare/c##-4",
                                                    "srmag/ps-of1/c##-b",                                                    
                                                    "srmag/ps-sd1/c##-a",
                                                    "srmag/ps-sf2/c##-a",
                                                    "srmag/ps-sd1/c##-b",                                                    
                                                    "srmag/ps-sd1/c##-d",
                                                    "srmag/ps-sf2/c##-e",
                                                    "srmag/ps-sd1/c##-e",                                                    
                                                    "srmag/ps-dq1/c##-b",
                                                    "srmag/ps-dq2/c##-c",
                                                    "srmag/ps-sospare/c##-1",                                                    
                                                    "srmag/ps-dq1/c##-d",
                                                    "srmag/ps-dqspare/c##-1",
                                                    "srmag/ps-of1/c##-d"
                                                    };

    
    static public final String[]                C3_PS_CHANNEL_DEV_NAMES = {
                                                    "srmag/ps-qf6/c##-b",
                                                    "srmag/ps-qf1/c##-a",
                                                    "srmag/ps-qd2/c##-a",                                                    
                                                    "srmag/ps-qf8/c##-b",
                                                    "srmag/ps-qd3/c##-a",
                                                    "srmag/ps-qf4/c##-a",                                                    
                                                    "srmag/ps-qf8/c##-d",
                                                    "srmag/ps-qf4/c##-b",
                                                    "srmag/ps-qd5/c##-b",                                                    
                                                    "srmag/ps-qf6/c##-d",
                                                    "srmag/ps-qd5/c##-d",
                                                    "srmag/ps-qf4/c##-d",                                                    
                                                    "srmag/ps-qspare/c##-1",
                                                    "srmag/ps-qf4/c##-e",
                                                    "srmag/ps-qf2/c##-e",                                                   
                                                    "srmag/ps-qspare/c##-2",
                                                    "srmag/ps-qd2/c##-e",
                                                    "srmag/ps-qd3/c##-e",                                                    
                                                    "srmag/ps-qspare/c##-3",
                                                    "srmag/ps-qspare/c##-4",
                                                    "srmag/ps-of1/c##-b",                                                    
                                                    "srmag/ps-sd1/c##-a",
                                                    "srmag/ps-sf2/c##-a",
                                                    "srmag/ps-sd1/c##-b",                                                    
                                                    "srmag/ps-sd1/c##-d",
                                                    "srmag/ps-sf2/c##-e",
                                                    "srmag/ps-sd1/c##-e",                                                    
                                                    "srmag/ps-dq1/c##-b",
                                                    "srmag/ps-dq2/c##-c",
                                                    "srmag/ps-sospare/c##-1",                                                    
                                                    "srmag/ps-dq1/c##-d",
                                                    "srmag/ps-dqspare/c##-1",
                                                    "srmag/ps-of1/c##-d"
                                                    };
    
    static public final String[]                C4_PS_CHANNEL_DEV_NAMES = {
                                                    "srmag/ps-qf6/c##-b",
                                                    "srmag/ps-qd3/c##-a",
                                                    "srmag/ps-qd2/c##-a",                                                    
                                                    "srmag/ps-qf8/c##-b",
                                                    "srmag/ps-qf2/c##-a",
                                                    "srmag/ps-qf4/c##-a",                                                    
                                                    "srmag/ps-qf8/c##-d",
                                                    "srmag/ps-qf4/c##-b",
                                                    "srmag/ps-qd5/c##-b",                                                    
                                                    "srmag/ps-qf6/c##-d",
                                                    "srmag/ps-qd5/c##-d",
                                                    "srmag/ps-qf4/c##-d",                                                    
                                                    "srmag/ps-qspare/c##-1",
                                                    "srmag/ps-qf4/c##-e",
                                                    "srmag/ps-qd3/c##-e",                                                   
                                                    "srmag/ps-qspare/c##-2",
                                                    "srmag/ps-qd2/c##-e",
                                                    "srmag/ps-qf1/c##-e",                                                    
                                                    "srmag/ps-qspare/c##-3",
                                                    "srmag/ps-qspare/c##-4",
                                                    "srmag/ps-of1/c##-b",                                                    
                                                    "srmag/ps-sd1/c##-a",
                                                    "srmag/ps-sf2/c##-a",
                                                    "srmag/ps-sd1/c##-b",                                                    
                                                    "srmag/ps-sd1/c##-d",
                                                    "srmag/ps-sf2/c##-e",
                                                    "srmag/ps-sd1/c##-e",                                                    
                                                    "srmag/ps-dq1/c##-b",
                                                    "srmag/ps-dq2/c##-c",
                                                    "srmag/ps-sospare/c##-1",                                                    
                                                    "srmag/ps-dq1/c##-d",
                                                    "srmag/ps-dqspare/c##-1",
                                                    "srmag/ps-of1/c##-d"
                                                    };
    
    static public final String              DOOR_WRAPPER = "srmag/door_wrapper/01";
    public static JDoorWrapperProxy         JDOOR_WRAPPER_PROXY = null;
    
    public static final Color[]             MOSPLATE_DATAVIEW_COLORS = { Color.RED, Color.BLUE, Color.BLACK };

    
    static // find the version number in the property file
    {
        java.util.Properties props = new java.util.Properties();
        java.io.InputStream stream = JEbsSrPsConstants.class.getClassLoader().getResourceAsStream("jebssrps.properties");
        String propValue = null;
        try
        {
            props.load(stream);
            propValue = props.getProperty("jebssrps.version");
//            System.out.println(propValue);
        }
        catch (java.io.IOException ex) {}

        if (propValue == null)
            APPLI_VERSION_TAG = "xx";
        else
            APPLI_VERSION_TAG = propValue;
    }
    
    static  // PsMain device timeout support
    {
        // get the free properties for JEbsSrPs to set the timeout for PS Main device proxy
        Database   dbConnection = null;
        try
        {
            dbConnection = ApiUtil.get_db_obj();
        }
        catch (DevFailed ex) {}
       
        if (dbConnection != null)
        {
            short  propValue;
                       
            propValue = getPropFromTangoDB(dbConnection,PSM_DEV_PROXY_TIMEOUT_MS_PROP );
            if (propValue > 0)
            {
                PSM_DEV_PROXY_TIMEOUT_MS = propValue;
            }

           else PSM_DEV_PROXY_TIMEOUT_MS = DEFAULT_PSM_DEV_PROXY_TIMEOUT_MS;

        }

        else  PSM_DEV_PROXY_TIMEOUT_MS = DEFAULT_PSM_DEV_PROXY_TIMEOUT_MS;
    }
    
    static  // Mini Beta PS channel names support
    {
        // get the free properties for JEbsSrPs to set the timeout for PS Main device proxy
        Database   dbConnection = null;
        try
        {
            dbConnection = ApiUtil.get_db_obj();
        }
        catch (DevFailed ex) {}
       
        if (dbConnection == null)
            MINI_BETA = false;
        else
            MINI_BETA = getBooleanPropFromTangoDB(dbConnection, MINI_BETA_PROP );
    }
    
    private static short getPropFromTangoDB(Database tangodb, String prop)
    { 
        if (tangodb == null) return -1;
               
        DbDatum dbd = null;
        try
        {
            dbd = tangodb.get_property(JEBSSRPS_FREE_PROP , prop);
        }
        catch (DevFailed ex) {}
           
        if (dbd == null) return -1;
        if (dbd.is_empty()) return -1;
       
        short argout = dbd.extractShort();
        return argout;
    }
    
    private static boolean getBooleanPropFromTangoDB(Database tangodb, String prop)
    { 
        if (tangodb == null) return false;
               
        DbDatum dbd = null;
        try
        {
            dbd = tangodb.get_property(JEBSSRPS_FREE_PROP , prop);
        }
        catch (DevFailed ex) {}
           
        if (dbd == null) return false;
        if (dbd.is_empty()) return false;
       
        boolean argout = dbd.extractBoolean();
        return argout;
    }
    
    static // connect to doorwrapper device
    {
        try
        {
            Device  dev = DeviceFactory.getInstance().getDevice(DOOR_WRAPPER);
            JDOOR_WRAPPER_PROXY = new JDoorWrapperProxy(DOOR_WRAPPER);
        }
        catch (ConnectionException ex)
        {
            JDOOR_WRAPPER_PROXY = null;
        }
    }

 }
