/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jebssrps;

import fr.esrf.tangoatk.core.ATKException;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.DeviceFactory;
import fr.esrf.tangoatk.core.ICommand;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.core.command.VoidVoidCommandGroup;
import fr.esrf.tangoatk.widget.jdraw.SynopticProgressListener;
import fr.esrf.tangoatk.widget.jdraw.TangoSynopticHandler;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import fr.esrf.tangoatk.widget.util.Splash;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.MissingResourceException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import viewers.SrpsMainSynopticViewer;

/**
 *
 * @author kramer
 */
public class EbsSrPsMainWindow extends javax.swing.JFrame implements SynopticProgressListener {

    private VoidVoidCommandGroup    resetItlkCmdg = null, resetPssCmdg = null, resetMagCmdg;
    private VoidVoidCommandGroup    resetVoltageCmdg = null;
    private ICommand  resetSRDCMatrix = null;
    Splash sw;

    /**
     * Creates new form EbsSrPs
     */
    public EbsSrPsMainWindow() {
        
        setTitle("EbsSrPs  " + JEbsSrPsConstants.APPLI_VERSION_TAG);
       
        sw = new Splash();
        sw.setVisible(true);
        sw.setTitle("EbsSrPs  " + JEbsSrPsConstants.APPLI_VERSION_TAG);
        sw.setCopyright("(c) ESRF 2018 - 2022");
        sw.setMessage("Connecting to EbsSrPs devices ...");
        sw.initProgress();
        
        initComponents();
                
        addSynoptic();
        
        resetItlkCmdg = new VoidVoidCommandGroup();
        resetPssCmdg = new VoidVoidCommandGroup();
        resetMagCmdg = new VoidVoidCommandGroup();
        resetVoltageCmdg = new VoidVoidCommandGroup();
        
        resetItlkCmdg.addErrorListener(ErrorPopup.getInstance());
        resetPssCmdg.addErrorListener(ErrorPopup.getInstance());
        resetMagCmdg.addErrorListener(ErrorPopup.getInstance());
        resetVoltageCmdg.addErrorListener(ErrorPopup.getInstance());

        resetItlkCmdg.addErrorListener(JEbsSrPsConstants.ERR_H);
        resetPssCmdg.addErrorListener(JEbsSrPsConstants.ERR_H);
        resetMagCmdg.addErrorListener(JEbsSrPsConstants.ERR_H);
        resetVoltageCmdg.addErrorListener(JEbsSrPsConstants.ERR_H);
        
        connectToResetCommands();
        
        if (resetVoltageCmdg.isEmpty())
            resetVoltageJMenuItem.setVisible(false);
        
        CommandList cmdL = new CommandList();
        cmdL.addErrorListener(ErrorPopup.getInstance());
        cmdL.addErrorListener(JEbsSrPsConstants.ERR_H);
        
        String command = "srmag/srdc-matrix/1/Reset";
        try {
            resetSRDCMatrix = (ICommand)cmdL.add(command);
        } catch (ConnectionException ex) {            
            JOptionPane.showMessageDialog(rootPane, "Failed to Connect to "+command, "Connection to " + command, JOptionPane.ERROR_MESSAGE);
        }
        
        sw.setVisible(false);
        ATKGraphicsUtils.centerFrameOnScreen(this);
        
        setVisible(true);
    }
    
    @Override
    public void progress(double d) {
        sw.progress((int)(d*100.0) );
    }

    private void addSynoptic()
    {       
        SrpsMainSynopticViewer  synoptViewer = new SrpsMainSynopticViewer(hscPlaceHolder);
        synoptViewer.setProgressListener(this);

        // Load the synoptic file                                
        try
        {
            synoptViewer.setErrorHistoryWindow(jebssrps.JEbsSrPsConstants.ERR_H);
            synoptViewer.setAutoZoom(true);
        }
        catch (Exception setErrwExcept)
        {
            System.out.println("Cannot set Error History Window for Synoptic");
        }

        InputStream jdFileInStream;
        jdFileInStream = jebssrps.JEbsSrPsConstants.class.getClassLoader().getResourceAsStream(jebssrps.JEbsSrPsConstants.EBSSRPS_SYNOPTIC_FILE_NAME);

        if (jdFileInStream == null)
        {
            javax.swing.JOptionPane.showMessageDialog(
                    null, "Failed to get the inputStream for the synoptic file resource \n",
                    "Resource error",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
        
        java.io.InputStreamReader inStrReader = new InputStreamReader(jdFileInStream);
        try
        {
            synoptViewer.loadMainSynopticFromStream(inStrReader);
            synoptViewer.setToolTipMode(TangoSynopticHandler.TOOL_TIP_NAME);
        }
        catch (IOException ioex)
        {
            javax.swing.JOptionPane.showMessageDialog(
                        null, "Cannot find load the synoptic input stream reader.\n"
                        + " Application will abort ...\n"
                        + ioex,
                        "Resource access failed",
                        javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
        catch (MissingResourceException mrEx)
        {
            javax.swing.JOptionPane.showMessageDialog(
                        null, "Cannot parse the synoptic file.\n"
                        + " Application will abort ...\n"
                        + mrEx,
                        "Cannot parse the file",
                        javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }                       
        
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.insets = new java.awt.Insets(5, 5, 5, 5);
        srpsSynopticPlaceHolder.add(synoptViewer, gbc);
        
        hscPlaceHolder.setMinimumSize(new Dimension(420, 570));
        srpsSplitPane.setDividerLocation((int) 500);        
    }
    
    void connectToResetCommands() 
    {
        String  msgcmd = new String("Cannot connect to the command: ");
        String  msg    = null;
        String  entityname = null;
        
        String devName = null;
        String cellStr = null;
        
        for (int i=1; i<33; i++)
        {
            cellStr = cellStr.format ("%02d", i);
            try {
                devName = JEbsSrPsConstants.HSC_ITLK_DEVNAME_PATTERN + cellStr + "/";
                
                entityname =  devName + JEbsSrPsConstants.HSC_ITLK_RESET_CMD;
                msg = msgcmd + entityname;
                resetItlkCmdg.add(entityname);

                entityname = devName + JEbsSrPsConstants.HSC_ITLK_RESET_PSS_CMD;
                msg = msgcmd + entityname;
                resetPssCmdg.add(entityname);

            } catch (Exception e) {
                String   str=null;

                if (e instanceof ATKException) {
                    ATKException  atkex = (ATKException) e;
                    str = atkex.getDescription();
                } else
                    str = e.toString();

                javax.swing.JOptionPane.showMessageDialog(
                        null, msg
                        + ".\n\n"
                        + "Connection Exception : " + str,
                        "Connection to " + entityname,
                        javax.swing.JOptionPane.ERROR_MESSAGE);
            }
            
            try {
                devName = JEbsSrPsConstants.MAG_ITLK_DEVNAME_PATTERN + cellStr + "/";
                
                entityname = devName + JEbsSrPsConstants.MAG_ITLK_RESET_CMD;
                msg = msgcmd + entityname;
                resetMagCmdg.add(entityname);
                
            } catch (Exception e) {
                String   str=null;

                if (e instanceof ATKException) {
                    ATKException  atkex = (ATKException) e;
                    str = atkex.getDescription();
                } else
                    str = e.toString();

                javax.swing.JOptionPane.showMessageDialog(
                        null, msg
                        + ".\n\n"
                        + "Connection Exception : " + str,
                        "Connection to " + entityname,
                        javax.swing.JOptionPane.ERROR_MESSAGE);
            }
            
            
            try
            {
                entityname = "srmag/ps-main/c" + cellStr + "/ResetVoltage";
                msg = msgcmd + entityname;
                resetVoltageCmdg.add(entityname);
            }
            catch (Exception e)
            {  // don't report error in error popup ...
            }
            
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        srpsSplitPane = new javax.swing.JSplitPane();
        srpsSynopticPlaceHolder = new javax.swing.JPanel();
        hscPlaceHolder = new javax.swing.JPanel();
        ebssrpsMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        exitJMenuItem = new javax.swing.JMenuItem();
        viewMenu = new javax.swing.JMenu();
        itlkJMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        errorsMenuItem = new javax.swing.JMenuItem();
        diagMenuItem = new javax.swing.JMenuItem();
        resetMenu = new javax.swing.JMenu();
        itlkMenu = new javax.swing.JMenu();
        resetItlkJMenuItem = new javax.swing.JMenuItem();
        resetPssJMenuItem = new javax.swing.JMenuItem();
        resetMagjMenuItem = new javax.swing.JMenuItem();
        resetVoltageJMenuItem = new javax.swing.JMenuItem();
        resetVoltageJMenuItem1 = new javax.swing.JMenuItem();
        jMenuHelp = new javax.swing.JMenu();
        jMenuItemAbout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        srpsSplitPane.setOneTouchExpandable(true);

        srpsSynopticPlaceHolder.setPreferredSize(new java.awt.Dimension(500, 570));
        srpsSynopticPlaceHolder.setLayout(new java.awt.GridBagLayout());
        srpsSplitPane.setLeftComponent(srpsSynopticPlaceHolder);

        hscPlaceHolder.setPreferredSize(new java.awt.Dimension(420, 570));
        hscPlaceHolder.setLayout(new java.awt.GridBagLayout());
        srpsSplitPane.setRightComponent(hscPlaceHolder);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(srpsSplitPane, gridBagConstraints);

        fileMenu.setText("File");

        exitJMenuItem.setText("Quit");
        exitJMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitJMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitJMenuItem);

        ebssrpsMenuBar.add(fileMenu);

        viewMenu.setText("View");

        itlkJMenuItem.setText("All Interlocks ...");
        itlkJMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itlkJMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(itlkJMenuItem);
        viewMenu.add(jSeparator1);

        errorsMenuItem.setText("Error History ...");
        errorsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                errorsMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(errorsMenuItem);

        diagMenuItem.setText("Diagnostics...");
        diagMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                diagMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(diagMenuItem);

        ebssrpsMenuBar.add(viewMenu);

        resetMenu.setText("Reset");

        itlkMenu.setText("Reset Interlocks");

        resetItlkJMenuItem.setText("All Cubicles");
        resetItlkJMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetItlkJMenuItemActionPerformed(evt);
            }
        });
        itlkMenu.add(resetItlkJMenuItem);

        resetPssJMenuItem.setText("All PSS DQ1D");
        resetPssJMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetPssJMenuItemActionPerformed(evt);
            }
        });
        itlkMenu.add(resetPssJMenuItem);

        resetMagjMenuItem.setText("All Magnets");
        resetMagjMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetMagjMenuItemActionPerformed(evt);
            }
        });
        itlkMenu.add(resetMagjMenuItem);

        resetMenu.add(itlkMenu);

        resetVoltageJMenuItem.setText("Reset All Voltages ...");
        resetVoltageJMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetVoltageJMenuItemActionPerformed(evt);
            }
        });
        resetMenu.add(resetVoltageJMenuItem);

        resetVoltageJMenuItem1.setText("Reset SRDC matrix");
        resetVoltageJMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetVoltageJMenuItem1ActionPerformed(evt);
            }
        });
        resetMenu.add(resetVoltageJMenuItem1);

        ebssrpsMenuBar.add(resetMenu);

        jMenuHelp.setText("Help");

        jMenuItemAbout.setText("About");
        jMenuItemAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAboutActionPerformed(evt);
            }
        });
        jMenuHelp.add(jMenuItemAbout);

        ebssrpsMenuBar.add(jMenuHelp);

        setJMenuBar(ebssrpsMenuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitJMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitJMenuItemActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_exitJMenuItemActionPerformed

    private void errorsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_errorsMenuItemActionPerformed
        // TODO add your handling code here:
        ATKGraphicsUtils.centerFrame(rootPane, JEbsSrPsConstants.ERR_H);
        JEbsSrPsConstants.ERR_H.setVisible(true);
    }//GEN-LAST:event_errorsMenuItemActionPerformed

    private void diagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diagMenuItemActionPerformed
        // TODO add your handling code here:
        fr.esrf.tangoatk.widget.util.ATKDiagnostic.showDiagnostic();
    }//GEN-LAST:event_diagMenuItemActionPerformed

    private void resetItlkJMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetItlkJMenuItemActionPerformed
        // TODO add your handling code here:
        if (resetItlkCmdg.isEmpty()) return;
        resetItlkCmdg.execute();
    }//GEN-LAST:event_resetItlkJMenuItemActionPerformed

    private void resetPssJMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetPssJMenuItemActionPerformed
        // TODO add your handling code here:
        if (resetPssCmdg.isEmpty()) return;
        resetPssCmdg.execute();
    }//GEN-LAST:event_resetPssJMenuItemActionPerformed

    private void resetMagjMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetMagjMenuItemActionPerformed
        // TODO add your handling code here:
        if (resetMagCmdg.isEmpty()) return;
        resetMagCmdg.execute();
    }//GEN-LAST:event_resetMagjMenuItemActionPerformed

    private void itlkJMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_itlkJMenuItemActionPerformed
    {//GEN-HEADEREND:event_itlkJMenuItemActionPerformed
        // TODO add your handling code here:
        ATKGraphicsUtils.centerFrameOnScreen(AllSrItlkFrameSingleton.getInstance());
        AllSrItlkFrameSingleton.getInstance().setVisible(true);
        
    }//GEN-LAST:event_itlkJMenuItemActionPerformed

    private void resetVoltageJMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_resetVoltageJMenuItemActionPerformed
    {//GEN-HEADEREND:event_resetVoltageJMenuItemActionPerformed
        // TODO add your handling code here:
        if (resetVoltageCmdg.isEmpty()) return;
        
        int dialReturn = javax.swing.JOptionPane.showConfirmDialog(null,
                "You are going to reset the voltage on all comeca PS channels in all SR cells.\n\nDo you want to proceed?",
                "All Cells Reset Voltage   Confimation",
                javax.swing.JOptionPane.YES_NO_OPTION,
                javax.swing.JOptionPane.QUESTION_MESSAGE);

        if (dialReturn != javax.swing.JOptionPane.YES_OPTION)
        {
            return;
        }
        
        System.out.println("YES");
        
        resetVoltageCmdg.execute();
        
    }//GEN-LAST:event_resetVoltageJMenuItemActionPerformed

    private void jMenuItemAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAboutActionPerformed
        JOptionPane.showMessageDialog(this,
            "JEbsSrPs version " + JEbsSrPsConstants.APPLI_VERSION_TAG + "\n"
            + "Powered by Java version " + System.getProperty("java.version"),
            "About JEbsSrPs",
            JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuItemAboutActionPerformed

    private void resetVoltageJMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetVoltageJMenuItem1ActionPerformed
        resetSRDCMatrix.execute();
    }//GEN-LAST:event_resetVoltageJMenuItem1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(EbsSrPsMainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(EbsSrPsMainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(EbsSrPsMainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(EbsSrPsMainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                if (args.length > 0)
                {
                    if (args[0].equalsIgnoreCase("--trace"))
                    {
                        if (args.length == 1)
                            DeviceFactory.getInstance().setTraceMode(JEbsSrPsConstants.ATK_TRACE_LEVEL);
                        else
                        {
                            String  traceMskStr = args[1];
                            int     traceMask = -1;
                            try
                            {
                                traceMask = Integer.parseInt(traceMskStr);
                            }
                            catch (NumberFormatException ex) {}
                            if (traceMask == -1)
                                DeviceFactory.getInstance().setTraceMode(JEbsSrPsConstants.ATK_TRACE_LEVEL);
                            else
                                DeviceFactory.getInstance().setTraceMode(traceMask);
                        }
                    }
                }
                new EbsSrPsMainWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem diagMenuItem;
    private javax.swing.JMenuBar ebssrpsMenuBar;
    private javax.swing.JMenuItem errorsMenuItem;
    private javax.swing.JMenuItem exitJMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JPanel hscPlaceHolder;
    private javax.swing.JMenuItem itlkJMenuItem;
    private javax.swing.JMenu itlkMenu;
    private javax.swing.JMenu jMenuHelp;
    private javax.swing.JMenuItem jMenuItemAbout;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuItem resetItlkJMenuItem;
    private javax.swing.JMenuItem resetMagjMenuItem;
    private javax.swing.JMenu resetMenu;
    private javax.swing.JMenuItem resetPssJMenuItem;
    private javax.swing.JMenuItem resetVoltageJMenuItem;
    private javax.swing.JMenuItem resetVoltageJMenuItem1;
    private javax.swing.JSplitPane srpsSplitPane;
    private javax.swing.JPanel srpsSynopticPlaceHolder;
    private javax.swing.JMenu viewMenu;
    // End of variables declaration//GEN-END:variables

}
