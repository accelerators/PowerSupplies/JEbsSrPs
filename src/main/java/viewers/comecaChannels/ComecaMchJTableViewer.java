/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewers.comecaChannels;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.IDevStateSpectrum;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.core.IStringSpectrum;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import static javax.swing.SwingConstants.LEFT;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author poncet
 */
public class ComecaMchJTableViewer  extends JTable 
                                     implements WindowListener
{

    private ComecaMchTableModel              tableModel = null;
    private ComecaMchRendererAndEditor       tableCellRenderer = null;
    private final ComecaMchHeaderRenderer    colHeaderRenderer = new ComecaMchHeaderRenderer();
    private final ComecaMchHeaderRenderer    rowHeaderRenderer = new ComecaMchHeaderRenderer();

    private INumberSpectrum             comecaChannelsCurrentAtt = null;
    private INumberSpectrum             comecaChannelsVoltageAtt = null;
    private IStringSpectrum             comecaChannelsDevNameAtt = null;
    
    private ComecaChannelSetFrame[][]         comecaChannelSetPanels = null;
    private ComecaChannelWindow[]             comecaChWindows = null;
    
    public ComecaMchJTableViewer()
    {
        
        tableModel = new ComecaMchTableModel();
        setModel(tableModel);
        
        tableCellRenderer = new ComecaMchRendererAndEditor(this);
        rowHeaderRenderer.setHorizontalAlignment(LEFT);
        
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        setIntercellSpacing(new Dimension(2, 2));
//        setRowHeight(25);
        setRowHeight(getRowHeight() + 6);
        
        getTableHeader().setReorderingAllowed(false);
        getTableHeader().setDefaultRenderer(colHeaderRenderer);
        
        for (int i=0; i<tableModel.getColumnCount(); i++)
        {
            getColumnModel().getColumn(i).setPreferredWidth(60);
        }
        getColumnModel().getColumn(ComecaMchTableModel.CURRENT_COLUMN).setPreferredWidth(90);
        getColumnModel().getColumn(ComecaMchTableModel.VOLTAGE_COLUMN).setPreferredWidth(90);

        int  colNb = tableModel.getColumnCount();
        int  rowNb = tableModel.getRowCount();
        
        comecaChannelSetPanels = new ComecaChannelSetFrame[rowNb][colNb];
        comecaChWindows = new ComecaChannelWindow[rowNb];
        for (int i=0; i<rowNb; i++)
        {
            for (int j=0; j<colNb; j++)
            {
                comecaChannelSetPanels[i][j] = null;
            }
        }
        addMouseListener(tableCellRenderer);
    }

    public void clearModels()
    {
        tableModel.clearCurrentModel();
        tableModel.clearVoltageModel();
        tableModel.clearNameModel();
        
        comecaChannelsCurrentAtt = null;
        comecaChannelsVoltageAtt = null;
        comecaChannelsDevNameAtt = null;
        
        closeSetPanels();
        closeComecaWindows();
    }
    

    public void setModels(INumberSpectrum  att1Model, INumberSpectrum  att2Model, IStringSpectrum nameAttModel)
    {
        clearModels();
        
        if ( att1Model == null ) return;
        if ( att2Model == null ) return;
        if ( nameAttModel == null ) return;

        comecaChannelsCurrentAtt = att1Model;
        comecaChannelsVoltageAtt = att2Model;
        comecaChannelsDevNameAtt = nameAttModel;

        tableModel.setCurrentModel(comecaChannelsCurrentAtt);
        tableModel.setVoltageModel(comecaChannelsVoltageAtt);
        tableModel.setNameModel(comecaChannelsDevNameAtt);
    }


    @Override
    public TableCellRenderer getCellRenderer(int row, int column)
    {
        if (column == 0)
            return rowHeaderRenderer;
        else
            return tableCellRenderer;
    }

    @Override
    public boolean isCellEditable(int row, int column)
    {
        if ((column == ComecaMchTableModel.ON_COLUMN) || (column == ComecaMchTableModel.OFF_COLUMN) || (column == ComecaMchTableModel.DETAIL_COLUMN))
            return true;
        return false;
    }
    
    @Override
    public TableCellEditor getCellEditor(int row, int column)
    {

        if (column != 0)
            return tableCellRenderer;

        return super.getCellEditor(row, column);
    }

    
    void closeSetPanels()
    {
        for (int i=0; i<comecaChannelSetPanels.length; i++)
        {
            for (int j=0; j<comecaChannelSetPanels[0].length; j++)
            {
                if (comecaChannelSetPanels[i][j] != null)
                {
                    comecaChannelSetPanels[i][j].removeWindowListener(this);
                    comecaChannelSetPanels[i][j].getToolkit().getSystemEventQueue().postEvent(new WindowEvent(comecaChannelSetPanels[i][j], WindowEvent.WINDOW_CLOSING));
                    comecaChannelSetPanels[i][j] = null;
                }
            }
        }
        
    }

    
    void closeComecaWindows()
    {
        for (int i=0; i<comecaChWindows.length; i++)
        {
            if (comecaChWindows[i] != null)
            {
                comecaChWindows[i].removeWindowListener(this);
                comecaChWindows[i].getToolkit().getSystemEventQueue().postEvent(new WindowEvent(comecaChWindows[i], WindowEvent.WINDOW_CLOSING));
                comecaChWindows[i] = null;
            }
        }        
    }
   

    void doClickCell(int row, int col)
    {
        ComecaChannelSetFrame    psSetWindow = null;
        
        if ((row < 0) || (col < ComecaMchTableModel.CURRENT_COLUMN)) return;
        if ((row >= comecaChannelSetPanels.length) || (col >= comecaChannelSetPanels[0].length))
            return;
        
        if (comecaChannelSetPanels[row][col] == null)
        {
            String psDev = tableModel.getDevName(row, col);
            if (psDev == null) return;
            if (psDev.equalsIgnoreCase(ComecaMchTableModel.CHANNEL_NO_NAME))
                return;
            
            try
            {
                psSetWindow = new ComecaChannelSetFrame(psDev, row, col);
                comecaChannelSetPanels[row][col] = psSetWindow;
                psSetWindow.addWindowListener(this);
                ATKGraphicsUtils.centerFrame(this, comecaChannelSetPanels[row][col]);
            }
            catch (Exception  ex)
            {
                javax.swing.JOptionPane.showMessageDialog(
                        this, 
                         "Failed to create the comeca channel Set Panel.\n" 
                         + ex.getMessage() + " \n\n",
                        "Comeca Channel Set Panel error",
                        javax.swing.JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        
        if (comecaChannelSetPanels[row][col] == null)
            return;

        comecaChannelSetPanels[row][col].setVisible(true);
    }
    
    private void comecaSetFrameClosing (int rId, int cId)
    {        
        if ((rId < 0) || (cId < 0)) return;
        if ((rId >= comecaChannelSetPanels.length) || (cId >= comecaChannelSetPanels[0].length))
            return;
        comecaChannelSetPanels[rId][cId] = null;
    }
    
    private void comecaWindowClosing (int rId)
    {        
        if ((rId >= comecaChWindows.length) || (rId < 0))
            return;
        comecaChWindows[rId] = null;
    }
    
    
    
    ComecaChannelWindow getComecaWindow(int  devIndex)
    {
        if ((devIndex < 0) || (devIndex >= comecaChWindows.length)) return null;
        if (comecaChWindows[devIndex] == null)
            comecaChWindows[devIndex] = createComecaChWindow(devIndex);
        return (comecaChWindows[devIndex]);
    }
    
    private ComecaChannelWindow createComecaChWindow(int  devIndex)
    {
        String  devName = tableModel.getDevName(devIndex, ComecaMchTableModel.DETAIL_COLUMN);
        if (devName == null) return null;
        
        ComecaChannelWindow chw = null;
        try
        {
            chw = new ComecaChannelWindow(devName, devIndex);
            ATKGraphicsUtils.centerFrame(this, chw);
            chw.addWindowListener(this);
        }
        catch (IllegalArgumentException ex) {}
        return chw;
    }



    @Override
    public void windowClosing(WindowEvent e)
    {
        if (e.getSource() instanceof ComecaChannelSetFrame)
        {
            ComecaChannelSetFrame  sf = (ComecaChannelSetFrame) e.getSource();
            int rowId = sf.getRowId();
            int colId = sf.getColId();
            comecaSetFrameClosing(rowId, colId);
        }
        
        if (e.getSource() instanceof ComecaChannelWindow)
        {
            ComecaChannelWindow  chw = (ComecaChannelWindow) e.getSource();
            int rowId = chw.getRowId();
            comecaWindowClosing(rowId);
        }
    }
    
    @Override
    public void windowOpened(WindowEvent e) {}

    @Override
    public void windowClosed(WindowEvent e) {}

    @Override
    public void windowIconified(WindowEvent e) {}

    @Override
    public void windowDeiconified(WindowEvent e) {}

    @Override
    public void windowActivated(WindowEvent e) {}

    @Override
    public void windowDeactivated(WindowEvent e) {}


    // ---------------------------------------------------
    // Main test fucntion
    // ---------------------------------------------------
    static public void main(String args[])
    {
        JFrame f = new JFrame();
        ComecaMchJTableViewer comecaTable = new ComecaMchJTableViewer();
//        comecaTable.setColorState(true);


        // It is necessary to put the tBpmStateJTableViewer bpmTable = new BpmStateJTableViewer();able inside a JScrollPane. The JTable does not
        // display the column names if the JTable is not in a scrollPane!!!
        comecaTable.setPreferredScrollableViewportSize(new java.awt.Dimension(500, 460));
        JScrollPane scrollPane = new JScrollPane(comecaTable);

        AttributeList   attl = new AttributeList();
        try
        {
//            INumberSpectrum  ins = (INumberSpectrum) attl.add(SrCoConstants.ALL_HORIZ_STEER_DEV + "/" + SrCoConstants.STEER_CURRENT_ATT_NAME);
//            IStringSpectrum  iss = (IStringSpectrum) attl.add(SrCoConstants.ALL_HORIZ_STEER_DEV + "/" + SrCoConstants.STEER_NAME_ATT_NAME);
//            IDevStateSpectrum  idss = (IDevStateSpectrum) attl.add(SrCoConstants.ALL_HORIZ_STEER_DEV + "/" + SrCoConstants.STEER_STATE_ATT_NAME);

            INumberSpectrum  ins1 = (INumberSpectrum) attl.add("srmag/ps-main/c32/Currents");
            INumberSpectrum  ins2 = (INumberSpectrum) attl.add("srmag/ps-main/c32/Voltages");
            IStringSpectrum  iss = (IStringSpectrum) attl.add("srmag/ps-main/c32/PSNames");

            comecaTable.setModels(ins1, ins2, iss);
        }
        catch (Exception ex) {}
        attl.startRefresher();

        f.setContentPane(scrollPane);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //bpmTable.doLayout();
        f.pack();
        f.setVisible(true);
    }   
    
}
