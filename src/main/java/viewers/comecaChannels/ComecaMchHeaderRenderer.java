/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewers.comecaChannels;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author poncet
 */
public class ComecaMchHeaderRenderer extends JLabel implements TableCellRenderer
{
    /** Creates a new instance of SteererMchHeaderRenderer used for row or col headers */
    ComecaMchHeaderRenderer()
    {
       setHorizontalAlignment(CENTER);
       setFont(new java.awt.Font("Dialog", 1, 12)); 
       setOpaque(true);
       setBackground(new java.awt.Color(228,228,228));
       setBorder(javax.swing.plaf.BorderUIResource.getEtchedBorderUIResource());
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
                                                  boolean isSelected, boolean hasFocus, int row, int column)
    {
       String    headerId;

       if (value instanceof String)
       {
          headerId = (String) value;
          setText(headerId);
          return this;
       }
       else
          return new JLabel("Unsupported header id Class");
    }   

}
