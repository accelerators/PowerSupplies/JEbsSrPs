/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewers.comecaChannels;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.core.INumberScalar;
import fr.esrf.tangoatk.widget.attribute.NumberScalarSetPanel;
import jebssrps.JEbsSrPsConstants;

/**
 *
 * @author poncet
 */
public class ComecaChannelSetFrame extends javax.swing.JFrame
{
    private String                   comecaChannelDevName = null;
    private INumberScalar            comecaAtt = null;
    private NumberScalarSetPanel     nsSetPanel = null;
    private AttributeList            attl = new AttributeList();
    
    private final int                rowId, colId;

    /**
     * Creates new form ComecaChannelSetFrame
     * @param comecaDevName
     */
    public ComecaChannelSetFrame(String  comecaDevName, int row, int col) throws IllegalArgumentException
    {
        if ( (col != ComecaMchTableModel.CURRENT_COLUMN) && (col != ComecaMchTableModel.VOLTAGE_COLUMN) )
        {
            throw new IllegalArgumentException("ComecaChannelSetFrame Not authorized for this column\n");            
        }
        
        initComponents();
        
        comecaChannelDevName = comecaDevName;
        
        attl.addErrorListener(JEbsSrPsConstants.ERR_H);
        IEntity  ie = null;
        try
        {
           ie = attl.add(comecaChannelDevName + "/" + ComecaMchTableModel.COMECA_ATT_CMD_NAMES[col]);
        }
        catch (Exception ex)
        {
            throw new IllegalArgumentException("Cannot connect to the comecaChannel device : "
                    + comecaChannelDevName + " \n");
        }

        comecaAtt = (INumberScalar) ie;
        nsSetPanel = new NumberScalarSetPanel();
        nsSetPanel.setFont(new java.awt.Font("Dialog", 1, 14));
        nsSetPanel.setAttModel(comecaAtt);
        setContentPane(nsSetPanel);
        rowId = row;
        colId = col;
        
        setTitle(comecaChannelDevName);
        attl.startRefresher();
        pack();
        
        java.awt.Dimension  oldSize = getPreferredSize();
        setPreferredSize(new java.awt.Dimension(oldSize.width + 30, oldSize.height));
        pack();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        // TODO add your handling code here:
        endPanel();
    }//GEN-LAST:event_formWindowClosing


    void endPanel()
    {
        attl.stopRefresher();
        if (nsSetPanel != null)
            nsSetPanel.clearModel();
        attl.clear();
        dispose();
    }
    
    int getRowId()
    {
        return rowId;
    }
    
    int getColId()
    {
        return colId;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(ComecaChannelSetFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(ComecaChannelSetFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(ComecaChannelSetFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(ComecaChannelSetFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new ComecaChannelSetFrame("srmag/ps-qf6/c32-b", 0, 1).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
