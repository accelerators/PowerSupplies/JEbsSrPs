/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewers.comecaChannels;

import fr.esrf.TangoDs.AttrManip;
import fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer;
import fr.esrf.tangoatk.widget.util.ATKFormat;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author poncet
 */
public class ComecaMchRendererAndEditor extends AbstractCellEditor 
                                        implements TableCellRenderer, TableCellEditor, MouseListener
{
    ComecaMchJTableViewer               tble;
    ComecaMchTableModel                 model;

    Component     rendererComp;
    Component     editorComp;
    Object        editorValue;
    boolean       isCmdRenderAndEdit;
    
    JButton                    rendererPanelBtn = new JButton();
    JButton                    editorPanelBtn = new JButton();

    
    ComecaMchRendererAndEditor(ComecaMchJTableViewer table)
    {
        tble = table;
        model = (ComecaMchTableModel) table.getModel();
        
        rendererPanelBtn.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 14));
        rendererPanelBtn.setText("...");
        editorPanelBtn.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 14));
        editorPanelBtn.setText("...");

        editorPanelBtn.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                int row = tble.convertRowIndexToModel(tble.getEditingRow());
                showChannelPanel(row);
                fireEditingStopped();
            }

        });
    }

    private void showChannelPanel(int row)
    {
//                System.out.println("ShowChannelPanel : row = " + row);
        ComecaChannelWindow  comPanel = tble.getComecaWindow(row);
        if (comPanel == null) return;
        comPanel.setVisible(true);
    }

    private boolean isMouseOnAttribute()
    {
        int col = tble.getSelectedColumn();

        return ((col == model.CURRENT_COLUMN) || (col == model.VOLTAGE_COLUMN));
    }

    private JLabel getLabelForDouble()
    {
        JLabel jl = new JLabel();
        
        Border paddingBorder = BorderFactory.createEmptyBorder(2,10,2,10);  
        jl.setBorder(paddingBorder);
        
        jl.setHorizontalAlignment(JLabel.RIGHT);
        jl.setBackground(java.awt.Color.WHITE);

        jl.setOpaque(true);
        return jl;
    }
    
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        ComecaMchTableModel     model = (ComecaMchTableModel) table.getModel();
        String                  psDevName = model.getDevName(row, column);
        
        if (column == ComecaMchTableModel.DETAIL_COLUMN)
        {
            String devName = model.getDevName(row, column);
            String tt = "ComecaChannelWindow ("+devName+")";
            rendererPanelBtn.setToolTipText(tt);
            return rendererPanelBtn;
        }
        
        if (value instanceof Double)
        {
            String attFormat = model.getAttFormat(column);
            String attUnit = model.getAttUnit(column);
            Double valdouble = (Double) value;
            
            JLabel jl = getLabelForDouble();
            jl.setToolTipText(psDevName);
            if (attFormat == null)
            {
                jl.setText(valdouble.toString()+" "+attUnit);
            }
            else
            {
                if (attFormat.indexOf('%') == -1)
                {
                    jl.setText(AttrManip.format(attFormat, valdouble)+" "+attUnit);
                }
                else
                {
                    jl.setText(ATKFormat.format(attFormat, valdouble)+" "+attUnit);
                }
            }
            rendererComp = jl;
            return rendererComp;
        }
        
        if (value instanceof VoidVoidCommandViewer)
        {
            VoidVoidCommandViewer vvcmdv = (VoidVoidCommandViewer) value;
            rendererComp = vvcmdv;
            return rendererComp;
        }
        
        rendererComp = new JLabel("Unsupported Class");
        return rendererComp;
    }

    @Override
    public Object getCellEditorValue()
    {
        return editorValue;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
    {
        
        if (column == model.DETAIL_COLUMN)
        {
//                System.out.println("getTableCellEditorComponent : DETAIL_COLUMN");
            return editorPanelBtn;
        }
        
        Component rend = getTableCellRendererComponent(table, value, isSelected, true, row, column);
        editorComp = rend;
        this.editorValue = rend;
        return editorComp;        
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
    }
    
    @Override
    public void mousePressed(MouseEvent e)
    {
//        System.out.println("Mouse pressed");
        boolean isAtt = isMouseOnAttribute();
        if (isAtt)
        {
//            System.out.println("An attribute pressed");
            tble.doClickCell(tble.getSelectedRow(), tble.getSelectedColumn());
        }
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {
    }

    @Override
    public void mouseExited(MouseEvent e)
    {
    }
    
}
