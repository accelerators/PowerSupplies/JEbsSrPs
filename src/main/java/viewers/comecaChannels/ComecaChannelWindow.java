/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewers.comecaChannels;

import jebssrps.*;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.Device;
import fr.esrf.tangoatk.core.DeviceFactory;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.ICommand;
import fr.esrf.tangoatk.core.IDevStateScalar;
import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.core.INumberScalar;
import fr.esrf.tangoatk.core.IResultListener;
import fr.esrf.tangoatk.core.IStringScalar;
import fr.esrf.tangoatk.core.ResultEvent;
import fr.esrf.tangoatk.core.command.ScalarArrayCommand;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author poncet
 */
public class ComecaChannelWindow extends javax.swing.JFrame
{
    private String                   comecaChannelDevName = null;
    private Device                   comecaChannelDevice = null;
    private AttributeList            attl = new AttributeList();
    private AttributeList            scalAttl = new AttributeList();
    private final CommandList        cmdl = new CommandList();
    
    private IDevStateScalar          stateAtt = null;
    private IStringScalar            statusAtt = null;
    private ICommand                 resetCmd = null, onCmd = null, offCmd = null;
    
    private final int                rowId;
    
    private atkpanel.MainPanel       comecaChannelAtkPanel=null;

    /**
     * Creates new form ComecaChannelWindow
     */
    public ComecaChannelWindow(String  comecaDevName) throws IllegalArgumentException
    {        
        initComponents();
        
        if (comecaDevName == null)
        {
            throw new IllegalArgumentException("ComecaChannelWindow failed : devname null\n"); 
        }
        
        comecaChannelDevName = comecaDevName;
        
        try
        {
            comecaChannelDevice = DeviceFactory.getInstance().getDevice(comecaChannelDevName);
        }
        catch (ConnectionException ex)
        {
            throw new IllegalArgumentException("ComecaChannelWindow failed : devname null\nCannot connect to the Comeca Channel device.\nFix the problem and restart the application.\n"); 
        }
        
        rowId = -1;
        
        attl = new AttributeList();
        
        attl.addErrorListener(JEbsSrPsConstants.ERR_H);
        attl.addSetErrorListener(JEbsSrPsConstants.ERR_H);
        attl.addSetErrorListener(ErrorPopup.getInstance());
        cmdl.addErrorListener(JEbsSrPsConstants.ERR_H);
        cmdl.addErrorListener(ErrorPopup.getInstance());
        
        connectToAttributesAndCommands();
        
        if (!scalAttl.isEmpty())
            scalarListViewer1.setModel(scalAttl);

        attl.startRefresher();
        setPreferredSize(new java.awt.Dimension(470, 300));
        pack();
        setVisible(true);
    }
    
    
    public ComecaChannelWindow(String  comecaDevName, int row) throws IllegalArgumentException
    {        
        initComponents();
        
        if (comecaDevName == null)
        {
            throw new IllegalArgumentException("ComecaChannelWindow failed : devname null\n"); 
        }
        
        comecaChannelDevName = comecaDevName;
        
        try
        {
            comecaChannelDevice = DeviceFactory.getInstance().getDevice(comecaChannelDevName);
        }
        catch (ConnectionException ex)
        {
            throw new IllegalArgumentException("ComecaChannelWindow failed : devname null\nCannot connect to the Comeca Channel device.\nFix the problem and restart the application.\n"); 
        }
        
        rowId = row;
        
        attl = new AttributeList();
        
        attl.addErrorListener(JEbsSrPsConstants.ERR_H);
        attl.addSetErrorListener(JEbsSrPsConstants.ERR_H);
        attl.addSetErrorListener(ErrorPopup.getInstance());
        cmdl.addErrorListener(JEbsSrPsConstants.ERR_H);
        cmdl.addErrorListener(ErrorPopup.getInstance());
        
        connectToAttributesAndCommands();
        
        if (!scalAttl.isEmpty())
            scalarListViewer1.setModel(scalAttl);

        attl.startRefresher();
        setPreferredSize(new java.awt.Dimension(470, 300));
        pack();
        setVisible(true);
    }
    
    int getRowId()
    {
        return rowId;
    }
    
    private void closeAtkPanel()
    {
        if (comecaChannelAtkPanel == null) return;
        
        comecaChannelAtkPanel.getToolkit().getSystemEventQueue().postEvent(new WindowEvent(comecaChannelAtkPanel, WindowEvent.WINDOW_CLOSING));
        comecaChannelAtkPanel = null;
    }


    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        stateViewer1 = new fr.esrf.tangoatk.widget.attribute.StateViewer();
        statusViewer1 = new fr.esrf.tangoatk.widget.attribute.StatusViewer();
        cmdJPanel = new javax.swing.JPanel();
        onVVcmdv = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
        offVVcmdv = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
        resetVVcmdv = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
        expertJButton = new javax.swing.JButton();
        scalarListViewer1 = new fr.esrf.tangoatk.widget.attribute.ScalarListViewer();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Comeca Window");
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanel1.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel1.add(stateViewer1, gridBagConstraints);

        statusViewer1.setColumns(20);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel1.add(statusViewer1, gridBagConstraints);

        cmdJPanel.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        cmdJPanel.add(onVVcmdv, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        cmdJPanel.add(offVVcmdv, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        cmdJPanel.add(resetVVcmdv, gridBagConstraints);

        expertJButton.setText("Expert Panel ...");
        expertJButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                expertJButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        cmdJPanel.add(expertJButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel1.add(cmdJPanel, gridBagConstraints);

        scalarListViewer1.setPropertyButtonVisible(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        jPanel1.add(scalarListViewer1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        getContentPane().add(jPanel1, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        // TODO add your handling code here:
        
        attl.stopRefresher();
        
        stateViewer1.clearModel();
        statusViewer1.clearModel();
        
        onVVcmdv.setModel((ICommand) null);
        offVVcmdv.setModel((ICommand) null);
        resetVVcmdv.setModel((ICommand) null);
        
        attl.clear();
        cmdl.clear();
        
        closeAtkPanel();
        
        dispose();
        
    }//GEN-LAST:event_formWindowClosing

    private void expertJButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_expertJButtonActionPerformed
    {//GEN-HEADEREND:event_expertJButtonActionPerformed
        // TODO add your handling code here:

        if ( (comecaChannelDevice==null) || (comecaChannelDevName==null) )
        return;
        if (comecaChannelAtkPanel==null)
        {
            launchComecaAtkPanel();
            return;
        }

        if (comecaChannelAtkPanel==null) return;

        comecaChannelAtkPanel.setVisible(true);
        comecaChannelAtkPanel.toFront();

    }//GEN-LAST:event_expertJButtonActionPerformed



    private void launchComecaAtkPanel()
    {
        comecaChannelAtkPanel = new atkpanel.MainPanel(comecaChannelDevName);
        
        if (comecaChannelAtkPanel == null) return;
        
        comecaChannelAtkPanel.addWindowListener(
                new java.awt.event.WindowAdapter()
                   {
                      public void windowClosed(java.awt.event.WindowEvent evt)
                      {
                          comecaAtkPanelExited();
                      }
                   });
        comecaChannelAtkPanel.setExpertView(true);
        ATKGraphicsUtils.centerFrame(expertJButton, comecaChannelAtkPanel);
        comecaChannelAtkPanel.toFront();
    }
    
    private void comecaAtkPanelExited()
    {
        comecaChannelAtkPanel=null;
    }
    
    
    private void closeComecaAtkPanel()
    {
        if (comecaChannelAtkPanel == null) return;
        comecaChannelAtkPanel.dispatchEvent(new java.awt.event.WindowEvent(comecaChannelAtkPanel, java.awt.event.WindowEvent.WINDOW_CLOSING));
        comecaChannelAtkPanel = null;
    }

    private void connectToAttributesAndCommands()
    {
        String msgatt = "Cannot connect to the attribute : ";
        String msgcmd = "Cannot connect to the command : ";
        String msg = null;
        String entityname;
        IEntity oneEntity;


        // Connect to the Comeca Channel device attributes
        try
        {
            entityname = comecaChannelDevName + "/State";
            msg = msgatt + entityname;
            oneEntity = attl.add(entityname);
            if (oneEntity instanceof IDevStateScalar)
            {
                stateAtt = (IDevStateScalar) oneEntity;
                stateViewer1.setModel(stateAtt);
            }
            
            entityname = comecaChannelDevName + "/Status";
            msg = msgatt + entityname;
            oneEntity = attl.add(entityname);
            if (oneEntity instanceof IStringScalar)
            {
                statusAtt = (IStringScalar) oneEntity;
                statusViewer1.setModel(statusAtt);
            }
            
            entityname = comecaChannelDevName + "/Current";
            msg = msgatt + entityname;
            oneEntity = attl.add(entityname);
            if (oneEntity instanceof INumberScalar)
            {
                scalAttl.add(oneEntity);
            }
            
            entityname = comecaChannelDevName + "/Voltage";
            msg = msgatt + entityname;
            oneEntity = attl.add(entityname);
            if (oneEntity instanceof INumberScalar)
            {
                scalAttl.add(oneEntity);
            }
        } 
        catch (ConnectionException ex)
        {
            javax.swing.JOptionPane.showMessageDialog(null, msg + " .\n"
                    + "Check if the device " + comecaChannelDevName + " is responding?\n",
                    "ComecaChannelWindow : Attribute connection",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        }


        // Connect to the Comeca device commands
        try
        {
            entityname = comecaChannelDevName + "/On";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand)
            {
                onCmd = (ICommand) oneEntity;
                onVVcmdv.setModel(onCmd);
            }
            
            entityname = comecaChannelDevName + "/Off";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand)
            {
                offCmd = (ICommand) oneEntity;
                offVVcmdv.setModel(offCmd);
            }
            
            entityname = comecaChannelDevName + "/Reset";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand)
            {
                resetCmd = (ICommand) oneEntity;
                resetVVcmdv.setModel(resetCmd);
            }
        } 
        catch (ConnectionException ex)
        {
            javax.swing.JOptionPane.showMessageDialog(null, msg + " .\n"
                    + "Check if the device " + comecaChannelDevName + " is responding?\n",
                    "ComecaChannelWindow : Command connection",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        }
        
    }

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(ComecaChannelWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(ComecaChannelWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(ComecaChannelWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(ComecaChannelWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new ComecaChannelWindow("srmag/ps-qf6/c32-b", 0).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel cmdJPanel;
    private javax.swing.JButton expertJButton;
    private javax.swing.JPanel jPanel1;
    private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer offVVcmdv;
    private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer onVVcmdv;
    private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer resetVVcmdv;
    private fr.esrf.tangoatk.widget.attribute.ScalarListViewer scalarListViewer1;
    private fr.esrf.tangoatk.widget.attribute.StateViewer stateViewer1;
    private fr.esrf.tangoatk.widget.attribute.StatusViewer statusViewer1;
    // End of variables declaration//GEN-END:variables

}
