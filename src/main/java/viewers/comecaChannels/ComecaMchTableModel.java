/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewers.comecaChannels;

import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.ICommand;
import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.core.ISpectrumListener;
import fr.esrf.tangoatk.core.IStringSpectrum;
import fr.esrf.tangoatk.core.IStringSpectrumListener;
import fr.esrf.tangoatk.core.NumberSpectrumEvent;
import fr.esrf.tangoatk.core.StringSpectrumEvent;
import fr.esrf.tangoatk.core.attribute.NumberSpectrum;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author poncet
 */
public class ComecaMchTableModel extends DefaultTableModel
                                  implements ISpectrumListener, IStringSpectrumListener
{
    private boolean            cmd_init = false;
    private final CommandList  cmdl = new CommandList();
    
    static final int           CHANNEL_NB = 33;
    static final String        CHANNEL_NO_NAME = "???";
    static final String        DETAIL_LABEL = "...";
    static final String[]      COMECA_COLNAMES = { "", "Current", "Voltage", "", "", ""};
    static final String[]      COMECA_ATT_CMD_NAMES = {"", "Current", "Voltage", "On", "Off", DETAIL_LABEL};
    
    static final int           CURRENT_COLUMN = 1;
    static final int           VOLTAGE_COLUMN = 2;
    static final int           ON_COLUMN = 3;
    static final int           OFF_COLUMN = 4;
    static final int           DETAIL_COLUMN = 5;
    
    private String[]       attFormat = new String[COMECA_COLNAMES.length];
    private String[]       attUnit = new String[COMECA_COLNAMES.length];
    private Object[][]     tableData;
    private String[]       channelDevNames;


    private INumberSpectrum     currentModel = null;
    private INumberSpectrum     voltageModel = null;
    private IStringSpectrum     nameModel = null;
    
    public ComecaMchTableModel()
    {
        cmdl.addErrorListener(jebssrps.JEbsSrPsConstants.ERR_H);
        cmdl.addErrorListener(ErrorPopup.getInstance());
        
        tableData = new Object[CHANNEL_NB][COMECA_COLNAMES.length];
        channelDevNames = new String[CHANNEL_NB];

        for (int i=0; i<CHANNEL_NB; i++)
        {
            for (int j=1; j<COMECA_COLNAMES.length; j++)
            {
                tableData[i][j] = Double.NaN;
                channelDevNames[i] = CHANNEL_NO_NAME;
            }
        }
        for (int i=0; i<CHANNEL_NB; i++)
        {
            tableData[i][0] = CHANNEL_NO_NAME;
        }
        for (int i=0; i<attFormat.length; i++)
        {
            attFormat[i] = null;
        }
        for (int i=0; i<attUnit.length; i++)
        {
            attUnit[i] = null;
        }
        
        this.setDataVector(tableData, COMECA_COLNAMES);        
    }

    

    
    void clearCurrentModel()
    {
        attFormat[CURRENT_COLUMN] = null;
        attUnit[CURRENT_COLUMN] = null;
        if (currentModel != null)
        {
            currentModel.removeSpectrumListener(this);
            currentModel = null;
        }
    }

    void clearVoltageModel()
    {
        attFormat[VOLTAGE_COLUMN] = null;
        attUnit[VOLTAGE_COLUMN] = null;
        if (voltageModel != null)
        {
            voltageModel.removeSpectrumListener(this);
            voltageModel = null;
        }
    }

    void clearNameModel()
    {
        if (nameModel != null)
        {
            nameModel.removeListener(this);
            nameModel = null;
        }
    }

    
    void setCurrentModel(INumberSpectrum  psCurr)
    {
        clearCurrentModel();
        if (psCurr != null)
        {
            attFormat[CURRENT_COLUMN] = psCurr.getFormat();
            attUnit[CURRENT_COLUMN] = psCurr.getUnit();
            currentModel = psCurr;
            currentModel.addSpectrumListener(this);
            currentModel.refresh();
        }
    }
    
    void setVoltageModel(INumberSpectrum  psVolt)
    {
        clearVoltageModel();
        if (psVolt != null)
        {
            attFormat[VOLTAGE_COLUMN] = psVolt.getFormat();
            attUnit[VOLTAGE_COLUMN] = psVolt.getUnit();
            voltageModel = psVolt;
            voltageModel.addSpectrumListener(this);
            voltageModel.refresh();
        }
    }

    void setNameModel(IStringSpectrum stNames)
    {
        clearNameModel();
        if (stNames != null)
        {
            nameModel = stNames;
            nameModel.addListener(this);
            nameModel.refresh();
        }
    }
    
    private ICommand getCommand(String devName, int cmdColIndex)
    {
        if ( (cmdColIndex != ON_COLUMN) && (cmdColIndex != OFF_COLUMN) )
            return null;
        
        ICommand ic = null;
        try
        {
            IEntity ie = cmdl.add(devName + "/" + COMECA_ATT_CMD_NAMES[cmdColIndex]);
            if (ie instanceof VoidVoidCommand)
                ic = (ICommand) ie;
        }
        catch (ConnectionException ce) {}
        
        return ic;
    }
    
    private void addButtons()
    {
        cmd_init = true;
//        channelDevNames[0] = "jlp/test/1";
//        channelDevNames[1] = "jlp/test/2";
        
        for (int i=0; i<channelDevNames.length; i++)
        {
            ICommand  cmd = getCommand(channelDevNames[i],ON_COLUMN);
            if (cmd != null)
            {
                VoidVoidCommandViewer  vvcmdv = new VoidVoidCommandViewer();
                vvcmdv.setModel(cmd);
                tableData[i][ON_COLUMN] = vvcmdv;
                setValueAt(tableData[i][ON_COLUMN], i, ON_COLUMN);
            }
            
            cmd = getCommand(channelDevNames[i],OFF_COLUMN);
            if (cmd != null)
            {
                VoidVoidCommandViewer  vvcmdv = new VoidVoidCommandViewer();
                vvcmdv.setModel(cmd);
                tableData[i][OFF_COLUMN] = vvcmdv;
                setValueAt(tableData[i][OFF_COLUMN], i, OFF_COLUMN);
            }
            
            String detailStr = new String(DETAIL_LABEL);
            tableData[i][DETAIL_COLUMN] = detailStr;
            setValueAt(tableData[i][DETAIL_COLUMN], i, DETAIL_COLUMN);
        }
        fireTableDataChanged();
    }
    
    
    private void updateCurrents(NumberSpectrumEvent nse)
    {
        int        row, col;

        double[]   readValues = nse.getValue();
        
        for (int i=0; i<readValues.length; i++)
        {
            row = i;
            col = CURRENT_COLUMN;
            if (row >= CHANNEL_NB)
                continue;
            tableData[row][col] = readValues[i];
            setValueAt(tableData[row][col],row,col);
        }
        fireTableDataChanged();
    }

    private void updateVoltages(NumberSpectrumEvent nse)
    {
        int        row, col;

        double[]   readValues = nse.getValue();
        
        for (int i=0; i<readValues.length; i++)
        {
            row = i;
            col = VOLTAGE_COLUMN;
            if (row >= CHANNEL_NB)
                continue;
            tableData[row][col] = readValues[i];
            setValueAt(tableData[row][col],row,col);
        }
        fireTableDataChanged();
    }

    
    
    @Override
    public void spectrumChange(NumberSpectrumEvent nse)
    {
        if (nse.getSource() == currentModel)
        {
            updateCurrents(nse);
        }
        if (nse.getSource() == voltageModel)
        {
            updateVoltages(nse);
        }
    }

    @Override
    public void stringSpectrumChange(StringSpectrumEvent sse)
    {
        int  row, col;
        String[]   val = sse.getValue();

        col = 0;
        for (int i=0; i<val.length; i++)
        {
            row = i;
            if (i >= CHANNEL_NB)
                continue;
            channelDevNames[i] = val[i];
            String psName = getPsName(channelDevNames[i]);
            tableData[row][col] = psName;
            setValueAt(tableData[row][col],row,col);
        }
        fireTableDataChanged();
        
        if (!cmd_init)
        {
            addButtons();
        }
    }

    private String getPsName(String devName)
    {
        int  firstDashIndex;
        int  lastDashIndex;
        lastDashIndex = devName.lastIndexOf("-");
        if (lastDashIndex < 0) return devName;
        
        String lastPart = devName.substring(lastDashIndex+1);
        firstDashIndex = devName.indexOf("-");
        if (firstDashIndex == lastDashIndex) return devName;
        
        int lastSlashIndex = devName.lastIndexOf("/");        
        if (lastSlashIndex < firstDashIndex) return devName;
        
        String firstPart = devName.substring(firstDashIndex+1, lastSlashIndex);
        
        String psName = firstPart.concat(lastPart);
        
        return psName.toUpperCase();
    }
    
    
    @Override
    public void stateChange(AttributeStateEvent ase)
    {
    }

    @Override
    public void errorChange(ErrorEvent ee)
    {
        
        if (ee.getSource() == currentModel)
        {
            for (int i=0; i<CHANNEL_NB; i++)
            {
                tableData[i][CURRENT_COLUMN] = Double.NaN;
            }
            fireTableDataChanged();
            return;
        }
        
        if (ee.getSource() == nameModel)
        {
            for (int i=0; i<CHANNEL_NB; i++)
            {
                channelDevNames[i] = CHANNEL_NO_NAME;
            }
        }
    }
    
    String getAttFormat(int attCol)
    {
        return attFormat[attCol];
    }
    
    String getAttUnit(int attCol)
    {
        return attUnit[attCol];
    }
    
    
    String getDevName(int row, int col)
    {
        if ((row < 0) || (row >= CHANNEL_NB )) return CHANNEL_NO_NAME;
        if (col == 0) return CHANNEL_NO_NAME;

        return (channelDevNames[row]);
    }
    
}
