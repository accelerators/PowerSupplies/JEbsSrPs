package viewers;

import fr.esrf.tangoatk.core.IStringScalarListener;
import fr.esrf.tangoatk.core.StringScalarEvent;
import fr.esrf.tangoatk.core.attribute.StringScalar;
import fr.esrf.tangoatk.widget.attribute.SimpleStateViewer;
import java.awt.Color;

public class PSStateViewer extends SimpleStateViewer implements IStringScalarListener {
    
    String psName;
    
    public PSStateViewer(String psDevName) {
        
        super();
        int i1 = psDevName.indexOf("-");
        int i2 = psDevName.lastIndexOf("-");
        int i3 = psDevName.lastIndexOf("/");

        if (psDevName.toLowerCase().contains("dqspare")) {
            psName = "DQspare";
        } else if (psDevName.toLowerCase().contains("qspare")) {
            if (i2 != -1) {
                psName = "Qspare" + psDevName.charAt(i2 + 1);
            } else {
                psName = "Qspare";
            }
        } else if (psDevName.toLowerCase().contains("spare")) {
            psName = "SOspare";
        } else {
            psName = psDevName.substring(i1+1,i3);
            psName += psDevName.substring(i2+1);
            psName = psName.toUpperCase();
        }
        setText(psName);
        
    }
    
    public void setNameModel(StringScalar model) {
        model.addStringScalarListener(this);
    }

    @Override
    public void stringScalarChange(StringScalarEvent sse) {
        String cName = sse.getValue();
        if(!cName.equalsIgnoreCase(psName) && 
           !cName.equalsIgnoreCase("NONE")) {
          setText(psName+" = "+cName);
        } else {
          setText(psName);            
        }
        if(!cName.equalsIgnoreCase("NONE")) {
            setForeground(Color.BLACK);
        } else {
            setForeground(Color.LIGHT_GRAY);            
        }
    }
    
    
    
}
