/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewers;

import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.IImageListener;
import fr.esrf.tangoatk.core.INumberImage;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.core.ISpectrumListener;
import fr.esrf.tangoatk.core.NumberImageEvent;
import fr.esrf.tangoatk.core.NumberSpectrumEvent;
import fr.esrf.tangoatk.widget.util.ATKFormat;
import fr.esrf.tangoatk.widget.util.chart.IJLChartListener;
import fr.esrf.tangoatk.widget.util.chart.JLAxis;
import fr.esrf.tangoatk.widget.util.chart.JLChart;
import fr.esrf.tangoatk.widget.util.chart.JLChartEvent;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JFrame;

/**
 *
 * @author poncet
 */
public class MosPlateDataViewer extends JLChart
                                implements IImageListener, ISpectrumListener, IJLChartListener
{
    // 27 magnet labels in the order they appear in the data (image)
    private  String[]     MAG_LABELS;

    public static String[]   MAG_LABELS_ALL_CELLS = { "DQ1B", "DQ2C", "DQ1D",
                                                  "QF1A", "QD2A", "QD3A",
                                                  "QF4A", "QF4B", "QD5B",
                                                  "QF6B", "QF8B", "QF8D",
                                                  "QF6D", "QD5D", "QF4D",
                                                  "QF4E", "QD3E", "QD2E",
                                                  "QF1E", "SD1A", "SF2A",
                                                  "SD1B", "SD1D", "SF2E",
                                                  "SD1E", "OF1B", "OF1D"
                                                };
    public  static final int       NB_MAGNETS = MAG_LABELS_ALL_CELLS.length;
    
    public static String[]   MAG_LABELS_C3 = { "DQ1B", "DQ2C", "DQ1D",
                                                  "QF1A", "QD2A", "QD3A",
                                                  "QF4A", "QF4B", "QD5B",
                                                  "QF6B", "QF8B", "QF8D",
                                                  "QF6D", "QD5D", "QF4D",
                                                  "QF4E", "QF2E", "QD2E",
                                                  "QD3E", "SD1A", "SF2A",
                                                  "SD1B", "SD1D", "SF2E",
                                                  "SD1E", "OF1B", "OF1D"
                                                };
    public static String[]   MAG_LABELS_C4 = { "DQ1B", "DQ2C", "DQ1D",
                                                  "QD3A", "QD2A", "QF2A",
                                                  "QF4A", "QF4B", "QD5B",
                                                  "QF6B", "QF8B", "QF8D",
                                                  "QF6D", "QD5D", "QF4D",
                                                  "QF4E", "QD3E", "QD2E",
                                                  "QF1E", "SD1A", "SF2A",
                                                  "SD1B", "SD1D", "SF2E",
                                                  "SD1E", "OF1B", "OF1D"
                                                };
    
    
    private INumberImage           mosDataModel = null;
    private INumberSpectrum        mosDataDateModel = null;
    
    private JLDataView[]           allDvs = new JLDataView[NB_MAGNETS];
    
    private double[]           indexTransform = new double[0];
    
    /**
     *
     */
    public MosPlateDataViewer()
    {
        setBackground(Color.white);
        setChartBackground(Color.white);
        getY1Axis().setGridVisible(true);
        getY1Axis().setAutoScale(true);
        getXAxis().setZeroAlwaysVisible(true);
        getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
        
        for (int i=0; i<allDvs.length; i++)
            allDvs[i] = null;
        
        MAG_LABELS = MAG_LABELS_ALL_CELLS;
        setJLChartListener(this);
    }
    
    public void clearModel()
    {
        if ( mosDataDateModel != null){
            mosDataDateModel.removeSpectrumListener(this);
            mosDataDateModel = null;
        }
        
        if (mosDataModel == null) return;
        
        mosDataModel.removeImageListener(this);
        
        mosDataModel = null;
        MAG_LABELS = MAG_LABELS_ALL_CELLS;        
       
        for (int i=0; i<allDvs.length; i++)
            allDvs[i] = null;
    }
    
    public void setModel(INumberImage mosDataAtt,INumberSpectrum mosDataDateAtt)
    {
        clearModel();
        if (mosDataAtt == null) return;
                
        mosDataModel = mosDataAtt;
        mosDataDateModel = mosDataDateAtt;
        
        int index = mosDataAtt.getDeviceName().lastIndexOf("/");
        String cellStr = mosDataAtt.getDeviceName().substring(index+2);
        
        this.setHeaderFont(new java.awt.Font("Dialog", 1, 12));
        this.setHeader("Cell "+cellStr);
        
        if (cellStr.equals("03"))
           MAG_LABELS = MAG_LABELS_C3; 
        if (cellStr.equals("04"))
           MAG_LABELS = MAG_LABELS_C4;
        
        createDataViews();
       
        mosDataDateModel.addSpectrumListener(this);
        mosDataDateAtt.refresh();
        
        this.getY1Axis().setName("Magnet current ("+ mosDataModel.getUnit()+")");
        this.getXAxis().setName("Time with origin is swap trigger ("+ mosDataDateModel.getUnit()+")");
        
        mosDataModel.addImageListener(this);
        mosDataModel.refresh();
    }
    
    private void createDataViews()
    {        
        for (int i=0; i<allDvs.length; i++)
        {
            JLDataView  dv = new JLDataView();
//            dv.setColor(xx);
//            dv.setMarkerColor(xx);
//            dv.setMarker(JLDataView.MARKER_BOX);
//            dv.setMarkerSize(4);
            dv.setName(MAG_LABELS[i]);
            allDvs[i] = dv;
        }
        
        // By default the 1st dataview to the chart
        getY1Axis().addDataView(allDvs[0]);
    }
    
    public void updateChart(String magName, Boolean add)
    {
        if (add) {
            addMagnetToChart(magName);
        }
        else {
            removeMagnetFromChart(magName);
        }
    }
    
    private int getMagnetIndex(String magName)
    {
        int i = -1;
        
        for (i=0; i<NB_MAGNETS; i++)
            if (magName.equals(MAG_LABELS[i]))
                break;
        
        return i;
    }
    
    private void addMagnetToChart(String magName)
    {
        int magNumber = getMagnetIndex(magName);
        
        if (magNumber >=0)
            this.getY1Axis().addDataView(allDvs[magNumber]);
        
        int dvNb = getY1Axis().getViewNumber();
        for (int i=0; i<dvNb; i++)
        {
            JLDataView dv = getY1Axis().getDataView(i);
            int  colorIndex = i%jebssrps.JEbsSrPsConstants.MOSPLATE_DATAVIEW_COLORS.length;
            dv.setColor(jebssrps.JEbsSrPsConstants.MOSPLATE_DATAVIEW_COLORS[colorIndex]);
        }
        repaint();
    }
    
    private void removeMagnetFromChart(String magName)
    {
        int magNumber = getMagnetIndex(magName);
        
        if (magNumber >=0)
            this.getY1Axis().removeDataView(allDvs[magNumber]);
        repaint();
    }
    
    
    private void updateDataView(int magNumber, double[] magData)
    {
        JLDataView dv = allDvs[magNumber];
        dv.reset();
        
        for (int i=0; i<magData.length; i++)
        {
            dv.add(indexTransform[i], magData[i]);
        }
        repaint();
    }
    
    @Override
    public void imageChange(NumberImageEvent nie)
    { 
        if (nie.getSource() != mosDataModel) return;
        
        if (nie.getValue() == null || indexTransform.length == 0) return;
        double[][] mosData = nie.getValue();
        
        for (int row=0; row<NB_MAGNETS; row++)
        {
            updateDataView(row, mosData[row]);
        }
    }

    @Override
    public void stateChange(AttributeStateEvent ase)
    {
    }

    @Override
    public void errorChange(ErrorEvent ee)
    {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                JFrame  jf = new JFrame();
                jf.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                
                MosPlateDataViewer    mpdv = new MosPlateDataViewer();
                
                jf.setContentPane(mpdv);
                jf.setPreferredSize(new Dimension(400, 200));
                jf.pack();
                jf.setVisible(true);
            }
        });
    }

    @Override
    public void spectrumChange(NumberSpectrumEvent nse) {
        indexTransform = nse.getValue();            
    }

    @Override
    public String[] clickOnChart(JLChartEvent evt) {
        
                
        String [] tooltip = {
            evt.getDataView().getName(),
            ATKFormat.format(mosDataDateModel.getFormat(), evt.getXValue()) + " " + mosDataDateModel.getUnit(),
            ATKFormat.format(mosDataModel.getFormat(), evt.getYValue()) + " " + mosDataModel.getUnit()
                    };
        return tooltip;
        /*super.chartMenu.setT
        evt.get
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody*/
    }
}
