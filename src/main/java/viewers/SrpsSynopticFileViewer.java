/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewers;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributePolledList;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.core.IStringScalarListener;
import fr.esrf.tangoatk.core.StringScalarEvent;
import fr.esrf.tangoatk.core.attribute.AAttribute;
import fr.esrf.tangoatk.core.attribute.BooleanScalar;
import fr.esrf.tangoatk.core.attribute.StringScalar;
import fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer;
import fr.esrf.tangoatk.widget.jdraw.TangoSynopticHandler;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import fr.esrf.tangoatk.widget.util.jdraw.JDLabel;
import fr.esrf.tangoatk.widget.util.jdraw.JDMouseAdapter;
import fr.esrf.tangoatk.widget.util.jdraw.JDMouseEvent;
import fr.esrf.tangoatk.widget.util.jdraw.JDObject;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import jebssrps.JEbsSrPsConstants;

/**
 *
 * @author poncet
 */
public class SrpsSynopticFileViewer extends SynopticFileViewer implements IStringScalarListener
{
    public final static int TYPE_HSC       = 1;
    public final static int TYPE_INHIB = 2;
    
    AttributePolledList       channelConnectionAttl = new AttributePolledList();
    
    private int     synopticType;
    
    private HashMap<StringScalar, String>  connectedStateMap = new HashMap<StringScalar, String> ();
    
    private JPopupMenu  swapInhibitMenu = null;
    private JMenuItem   enableSwapMenuItem = null;
    private JMenuItem   disableSwapMenuItem = null;

    private JPopupMenu  hsmInhibitMenu = null;
    private JMenuItem   hsmInhibMenuItem = null;
    private JMenuItem   hsmEnableMenuItem = null;
    
    private JDObject              clickedObject=null;
    private ArrayList<JDObject>   swapInhibitObjects=null;
    private ArrayList<JDObject>   hsmInhibitObjects=null;
    
    Object     monitor = new Object();

    
    public SrpsSynopticFileViewer(int type)
    {
        super();
        synopticType = type;
        channelConnectionAttl.addErrorListener(this.getErrorHistoryWindow());
        channelConnectionAttl.addErrorListener(this);
    }

    
    /**
     * Load a jdraw grpahics input stream into the drawing area. This method
     * allows to load a synoptic which is not necessarily a file on disk. This
     * method is particularly used when the synoptic jdraw file is pakaged into
     * the application jarfile and cannot be accessed as a separate file on the
     * disk.
     *
     * @param templateInputStream An InputStreamReader should be created by the
     * application and passed to this method
     * @throws IOException Exception when the inputStream cannot be accessed.
     * @throws MissingResourceException when the "jdraw" inputStream cannot be
     * parsed.
     */
    public void loadTemplateSynopticFromStream(InputStreamReader templateInputStream, String cell) throws IOException, MissingResourceException
    {
        jdHash = new HashMap<String, List<JDObject>>();
        stateCashHash = new HashMap<String, List<String>>();
        // Here should disconnect from all attributes and devices in the previous
        // Jdraw file.

        loadFromStream(templateInputStream);
        jdrawFileFullName = "TemplateInputStreamReader";
        if (getObjectNumber() == 0)
        {
            throw new MissingResourceException(
                    "The Jdraw file has no component inside. First draw a Jdraw File.",
                    "JDrawEditor", null);
        }

        if (synopticType==TYPE_HSC)
        {
            replaceCubicleTitle(cell);
            replaceHsmName(cell);
            replaceHscItlkName(cell);
            replaceMagItlkName(cell);
            replaceComecaRackNames(cell);
            replaceComecaPoeNames(cell);
            replaceComecaChannelNames(cell);
        }
        else
        {
            replaceCubicleTitle(cell);
            replaceInhibitedNames(cell);
            addJPopupMenu();
        }
        
        
        parseJdrawComponents();
        computePreferredSize();
        if (synopticType!=TYPE_HSC)
            addErrorListeners();
        
        // We need to refresh all attributes because if several JDObject
        // has the same model , only the first one get the event. (API
        // specification )
        if (allAttributes.size() > 0)
        {
            for (int i = 0; i < allAttributes.size(); i++)
            {
                ((AAttribute) allAttributes.elementAt(i)).refresh();
            }
        }
        
        if (channelConnectionAttl.size() > 0)
        {
            for (int i = 0; i < channelConnectionAttl.size(); i++)
            {
                ((AAttribute) channelConnectionAttl.elementAt(i)).refresh();
            }
        }
        allAttributes.startRefresher();
        channelConnectionAttl.startRefresher();
    }
    
    
    
    private void mouseifySWapInhibitedAttribute(JDObject jdo)
    {
        /* Attach a JDMouse listener to inhibited att */
        jdo.addMouseListener(
                new JDMouseAdapter()
        {
            public void mouseClicked(JDMouseEvent e)
            {
                swapInhibitedClicked(e, jdo);
            }
        });
    }
    private void mouseifyHSMInhibitedAttribute(JDObject jdo)
    {
        /* Attach a JDMouse listener to inhibited att */
        jdo.addMouseListener(
                new JDMouseAdapter()
        {
            public void mouseClicked(JDMouseEvent e)
            {
                hsmInhibitedClicked(e, jdo);
            }
        });
    }
    
    private void addJPopupMenu()
    {
        // Auto swap
        swapInhibitMenu = new JPopupMenu();
        
        enableSwapMenuItem = new JMenuItem("Enable Auto Swap");
        enableSwapMenuItem.setEnabled(true);
        swapInhibitMenu.add(enableSwapMenuItem);
        
        disableSwapMenuItem = new JMenuItem("Disable Auto Swap");
        disableSwapMenuItem.setEnabled(true);
        swapInhibitMenu.add(disableSwapMenuItem);
        
        enableSwapMenuItem.addActionListener(new ActionListener()
                                                {
                                                   public void actionPerformed(ActionEvent ae)
                                                   {
                                                       setBooleanAtt(ae,false);
                                                   }
                                                }
                                            );
        
        disableSwapMenuItem.addActionListener(new ActionListener()
                                                {
                                                   public void actionPerformed(ActionEvent ae)
                                                   {
                                                       setBooleanAtt(ae,true);
                                                   }
                                                }
                                            );
        
        // Spare HSM Inhib
        hsmInhibitMenu = new JPopupMenu();
        
        hsmInhibMenuItem = new JMenuItem("Inhib from HSM");
        hsmInhibMenuItem.setEnabled(true);
        hsmInhibitMenu.add(hsmInhibMenuItem);
        
        hsmEnableMenuItem = new JMenuItem("Enable");
        hsmEnableMenuItem.setEnabled(true);
        hsmInhibitMenu.add(hsmEnableMenuItem);
        
        hsmInhibMenuItem.addActionListener(new ActionListener()
                                                {
                                                   public void actionPerformed(ActionEvent ae)
                                                   {
                                                       setBooleanAtt(ae,true);
                                                   }
                                                }
                                            );
        
        hsmEnableMenuItem.addActionListener(new ActionListener()
                                                {
                                                   public void actionPerformed(ActionEvent ae)
                                                   {
                                                       setBooleanAtt(ae,false);
                                                   }
                                                }
                                            );

    }
    
    private void addErrorListeners()
    {
        for (int i=0; i<swapInhibitObjects.size(); i++)
        {
            JDObject jdo = swapInhibitObjects.get(i);
            String   attName = jdo.getName();
            if (attName == null) continue;
            
            AttributeList  hsmAttl = super.getAttributeList();
            IEntity  ie = hsmAttl.get(attName);
            if (ie == null) continue;
            
            if (!(ie instanceof BooleanScalar)) continue;
            
            BooleanScalar bs = (BooleanScalar) ie;
            bs.addErrorListener(JEbsSrPsConstants.ERR_H);
            bs.addSetErrorListener(JEbsSrPsConstants.ERR_H);
            bs.addSetErrorListener(ErrorPopup.getInstance());
        }                
        for (int i=0; i<hsmInhibitObjects.size(); i++)
        {
            JDObject jdo = hsmInhibitObjects.get(i);
            String   attName = jdo.getName();
            if (attName == null) continue;
            
            AttributeList  hsmAttl = super.getAttributeList();
            IEntity  ie = hsmAttl.get(attName);
            if (ie == null) continue;
            
            if (!(ie instanceof BooleanScalar)) continue;
            
            BooleanScalar bs = (BooleanScalar) ie;
            bs.addErrorListener(JEbsSrPsConstants.ERR_H);
            bs.addSetErrorListener(JEbsSrPsConstants.ERR_H);
            bs.addSetErrorListener(ErrorPopup.getInstance());
        }                
        
    }
    
    private void setBooleanAtt(ActionEvent ae, boolean value) {

        if (clickedObject != null) {
            String name = clickedObject.getName();
            AttributeList hsmAttl = super.getAttributeList();
            IEntity ie = hsmAttl.get(name);
            if (ie == null) {
                return;
            }
            if (!(ie instanceof BooleanScalar)) {
                return;
            }
            BooleanScalar bs = (BooleanScalar) ie;
            bs.setValue(value);
        }

    }
        
    public void swapInhibitedClicked(JDMouseEvent e, JDObject clickedJdo)
    {
        if (synopticType==TYPE_HSC) return;
        if (clickedJdo == null) return;
        
        clickedObject = clickedJdo;
        swapInhibitMenu.show(this, e.getX(), e.getY());
    }
    
    public void hsmInhibitedClicked(JDMouseEvent e, JDObject clickedJdo)
    {
        if (synopticType==TYPE_HSC) return;
        if (clickedJdo == null) return;
        
        clickedObject = clickedJdo;
        hsmInhibitMenu.show(this, e.getX(), e.getY());
    }

    
    private void replaceCubicleTitle(String cellNb)
    {
        Vector objVect = null;
        objVect = getObjectsByName("cubicle-title", false);
        
        if (objVect.isEmpty()) return;
        for (int i = 0; i < objVect.size(); i++)
        {
            Object obj = objVect.get(i);
            if (!(obj instanceof JDLabel))
            {
                continue;
            }
            JDLabel jdl = (JDLabel) obj;
            String currentText = jdl.getText();
            String newText = currentText.replace("##", cellNb);
            
//            System.out.println("replaceCubicleTitle : currentText = "+currentText+" newText = "+newText);
            jdl.setText(newText);
        }
        objVect.clear();
        objVect = null;
    }
    
    
    private void replaceHsmName(String cellNb)
    {
        Vector objVect = null;
        objVect = getObjectsByName("srmag/hsm/c##/State", false);
        
        if (objVect.isEmpty()) return;
        for (int i = 0; i < objVect.size(); i++)
        {
            Object obj = objVect.get(i);
            if (!(obj instanceof JDObject))
            {
                continue;
            }
            JDObject jdo = (JDObject) obj;
            String currentName = jdo.getName();
            String newName = currentName.replace("##", cellNb);
            
//            System.out.println("replaceHsmName : currentName = "+currentName+" newName = "+newName);
            jdo.setName(newName);
        }
        objVect.clear();
        objVect = null;
    }
    
    
    private void replaceHscItlkName(String cellNb)
    {
        Vector objVect = null;
        objVect = getObjectsByName("srmag/hsc-itlk/c##/State", false);
        
        if (objVect.isEmpty()) return;
        for (int i = 0; i < objVect.size(); i++)
        {
            Object obj = objVect.get(i);
            if (!(obj instanceof JDObject))
            {
                continue;
            }
            JDObject jdo = (JDObject) obj;
            String currentName = jdo.getName();
            String newName = currentName.replace("##", cellNb);
            
//            System.out.println("replaceHscItlkName : currentName = "+currentName+" newName = "+newName);
            jdo.setName(newName);
        }
        objVect.clear();
        objVect = null;
    }
    
    
    private void replaceMagItlkName(String cellNb)
    {
        Vector objVect = null;
        objVect = getObjectsByName("srmag/fl-temp-itlk/c##/State", false);
        
        if (objVect.isEmpty()) return;
        for (int i = 0; i < objVect.size(); i++)
        {
            Object obj = objVect.get(i);
            if (!(obj instanceof JDObject))
            {
                continue;
            }
            JDObject jdo = (JDObject) obj;
            String currentName = jdo.getName();
            String newName = currentName.replace("##", cellNb);
            
//            System.out.println("replaceMagItlkName : currentName = "+currentName+" newName = "+newName);
            jdo.setName(newName);
        }
        objVect.clear();
        objVect = null;
    }
    
    
    private void replaceComecaRackNames(String cellNb)
    {
        Vector objVect = null;

        for (int i = 1; i <= 11; i++)  // Loop for 11 racks
        {
            String rackNb = String.format("%02d", i);
            objVect = getObjectsByName("srmag/ps-conv-rack/c##-"+rackNb+"/State", false);
            if (objVect.isEmpty())
            {
                continue;
            }
            for (int j = 0; j < objVect.size(); j++)
            {
                Object obj = objVect.get(j);
                if (!(obj instanceof JDObject))
                {
                    continue;
                }
                JDObject jdo = (JDObject) obj;
                String currentName = jdo.getName();
                String newName = currentName.replace("##", cellNb);
                
//                System.out.println("replaceComecaRackNames : currentName = "+currentName+" newName = "+newName);
                jdo.setName(newName);
            }
            objVect.clear();
            objVect = null;
        }
    }
    
    
    private void replaceComecaPoeNames(String cellNb)
    {
        Vector objVect = null;

        for (int i = 1; i <= 11; i++)  // Loop for 11 racks
        {
            String rackNb = String.format("%02d", i);
            objVect = getObjectsByName("srmag/ps-conv-poe/c##-"+rackNb+"/State", false);
            if (objVect.isEmpty())
            {
                continue;
            }
            for (int j = 0; j < objVect.size(); j++)
            {
                Object obj = objVect.get(j);
                if (!(obj instanceof JDObject))
                {
                    continue;
                }
                JDObject jdo = (JDObject) obj;
                String currentName = jdo.getName();
                String newName = currentName.replace("##", cellNb);
                
//                System.out.println("replaceComecaPoeNames : currentName = "+currentName+" newName = "+newName);
                jdo.setName(newName);
            }
            objVect.clear();
            objVect = null;
        }
    }

    

    private void replaceComecaChannelNames(String cellNb)
    {
        Vector objVect = null;
        for (int i = 0; i < getObjectNumber(); i++)
        {
            JDObject jdObj = getObjectAt(i);
            String currentName = jdObj.getName();
            if (currentName.matches("srmag/ps-(.*)/c##(.*)"))
            {
                String newName = currentName.replace("##", cellNb);
                
//                System.out.println("replaceComecaChannelNames : currentName = "+currentName+" newName = "+newName);
                jdObj.setName(newName);
//                jdObj.addExtension("classParam");
//                jdObj.setExtendedParam("classParam", cellNb);
               
                if (JEbsSrPsConstants.MINI_BETA)
                {
                    if ( cellNb.equalsIgnoreCase("30") || cellNb.equalsIgnoreCase("31") )
                    {
                        String name = newName.toLowerCase();
                        if ( name.contains("ps-qspare") && name.endsWith("-2/state") )
                            changeLabelToMiniBeta(jdObj, cellNb);
                        else
                        {
                            if ( name.contains("ps-sospare") && name.endsWith("-1/state") )
                                changeLabelToMiniBeta(jdObj, cellNb);
                        }
                    }
                }

               // connect to "connected" boolean attribute
                try
                {
                    String     connectedAttName = newName.toLowerCase().replace("/state", "/connectedname");
                    IEntity    ie = channelConnectionAttl.add(connectedAttName);
                    if (ie instanceof StringScalar)
                    {
                        StringScalar  bs = (StringScalar) ie;
                        bs.addStringScalarListener(this);
                        connectedStateMap.put(bs, null);
                    }
                }
                catch (ConnectionException ce) {}                

            }
        }
    }
    
    private void changeLabelToMiniBeta(JDObject jdObj, String cellNb)
    {
        String  objName = jdObj.getName().toLowerCase();
        Vector<JDObject> foundObjs = new Vector<JDObject>();
        
        jdObj.getObjectsByName(foundObjs, "IgnoreRepaint", true);
        if (foundObjs.isEmpty()) return;
        
        JDLabel jdl = (JDLabel) foundObjs.get(0);
        
        if ( objName.contains("ps-qspare") && objName.endsWith("-2/state") )
        {
            if (cellNb.equalsIgnoreCase("30"))
               jdl.setText("QD2-MBE");
            else
                if (cellNb.equalsIgnoreCase("31"))
                   jdl.setText("QD2-MBA");
            return;
        }
        
        if ( objName.contains("ps-sospare") && objName.endsWith("-1/state") )
        {
            if (cellNb.equalsIgnoreCase("30"))
               jdl.setText("QF1-MBE");
            else
                if (cellNb.equalsIgnoreCase("31"))
                   jdl.setText("QF1-MBA");
            return;
        }
    }
    
    

    private void replaceInhibitedNames(String cellNb)
    {
        swapInhibitObjects = new ArrayList<JDObject>();
        hsmInhibitObjects = new ArrayList<JDObject>();
        Vector objVect = null;
        for (int i = 0; i < getObjectNumber(); i++)
        {
            JDObject jdObj = getObjectAt(i);
            String currentName = jdObj.getName();
            
            // Virual PS (magnet)
            if (currentName.matches("srmag/vps-(.*)/c##(.*)"))
            {
                String newName = currentName.replace("##", cellNb);
                jdObj.setName(newName);
                swapInhibitObjects.add(jdObj);                
            }
            Iterator<JDObject> it = swapInhibitObjects.iterator();
            while (it.hasNext())
            {
                JDObject jdo = it.next();
                mouseifySWapInhibitedAttribute(jdo);
            }        

            // Spare ps
            if (currentName.matches("srmag/ps-(.*)/c##(.*)"))
            {
                String newName = currentName.replace("##", cellNb);
                jdObj.setName(newName);
                hsmInhibitObjects.add(jdObj);
                try
                {
                    String     connectedAttName = newName.toLowerCase().replace("/hsminhibited", "/connectedname");
                    IEntity    ie = channelConnectionAttl.add(connectedAttName);
                    if (ie instanceof StringScalar)
                    {
                        StringScalar  bs = (StringScalar) ie;
                        bs.addStringScalarListener(this);
                        connectedStateMap.put(bs, null);
                    }
                }
                
                catch (ConnectionException ce) {}                
            }
            it = hsmInhibitObjects.iterator();
            while (it.hasNext())
            {
                JDObject jdo = it.next();
                mouseifyHSMInhibitedAttribute(jdo);
            }        
            
        }
        
    }
    
    
    private String getConnectedSpareLabel(String spareDevName, String connectedMagName)
    {
        String  spLabel = connectedMagName;
        
        if (spareDevName.toLowerCase().contains("dqspare"))
        {
                spLabel = "dq=";
                spLabel = spLabel+connectedMagName;
                return spLabel;
        }
        
        if (spareDevName.toLowerCase().contains("qspare"))
        {
            spLabel = "q";
//            spLabel = "";
            String spareNumber = spareDevName.substring(spareDevName.length()-1); // Last character of the spare devName
            spLabel = spLabel + spareNumber;
            spLabel = spLabel + "=";
            spLabel = spLabel+connectedMagName;
            return spLabel;
        }
        
        if (spareDevName.toLowerCase().contains("sospare"))
        {
            spLabel = "so=";
            spLabel = spLabel+connectedMagName;
            return spLabel;
        }       
        return spLabel;
    }
    
    private void changeSpareChannelLabel(JDLabel jdl, String spareDevname, String connectedName)
    {
        final String      spareLabel;
        if (!connectedName.equals("NONE"))
        {
            spareLabel = getConnectedSpareLabel(spareDevname, connectedName.toLowerCase());
        }
        else
        {
            String  spareName = "";
            if (spareDevname.toLowerCase().contains("dqspare"))
                spareName = "DQspare";
            else
            {
                if (spareDevname.toLowerCase().contains("qspare"))
                {
                    int index = spareDevname.lastIndexOf("-");
                    if (index != -1)
                        spareName = "Qspare"+spareDevname.charAt(index+1);
                }
                else
                {
                    if (spareDevname.toLowerCase().contains("sospare"))
                        spareName = "SOspare";
                }
            }
            spareLabel = spareName;
        }
        jdl.setText(spareLabel);
    }
    
    
    private void changeChannel(StringScalar connectedAtt, String connectedName)
    {
        String    devName = connectedAtt.getDeviceName().toLowerCase();
        boolean   isSpareChannel = devName.contains("spare");

        if (JEbsSrPsConstants.MINI_BETA)
        {
            if (devName.contains("c30-") || devName.contains("c31-"))
            {
                if (isSpareChannel)
                {
                    if ( devName.contains("ps-qspare") && devName.contains("-2") )
                        return;
                    if ( devName.contains("ps-sospare") && devName.contains("-1") )
                        return;
                }
            }
        }
        Vector<JDObject>  objVect = new Vector();
        if(synopticType==TYPE_HSC) {
          objVect = getObjectsByName(devName+"/State", false);
        } else if (synopticType==TYPE_INHIB) {
          objVect = getObjectsByName(devName+"/HSMInhibited", false);
        }
        
        if (objVect.isEmpty()) return;

        for (int i = 0; i < objVect.size(); i++)
        {
            JDObject jdo = objVect.get(i);
            Vector<JDObject> foundObjs = new Vector<JDObject>();
            jdo.getObjectsByName(foundObjs, "IgnoreRepaint", true);
            if (foundObjs.isEmpty()) continue;
            JDLabel jdl = (JDLabel) foundObjs.get(0);
            if (isSpareChannel) {
                changeSpareChannelLabel(jdl, devName, connectedName);
            } else {
                jdl.setVisible(!connectedName.equals("NONE"));
            }
        }
        repaint();
    }
    
    @Override
    public void stringScalarChange(StringScalarEvent sse) {
        
        StringScalar ss = (StringScalar) sse.getSource();
        String previous = connectedStateMap.get(ss);
        if (previous != null) {
            if (previous.equals(sse.getValue())) // same value don't change anything
            {
                return;
            }
        }
        //either previous connected state was null or it was # with the new one just received
        changeChannel(ss, sse.getValue());
        connectedStateMap.put(ss, sse.getValue());
    }

    
    @Override
    public void stateChange(AttributeStateEvent qfe)
    {
        if ( channelConnectionAttl.contains(qfe.getSource()) )
        {
//            System.out.println(qfe.getState());
        }
        else
        {
            super.stateChange(qfe);
        }        
    }
    
    @Override
    public void errorChange(ErrorEvent ee)
    {
        if ( channelConnectionAttl.contains(ee.getSource()) )
        {
            
        }
        else
        {
            super.errorChange(ee);
        }                
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
//                String  cellNb = "10";
                String  cellNb = "37";
                JFrame  jf = new JFrame();
                jf.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                
                SrpsSynopticFileViewer  synoptViewer = new SrpsSynopticFileViewer(SrpsSynopticFileViewer.TYPE_INHIB);
                
                // Load the synoptic file                                
                try
                {
                    synoptViewer.setErrorHistoryWindow(jebssrps.JEbsSrPsConstants.ERR_H);
                    synoptViewer.setAutoZoom(true);
                }
                catch (Exception setErrwExcept)
                {
                    System.out.println("Cannot set Error History Window for Synoptic");
                }

                InputStream jdFileInStream;
                jdFileInStream = jebssrps.JEbsSrPsConstants.class.getClassLoader().getResourceAsStream(jebssrps.JEbsSrPsConstants.SWAP_INHIB_SYNOPTIC_FILE_NAME);

                if (jdFileInStream == null)
                {
                    javax.swing.JOptionPane.showMessageDialog(
                            null, "Failed to get the inputStream for the synoptic file resource \n",
                            "Resource error",
                            javax.swing.JOptionPane.ERROR_MESSAGE);
                    System.exit(-1);
                }

                java.io.InputStreamReader inStrReader = new InputStreamReader(jdFileInStream);
                try
                {
                    synoptViewer.loadTemplateSynopticFromStream(inStrReader, cellNb);
                    synoptViewer.setToolTipMode(TangoSynopticHandler.TOOL_TIP_NAME);
                }
                catch (IOException ioex)
                {
                    javax.swing.JOptionPane.showMessageDialog(
                            null, "Cannot find load the synoptic input stream reader.\n"
                            + " Application will abort ...\n"
                            + ioex,
                            "Resource access failed",
                            javax.swing.JOptionPane.ERROR_MESSAGE);
                    System.exit(-1);
                }
                catch (MissingResourceException mrEx)
                {
                    javax.swing.JOptionPane.showMessageDialog(
                            null, "Cannot parse the synoptic file.\n"
                            + " Application will abort ...\n"
                            + mrEx,
                            "Cannot parse the file",
                            javax.swing.JOptionPane.ERROR_MESSAGE);
                    System.exit(-1);
                }  
                
//                synoptViewer.connectToChannelConnectionAttributes();
                
                jf.setContentPane(synoptViewer);
                jf.pack();
                jf.setVisible(true);
            }
        });
    }

    
}
