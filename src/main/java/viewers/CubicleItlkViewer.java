/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewers;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.DevStateSpectrumEvent;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.IDevStateSpectrum;
import fr.esrf.tangoatk.core.IDevStateSpectrumListener;
import fr.esrf.tangoatk.core.IDevice;
import fr.esrf.tangoatk.core.IStringSpectrum;
import fr.esrf.tangoatk.core.IStringSpectrumListener;
import fr.esrf.tangoatk.core.StringSpectrumEvent;
import fr.esrf.tangoatk.widget.util.ATKConstant;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author poncet
 */
public class CubicleItlkViewer extends JPanel
                               implements IStringSpectrumListener, IDevStateSpectrumListener
{
    private IStringSpectrum         namesModel = null;
    private IDevStateSpectrum       itlkModel = null;
    
    private JLabel[]                nameViewers = null;
    private JLabel[]                stateViewers = null;
    
    public CubicleItlkViewer()
    {
        setLayout(new GridBagLayout());
    }
    
    public void clearModels()
    {
        clearNamesModel();
        clearItlkModel();
        clearViewers();
    }

    public void setModels(IStringSpectrum namesAtt, IDevStateSpectrum statesAtt)
    {
        clearModels();
        
        namesAtt.addListener(this);
        statesAtt.addDevStateSpectrumListener(this);
        namesAtt.refresh();
        statesAtt.refresh();
    }
    
    private void clearNamesModel()
    {
        if (namesModel == null) return;
        namesModel.removeListener(this);
        namesModel = null;
    }
    
    private void clearItlkModel()
    {
        if (itlkModel == null) return;
        itlkModel.removeDevStateSpectrumListener(this);
        itlkModel = null;
    }
    
    private void clearViewers()
    {
        if (nameViewers!= null)
        {
            for (int i=0; i<nameViewers.length; i++)
            {
                nameViewers[i] = null;
            }
            nameViewers = null;
        }
        
        if (stateViewers!= null)
        {
            for (int i=0; i<stateViewers.length; i++)
            {
                stateViewers[i] = null;
            }
            stateViewers = null;
        }
        this.removeAll();
   }
    
    private void initViewers(String[] itlkNames)
    {
        GridBagConstraints     gbc = new GridBagConstraints();
        
        nameViewers = new JLabel[itlkNames.length];
        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.weightx = 1.0;
        gbc.ipadx = 6;
        gbc.ipady = 10;
        gbc.gridx = 0;
        
        for (int i=0; i<itlkNames.length; i++)
        {
            JLabel jl = getItlkNameViewer();
            jl.setText(itlkNames[i]);
            gbc.gridy = i;
            this.add(jl, gbc);
            nameViewers[i] = jl;
        }
        
    }
    
    private JLabel getItlkNameViewer()
    {
        JLabel jlb = null;
        jlb = new JLabel();
        
        jlb.setOpaque(true);
        jlb.setBackground(ATKConstant.getColor4State(IDevice.UNKNOWN));
        jlb.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 14));
        jlb.setHorizontalAlignment(JLabel.CENTER);
        jlb.setVerticalAlignment(JLabel.CENTER);
        jlb.setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.Color.BLACK));

        
        return jlb;
    }
    
    
    
    

    @Override
    public void stringSpectrumChange(StringSpectrumEvent e)
    {
        String[] newNames = e.getValue();
        if (nameViewers != null)
            if (newNames.length == nameViewers.length) return;
        clearViewers();
        initViewers(e.getValue());
        if (itlkModel != null) itlkModel.refresh();
    }

    @Override
    public void devStateSpectrumChange(DevStateSpectrumEvent e)
    {
        String[]  states = e.getValue();
        if (nameViewers == null) return;
        if (nameViewers.length != states.length) return;
        for (int i=0; i<states.length; i++)
        {
            nameViewers[i].setBackground(ATKConstant.getColor4State(states[i]));
        }
    }

    @Override
    public void stateChange(AttributeStateEvent e)
    {
    }

    @Override
    public void errorChange(ErrorEvent evt)
    {
        if (nameViewers == null) return;
        if (evt.getSource() == itlkModel)
        {
            for (int i=0; i<nameViewers.length; i++)
            {
                nameViewers[i].setBackground(ATKConstant.getColor4State(IDevice.UNKNOWN));
            }
        }
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                JFrame  jf = new JFrame();
                jf.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                AttributeList attl = new AttributeList();
                
                CubicleItlkViewer itlkv = new CubicleItlkViewer();
                
                try
                {
                    IDevStateSpectrum itlkStatesAtt = (IDevStateSpectrum) attl.add("srmag/hsc-itlk/c37/InterlockStates");
                    IStringSpectrum itlkNamesAtt = (IStringSpectrum) attl.add("srmag/hsc-itlk/c37/InterlockNames");
                    itlkv.setModels(itlkNamesAtt, itlkStatesAtt);
                }
                catch (Exception ex) {}
                
                attl.startRefresher();
                jf.setContentPane(itlkv);
                jf.setPreferredSize(new java.awt.Dimension(400, 200));
                jf.pack();
                jf.setVisible(true);
            }
        });
    }
    
}
