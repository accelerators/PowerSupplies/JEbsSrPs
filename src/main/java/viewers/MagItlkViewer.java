/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ItlkViewer.java
 *
 * Created on Jun 24, 2011, 2:29:20 PM
 */
package viewers;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.DevStateSpectrumEvent;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.IDevStateSpectrum;
import fr.esrf.tangoatk.core.IDevStateSpectrumListener;
import fr.esrf.tangoatk.core.IDevice;
import fr.esrf.tangoatk.widget.util.ATKConstant;
import java.awt.GridBagConstraints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author poncet
 */
public class MagItlkViewer extends javax.swing.JPanel
                        implements IDevStateSpectrumListener
{
    private IDevStateSpectrum         itlkStatesModel = null;
    private String[]                  itlkStateLabels = null;

    private JLabel[]                  itlkViewers = null;

    private int                       nbPerRow = 10;
    
    private MagItlkViewerListener     clickListener = null;

    /** Creates new form ItlkViewer */
    public MagItlkViewer()
    {
        initComponents();
    }

    public int getNbPerRow()
    {
        return nbPerRow;
    }

    public void setNbPerRow(int nb)
    {
        nbPerRow = nb;
    }


    public IDevStateSpectrum getItlkStatesModel()
    {
        return itlkStatesModel;
    }

    public void clearItlkStatesModel()
    {
        if (itlkStatesModel == null) return;
        itlkStatesModel.removeDevStateSpectrumListener(this);

        itlkStatesModel = null;
    }

    public void setItlkStatesModel(IDevStateSpectrum  states)
    {
        clearItlkStatesModel();
        if (states == null) return;

        itlkStatesModel = states;
        if (!itlkStatesModel.areAttPropertiesLoaded())
            itlkStatesModel.loadAttProperties();
        itlkStateLabels = itlkStatesModel.getStateLabels();
        
        createStateLabels();

        itlkStatesModel.addDevStateSpectrumListener(this);
        itlkStatesModel.refresh();
    }
    
    public void setClickListener(MagItlkViewerListener lis)
    {
        clickListener = lis;
    }


    private void createStateLabels()
    {
        JLabel                  stateJLabel = null;
        GridBagConstraints      gbc = new GridBagConstraints();

        gbc.insets = new java.awt.Insets(5,5,5,5);
        
        if (itlkStateLabels == null) return;

        itlkViewers = new JLabel[itlkStateLabels.length];

        for (int i=0; i<itlkStateLabels.length; i++)
        {
            stateJLabel = new JLabel();
            stateJLabel.setPreferredSize(new java.awt.Dimension(44, 18));
            stateJLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
            stateJLabel.setBackground(new java.awt.Color(255, 255, 255));
            stateJLabel.setFont(new java.awt.Font("Dialog", 1, 12));
            stateJLabel.setText(itlkStateLabels[i]);
            stateJLabel.setOpaque(true);
            
            final String  itlkLabel = itlkStateLabels[i];
            final int     itlkIndex = i;

            stateJLabel.addMouseListener(new MouseAdapter()
                                        {
                                            @Override
                                            public void mouseClicked(MouseEvent e)
                                            {
                                                itlkClicked(itlkLabel, itlkIndex);
                                            }

                                        }
                                    );
            itlkViewers[i] = stateJLabel;

            gbc.gridx = i % nbPerRow;
            gbc.gridy = i / nbPerRow;
            this.add(stateJLabel, gbc);
        }
    }
    
    private void itlkClicked(String magName, int magIndex)
    {
//        System.out.println("Magnet Name = " + magName + "; Magnet Index = " + magIndex);
        if (clickListener != null)
            clickListener.magItlkClicked(magName, magIndex);
    }


    private void updateStateColors(String[] states)
    {
        if ((itlkViewers == null) || (itlkViewers.length == 0)) return;
        if ((states == null) || (states.length == 0)) return;

        for (int i=0; i<states.length; i++)
        {
            if (i >= itlkViewers.length) break;

            itlkViewers[i].setBackground(ATKConstant.getColor4State(states[i]));
            itlkViewers[i].setToolTipText(itlkStatesModel.getName()+"["+i+"]");
        }
    }



    public void devStateSpectrumChange(DevStateSpectrumEvent dsse)
    {
        updateStateColors(dsse.getValue());
    }
    
    
    public void stateChange(AttributeStateEvent ase)
    {
    }

    public void errorChange(ErrorEvent ee)
    {
        if (ee.getSource() == itlkStatesModel)
        {
            if (itlkViewers == null) return;
            for (int i=0; i<itlkViewers.length; i++)
            {
                itlkViewers[i].setBackground(ATKConstant.getColor4State(IDevice.UNKNOWN));
            }            
        }
    }


    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        setLayout(new java.awt.GridBagLayout());
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables


    
    
    
    
    /**
    * @param args the command line arguments
    */
    public static void main(String args[])
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {

            public void run()
            {
                JFrame               jf = new JFrame();
                MagItlkViewer        itlkv = new MagItlkViewer();
                AttributeList        attl = new AttributeList();
                IDevStateSpectrum    stateSpectAtt = null;
                
                itlkv.setNbPerRow(7);

                jf.setContentPane(itlkv);

                try
                {
                    stateSpectAtt = (IDevStateSpectrum) attl.add("srmag/fl-temp-itlk/c37/MagnetStates");
                    itlkv.setItlkStatesModel(stateSpectAtt);
                }
                catch (Exception ex)
                {
                    System.exit(-1);
                }

                attl.startRefresher();
                jf.pack();
                jf.setPreferredSize(new java.awt.Dimension(300, 150));
                jf.setVisible(true);
            }
        });
    }


}
