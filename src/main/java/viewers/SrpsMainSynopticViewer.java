/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewers;

import fr.esrf.tangoatk.core.attribute.AAttribute;
import fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer;
import fr.esrf.tangoatk.widget.util.jdraw.JDMouseAdapter;
import fr.esrf.tangoatk.widget.util.jdraw.JDMouseEvent;
import fr.esrf.tangoatk.widget.util.jdraw.JDObject;
import hsm.HsmPanel;
import java.awt.GridBagConstraints;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Vector;
import javax.swing.JPanel;
import jebssrps.HscPanel;

/**
 *
 * @author poncet
 */
public class SrpsMainSynopticViewer extends SynopticFileViewer
{
    JPanel                           rightSidePanelPlaceHolder = null;
    HashMap<String, JPanel>          panelMap = new HashMap<String, JPanel>();
    
    private boolean                  srpsSynoptic = true;
    private boolean                  hsmSynoptic = false;

    public SrpsMainSynopticViewer(JPanel  rightPanelPlaceHolder)
    {
        super();
        rightSidePanelPlaceHolder = rightPanelPlaceHolder;
    }

    public SrpsMainSynopticViewer(JPanel  rightPanelPlaceHolder, boolean hsm)
    {
        super();
        rightSidePanelPlaceHolder = rightPanelPlaceHolder;
        hsmSynoptic = hsm;
        srpsSynoptic = !hsmSynoptic;
    }

    /**
     * Load a jdraw grpahics input stream into the drawing area. This method
     * allows to load a synoptic which is not necessarily a file on disk. This
     * method is particularly used when the synoptic jdraw file is pakaged into
     * the application jarfile and cannot be accessed as a separate file on the
     * disk.
     *
     * @param synopInputStream An InputStreamReader should be created by the
     * application and passed to this method
     * @throws IOException Exception when the inputStream cannot be accessed.
     * @throws MissingResourceException when the "jdraw" inputStream cannot be
     * parsed.
     */
    public void loadMainSynopticFromStream(InputStreamReader synopInputStream) throws IOException, MissingResourceException
    {
        jdHash = new HashMap<String, List<JDObject>>();
        stateCashHash = new HashMap<String, List<String>>();

        super.loadFromStream(synopInputStream);
        jdrawFileFullName = "synopInputStreamReader";
        if (getObjectNumber() == 0)
        {
            throw new MissingResourceException(
                    "The Jdraw file has no component inside. First draw a Jdraw File.",
                    "JDrawEditor", null);
        }
        parseJdrawComponents();
        replaceMouseListeners();
        computePreferredSize();

        // We need to refresh all attributes because if several JDObjects
        // have the same model , only the first one get the event. (API
        // specification )
        if (allAttributes.size() > 0)
        {
            for (int i = 0; i < allAttributes.size(); i++)
            {
                ((AAttribute) allAttributes.elementAt(i)).refresh();
            }
            allAttributes.startRefresher();
        }

    }
    
    
    private void replaceMouseListeners()
    {
        Vector objVect = null;

        for (int i = 1; i <= 32; i++)  // Loop for 32 cells
        {
            String cellNb = String.format("%02d", i);
            if (srpsSynoptic)
                objVect = getObjectsByName("srmag/ps-main/c"+cellNb+"/State", false);
            else // hsmSynoptic
                objVect = getObjectsByName("srmag/hsm/c"+cellNb+"/State", false);

            if (objVect.isEmpty())
            {
                continue;
            }
            for (int j = 0; j < objVect.size(); j++)
            {
                Object obj = objVect.get(j);
                if (!(obj instanceof JDObject))
                {
                    continue;
                }
                JDObject jdo = (JDObject) obj;
                jdo.clearMouseListener();
                mouseifyCellStateAttribute(jdo);
                
//                System.out.println("replaceMouseListeners : end ");
            }
            objVect.clear();
            objVect = null;
        }
    }

    private void mouseifyCellStateAttribute(JDObject jdObj)
    {
        /* Attach a JDMouse listener to the cell state attribute component. */
        jdObj.addMouseListener( new JDMouseAdapter()
                                    {
                                        @Override
                                        public void mousePressed(JDMouseEvent e)
                                        {
                                            cellStateAttributeClicked(e);
                                        }

                                        @Override
                                        public void mouseEntered(JDMouseEvent e)
                                        {
                                            cellStateDisplayToolTip(e);
                                        }

                                        @Override
                                        public void mouseExited(JDMouseEvent e)
                                        {
                                            cellStateRemoveToolTip();
                                        }
                                    });
    }

    private void cellStateAttributeClicked(JDMouseEvent evt)
    {
        if (evt.getButton() != MouseEvent.BUTTON1)
        {
            return;
        }
        JDObject comp = (JDObject) evt.getSource();
        String attName = comp.getName();
        
//        System.out.println("cellStateAttributeClicked : " + attName);
        int i = attName.lastIndexOf('/');
        if (i > 0)
        {
            String devName = attName.substring(0, i);
            showRightSidePanel(devName);
        }

    }

    private void cellStateDisplayToolTip(JDMouseEvent e)
    {
        String devName;
        JDObject jdObj;
        
        jdObj = (JDObject) e.getSource();
        devName = jdObj.getName();     // The name of the device
        setToolTipText(devName);
        return;
    }

    private void cellStateRemoveToolTip()
    {
        setToolTipText(null);
    }
    
    private void showRightSidePanel(String deviceName)
    {
        if (rightSidePanelPlaceHolder == null) return;
        String devName = deviceName.toLowerCase();
        
        JPanel hp = panelMap.get(devName);
        if (hp == null)
        {
            if (srpsSynoptic)
                hp = new HscPanel(devName);
            else // hsmSynoptic
                hp = new HsmPanel(devName);
            
            panelMap.put(devName, hp);
        }
        
        if (rightSidePanelPlaceHolder.getComponentCount() > 0)
            if (rightSidePanelPlaceHolder.getComponent(0) == hp)
                return;
        
        rightSidePanelPlaceHolder.removeAll();
        
        GridBagConstraints  gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.insets = new java.awt.Insets(5, 5, 5, 5);
        rightSidePanelPlaceHolder.add(hp, gbc);
        
        rightSidePanelPlaceHolder.getParent().validate();
        rightSidePanelPlaceHolder.getParent().repaint();
    }
    
}
