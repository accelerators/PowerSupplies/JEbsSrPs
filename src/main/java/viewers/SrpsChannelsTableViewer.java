/*
 * SrpsChannelsTableViewer.java
 *
 * Created on 12 july 2018, 14:26
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package viewers;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.IAttribute;
import fr.esrf.tangoatk.core.ICommand;
import fr.esrf.tangoatk.widget.attribute.MultiAttAndCmdTableViewer;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import jebssrps.JEbsSrPsConstants;

/**
 *
 * @author PONCET
 */
public class SrpsChannelsTableViewer extends MultiAttAndCmdTableViewer
{
    private String                      cellNbStr = null;
    private String[]                    psChannelDevNames = null;
    
    private static final int            CURRENT_COL_INDEX = 0;
    private static final int            VOLTAGE_COL_INDEX = 1;
    private static final int            ON_COL_INDEX = 2;
    private static final int            OFF_COL_INDEX = 3;
    
    private static final int            PANEL_COL_INDEX = 4;
    
    private static final String[]	ATTS_COL_NAMES = {
                                                    "Current",
                                                    "Voltage",
                                                    " ",
                                                    " ",
                                                    " "};
    
    private static final String[]       PS_ROW_NAMES;
    
    private static final String[]	EBS_PS_ROW_NAMES = {
                                                    "QF6B",
                                                    "QF1A",
                                                    "QD2A",                                                    
                                                    "QF8B",
                                                    "QD3A",
                                                    "QF4A",                                                    
                                                    "QF8D",
                                                    "QF4B",
                                                    "QD5B",                                                    
                                                    "QF6D",
                                                    "QD5D",
                                                    "QF4D",                                                    
                                                    "Qspare1",
                                                    "QF4E",
                                                    "QD3E",                                                   
                                                    "Qspare2",
                                                    "QD2E",
                                                    "QF1E",                                                    
                                                    "Qspare3",
                                                    "Qspare4",
                                                    "OF1B",                                                    
                                                    "SD1A",
                                                    "SF2A",
                                                    "SD1B",                                                    
                                                    "SD1D",
                                                    "SF2E",
                                                    "SD1E",                                                    
                                                    "DQ1B",
                                                    "DQ2C",
                                                    "SOspare1",                                                    
                                                    "DQ1D",
                                                    "DQspare1",
                                                    "OF1D",
                                                    };
    
//    private static final String[]	CHARTREUSE_PS_ROW_NAMES = {
//                                                    "QF6B",
//                                                    "QF8B",
//                                                    "QF6D",                                                    
//                                                    "QF4A",
//                                                    "OF1B",
//                                                    "QD3A",                                                    
//                                                    "QD2A",
//                                                    "SF2A",
//                                                    "SD1A",                                                    
//                                                    "OF1D",
//                                                    "DQ1B",
//                                                    "QF6B",
//                                                    "QF8B",
//                                                    "QF6D",                                                    
//                                                    "QF4A",
//                                                    "OF1B",
//                                                    "QD3A",                                                    
//                                                    "QD2A",
//                                                    "SF2A",
//                                                    "SD1A",                                                    
//                                                    "OF1D",
//                                                    "DQ1B",
//                                                    "QF6B",
//                                                    "QF8B",
//                                                    "QF6D",                                                    
//                                                    "QF4A",
//                                                    "OF1B",
//                                                    "QD3A",                                                    
//                                                    "QD2A",
//                                                    "SF2A",
//                                                    "SD1A",                                                    
//                                                    "OF1D",
//                                                    "DQ1B"
//                                                    };
    
    private String[]                    PS_DEV_NAMES = null;
    private HashMap<String, JFrame>     atkpanelMap = new HashMap<String, JFrame>();

    private AttributeList    attl = null;
    private CommandList      cmdl = null;
    
//    PanelButtonColumn    pbc = null;
    PanelButton              pbRendererEditor;
    
    
    static
    {
//        if (JEbsSrPsConstants.CHARTREUSE_CONFIGURATION)
//            PS_ROW_NAMES = CHARTREUSE_PS_ROW_NAMES;
//        else
            PS_ROW_NAMES = EBS_PS_ROW_NAMES;        
    }
            
    
    /** Creates a new instance of SrpsChannelsTableViewer */
    public SrpsChannelsTableViewer(ErrorHistory errH, String cellStr)      
    {
        cellNbStr = cellStr;
        int  cellNb = -1;
        
        cellNb = Integer.parseInt(cellNbStr);
        
        if (cellNb == 37)
            psChannelDevNames = JEbsSrPsConstants.EBS_PS_CHANNEL_DEV_NAMES;
        else
        {
            if (cellNb == 3)
                psChannelDevNames = JEbsSrPsConstants.C3_PS_CHANNEL_DEV_NAMES;
            else 
                if (cellNb == 4)
                    psChannelDevNames = JEbsSrPsConstants.C4_PS_CHANNEL_DEV_NAMES;
                else
                    psChannelDevNames = JEbsSrPsConstants.EBS_PS_CHANNEL_DEV_NAMES;
        }
        
        
	//setAlarmEnabled(false);
	//setUnitVisible(false);
        setFont(new Font("Dialog", Font.PLAIN, 14));

        if (PS_ROW_NAMES == null)
           setNbRows(1);
        else
        {
           setNbRows(PS_ROW_NAMES.length);
           setRowIdents(PS_ROW_NAMES);
           getRowIdCellRenderer().setBackground(new java.awt.Color(228,228,228));
           PS_DEV_NAMES = new String[PS_ROW_NAMES.length];
        }
	setNbColumns(ATTS_COL_NAMES.length);
	setColumnIdents(ATTS_COL_NAMES);
        JComponent jcomp = (JComponent) this.getColumn(ATTS_COL_NAMES[0]).getHeaderRenderer();
        jcomp.setBackground(new java.awt.Color(228,228,228));
        
        attl = new AttributeList();
        attl.addErrorListener(errH);
        attl.addSetErrorListener(ErrorPopup.getInstance());
        
        cmdl = new CommandList();
        cmdl.addErrorListener(errH);
        cmdl.addErrorListener(ErrorPopup.getInstance());
        
        fillPsTable();
        
        pbRendererEditor = new PanelButton(this.getModel(), this);

        
        addPanelColumn();
        setRowHeight(getRowHeight() + 6);
//        setIntercellSpacing(new Dimension (10,20));
        
        for (int i=1; i<getColumnCount(); i++)
        {
            TableColumn  tc = getColumnModel().getColumn(i);
            int colIndex = i - 1;
            if ( (colIndex == CURRENT_COL_INDEX) || (colIndex == VOLTAGE_COL_INDEX) )
            {
                tc.setPreferredWidth(80);
            }
            else
            {
                tc.setPreferredWidth(30);
            }
        }
        attl.startRefresher();
    }
    
    //override the getCellRenderer method of JTable
    @Override
    public TableCellRenderer getCellRenderer(int row, int column)
    {
       if (column == PANEL_COL_INDEX+1)
           return pbRendererEditor;
       return super.getCellRenderer(row, column);
    }
    
    @Override
    public boolean isCellEditable(int row, int column)
    {
       if (column == PANEL_COL_INDEX+1)
           return true;
       return super.isCellEditable(row, column);        
    }
    
    private void fillPsTable()
    {
        String      genericDevName = null;
        String      entityFullName = null;
        
        if (psChannelDevNames.length != PS_ROW_NAMES.length) 
            return;
        
        for (int row=0; row<PS_ROW_NAMES.length; row++)
        {
            genericDevName = psChannelDevNames[row];
            String deviceName = genericDevName.replace("c##", "c"+cellNbStr);
            try
            {
                PS_DEV_NAMES[row] = deviceName.toLowerCase();
                entityFullName = deviceName + "/Current";
                IAttribute  iatt = (IAttribute) attl.add(entityFullName);
                this.setModelAt(iatt, row, CURRENT_COL_INDEX);

                entityFullName = deviceName + "/Voltage";
                iatt = (IAttribute) attl.add(entityFullName);
                this.setModelAt(iatt, row, VOLTAGE_COL_INDEX);

                entityFullName = deviceName + "/On";
                ICommand  icmd = (ICommand) cmdl.add(entityFullName);
                this.setModelAt(icmd, row, ON_COL_INDEX);

                entityFullName = deviceName + "/Off";
                icmd = (ICommand) cmdl.add(entityFullName);
                this.setModelAt(icmd, row, OFF_COL_INDEX);
                
            }
            catch (Exception ex)
            {
                  javax.swing.JOptionPane.showMessageDialog(
                      null, "Failed to add the " + entityFullName + " into the table.\n"
                          + "PS channels attributes & commands table is not complete ...\n"
                          + "Exception received : " + ex,
                          "Failed to connect to attribute or command ",
                          javax.swing.JOptionPane.ERROR_MESSAGE);
            }                
        }
    }
    
    private void addPanelColumn()
    {
        TableModel tm = this.getModel();
        
        for (int row=0; row<PS_ROW_NAMES.length; row++)
        {
            tm.setValueAt("...", row, PANEL_COL_INDEX+1);
        }
        this.getColumnModel().getColumn(PANEL_COL_INDEX+1).setCellEditor(pbRendererEditor);
    }
    
    
    @Override
    public void clearModel()
    {
        attl.stopRefresher();
        super.clearModel();
        attl.clear();
        attl = null;
    }
    
    
           
       
    
       // Inner classes
    
       class PanelButton extends AbstractCellEditor implements TableCellRenderer, TableCellEditor
       {
            TableModel  tm;
            JTable      tab;
            JButton     rendererBtn = new JButton();
            JButton     editorBtn = new JButton();
            Object      editorValue;
            boolean     isPanelButtonEditor;
            
	    /** Creates a new instance of DetailsButtonCellRenderer */
	    PanelButton(TableModel tabModel, JTable table)
	    {
                tm = tabModel;
                tab = table;
                rendererBtn.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 14));
                rendererBtn.setText("...");
//                rendererBtn.setBorder(new CompoundBorder(new EmptyBorder(new Insets(1,14,1,14)), rendererBtn.getBorder()));
                editorBtn.setFont(new java.awt.Font("Dialog", java.awt.Font.BOLD, 14));
                editorBtn.setText("...");
                
                editorBtn.addActionListener(new ActionListener()
                        {
                            @Override
                            public void actionPerformed(ActionEvent e)
                            {
                                int row = tab.convertRowIndexToModel(tab.getEditingRow());
                                showExpertPanel(row);
                                fireEditingStopped();
                            }
                            
                        });
      	    }
            
            private void showExpertPanel(int row)
            {
//                System.out.println("AtkPanel : row = " + row);
                String devName = getDeviceName(row);
                if (devName == null) return;
                
                JFrame panel = atkpanelMap.get(devName);
                if (panel == null)
                {
                    panel = new atkpanel.MainPanel(devName, false, false);
                    atkpanelMap.put(devName, panel);
                    final String device = devName;
                    panel.addWindowListener(new java.awt.event.WindowAdapter()
                                {
                                    public void windowClosing(java.awt.event.WindowEvent evt)
                                    {
                                        Object obj = evt.getSource();
                                        if (obj instanceof JFrame)
                                        {
                                            JFrame jf = (JFrame) obj;
                                            atkpanelClosed(device, jf);
                                        }
                                    }
                                });
                }
                else
                {
                   panel.setVisible(true); 
                }

            }
            
            private void atkpanelClosed(String deviceName, JFrame atkp)
            {
                JFrame panel = atkpanelMap.get(deviceName);
                if (panel == null) return;
                if (panel != atkp) return;
                atkpanelMap.remove(deviceName);
            }
            
            private String getDeviceName(int row)
            {
                if (PS_DEV_NAMES == null) return null;
                if (row >= PS_DEV_NAMES.length) return null;
                return(PS_DEV_NAMES[row]);
            }

            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
            {
                if (isSelected)
                {
                    rendererBtn.setForeground(table.getSelectionForeground());
                    rendererBtn.setBackground(table.getSelectionBackground());
                }
                else
                {
                    rendererBtn.setForeground(table.getForeground());
                    rendererBtn.setBackground(UIManager.getColor("Button.background"));
                }
                String  devName = getDeviceName(row);
                if (devName != null)
                    rendererBtn.setToolTipText("AtkPanel : "+devName);
               return rendererBtn;
            }

            @Override
            public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column)
            {
                this.editorValue = value;
                return editorBtn;
            }

            @Override
            public Object getCellEditorValue()
            {
                return editorValue;
            }


       }
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                JFrame  jf = new JFrame();
                jf.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
                
                SrpsChannelsTableViewer     psch = new SrpsChannelsTableViewer(JEbsSrPsConstants.ERR_H, "37");
                JScrollPane                    pscScrollPane = new JScrollPane(psch);
                
                jf.setContentPane(pscScrollPane);
                jf.pack();
                jf.setVisible(true);
            }
        });
    }



}
