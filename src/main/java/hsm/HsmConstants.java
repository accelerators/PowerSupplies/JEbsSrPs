/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hsm;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;

/**
 *
 * @author poncet
 */
public abstract class HsmConstants
{
    static public final String                  HSM_SYNOPTIC_FILE_NAME    = "HsmMain.jdw";
    public static String                        HSM_SUMMARY_DEVNAME = "srmag/hsm/summary";
    public static String                        HSM_DEV_NAME_PATTERN = "srmag/hsm/c";
    
    static public final String                  VPS_PREFIX;
    
    static public final String[]                HSM_MAG_NAMES = {
                                                    "qf6b",
                                                    "qf1a",
                                                    "qd2a",                                                    
                                                    "qf8b",
                                                    "qd3a",
                                                    "qf4a",                                                    
                                                    "qf8d",
                                                    "qf4b",
                                                    "qd5b",                                                    
                                                    "qf6d",
                                                    "qd5d",
                                                    "qf4d",                                                    
                                                    "qf4e",
                                                    "qd3e",                                                   
                                                    "qd2e",
                                                    "qf1e",                                                    
                                                    "of1b",                                                    
                                                    "sd1a",
                                                    "sf2a",
                                                    "sd1b",                                                    
                                                    "sd1d",
                                                    "sf2e",
                                                    "sd1e",                                                    
                                                    "dq1b",
                                                    "dq2c",
                                                    "dq1d",
                                                    "of1d"
                                                    };
    
    static public final String[]                HSM_MAG_PS_DEV_NAME_PATTERNS = {
                                                    "srmag/?-qf6/c##-b",
                                                    "srmag/?-qf1/c##-a",
                                                    "srmag/?-qd2/c##-a",                                                    
                                                    "srmag/?-qf8/c##-b",
                                                    "srmag/?-qd3/c##-a",
                                                    "srmag/?-qf4/c##-a",                                                    
                                                    "srmag/?-qf8/c##-d",
                                                    "srmag/?-qf4/c##-b",
                                                    "srmag/?-qd5/c##-b",                                                    
                                                    "srmag/?-qf6/c##-d",
                                                    "srmag/?-qd5/c##-d",
                                                    "srmag/?-qf4/c##-d",                                                    
                                                    "srmag/?-qf4/c##-e",
                                                    "srmag/?-qd3/c##-e",                                                   
                                                    "srmag/?-qd2/c##-e",
                                                    "srmag/?-qf1/c##-e",                                                    
                                                    "srmag/?-of1/c##-b",                                                    
                                                    "srmag/?-sd1/c##-a",
                                                    "srmag/?-sf2/c##-a",
                                                    "srmag/?-sd1/c##-b",                                                    
                                                    "srmag/?-sd1/c##-d",
                                                    "srmag/?-sf2/c##-e",
                                                    "srmag/?-sd1/c##-e",                                                    
                                                    "srmag/?-dq1/c##-b",
                                                    "srmag/?-dq2/c##-c",
                                                    "srmag/?-dq1/c##-d",
                                                    "srmag/?-of1/c##-d"
                                                    };

    
    static public final String[]                HSM_SPARE_NAMES = {
                                                    "qspare1",
                                                    "qspare2",
                                                    "qspare3",                                                    
                                                    "qspare4",
                                                    "sospare1",                                                    
                                                    "dqspare1" };
    
    static public final String[]                HSM_SPARE_PS_DEV_NAME_PATTERNS = {
                                                    "srmag/ps-qspare/c##-1",
                                                    "srmag/ps-qspare/c##-2",
                                                    "srmag/ps-qspare/c##-3",
                                                    "srmag/ps-qspare/c##-4",
                                                    "srmag/ps-sospare/c##-1",                                                    
                                                    "srmag/ps-dqspare/c##-1"
                                                    };


    
    
    static  // get the property VpsPrefix for te class HSMPowerSupply if "m"-> old magnet archi if "vps" new magnet archi
    {
        // get the free properties for JEbsSrPs to set the timeout for PS Main device proxy
        Database   dbConnection = null;
        try
        {
            dbConnection = ApiUtil.get_db_obj();
        }
        catch (DevFailed ex) {}
       
        if (dbConnection != null)
        {
            String  vpsprefix;
                       
            vpsprefix = getClassPropFromTangoDB(dbConnection, "HSMPowerSupply", "VpsPrefix");
            if (vpsprefix == null)
                VPS_PREFIX = "m";
            else
                VPS_PREFIX = vpsprefix;
        }
        else
            VPS_PREFIX = "m";
        
//        System.out.println("VPS_PREFIX = "+ VPS_PREFIX + "\n");
    }
    
    private static String getClassPropFromTangoDB(Database tangodb, String className, String prop)
    { 
        if (tangodb == null) return null;
               
        DbDatum dbd = null;
        try
        {
            dbd = tangodb.get_class_property(className, prop);
        }
        catch (DevFailed ex) {}
           
        if (dbd == null) return null;
        if (dbd.is_empty()) return null;
       
        String argout = dbd.extractString();
        return argout;
    }
    
}
