/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hsm;

import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.core.IErrorListener;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javax.swing.JCheckBox;
import jebssrps.JEbsSrPsConstants;
import tangodb.TangoDbUtil;

/**
 *
 * @author poncet
 */
public class AllCellsConfigFrame extends javax.swing.JFrame
{
    static private AllCellsConfigFrame            self = null;
    
    private JCheckBox[]                    cbArray = new JCheckBox[32];
    private HashMap<JCheckBox, String>     cbToDevName = new HashMap<JCheckBox, String>();
    private HashMap<String, JCheckBox>     devNameToCb = new HashMap<String, JCheckBox>();
    
    private VoidVoidCommand                hsmSummaryInit = null;
    
    /** Creates new form AllCellsConfigFrame singleton */
    private AllCellsConfigFrame()
    {
        for (int i=0; i<cbArray.length; i++)
            cbArray[i] = null;
        initComponents();
        initMaps();
        
        CommandList  cmdl = new CommandList();
        cmdl.addErrorListener((IErrorListener) JEbsSrPsConstants.ERR_H);
        cmdl.addErrorListener((IErrorListener) ErrorPopup.getInstance());
        
        // Connect to the HSM device commands
        try
        {
            IEntity  oneEntity = cmdl.add(HsmConstants.HSM_SUMMARY_DEVNAME + "/Init");
            if (oneEntity instanceof VoidVoidCommand)
            {
                hsmSummaryInit = (VoidVoidCommand) oneEntity;
            }
        }
        catch (ConnectionException ex)
        {
            javax.swing.JOptionPane.showMessageDialog(null, "Cannot connect to the Hsm Summary Init command" + " .\n"
                    + "Check if the device " + HsmConstants.HSM_SUMMARY_DEVNAME + " is responding?\n",
                    "AllCellsConfigFrame : Command connection",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        }
        pack();
    }
    
    public static AllCellsConfigFrame getInstance()
    {
        if (self == null)
        {
            self = new AllCellsConfigFrame();
            self.setPreferredSize(new java.awt.Dimension(520, 380));
        }
        return self;
    }
    
    private void initMaps()
    {
        for (int i=0; i<cbArray.length; i++)
        {
            String  cellNbStr = String.format ("%02d", (i+1) );
            String  hsmDevName = HsmConstants.HSM_DEV_NAME_PATTERN + cellNbStr;
            
            cbToDevName.put(cbArray[i], hsmDevName);
            devNameToCb.put(hsmDevName, cbArray[i]);
        }
        
    }
    
    public void setVisible(boolean b)
    {
        if (b)
        {
            if (!isVisible())
            {
                refresh();
            }
        }
        super.setVisible(b);
    }
    
    
    private void saveCellList()
    {
        String[]  oldDevListArray = TangoDbUtil.getInstance().getDevicePropertyValues(HsmConstants.HSM_SUMMARY_DEVNAME, "HsmNames");
        List<String>        oldDevList = new ArrayList<String>(Arrays.asList(oldDevListArray));

        ArrayList<String>   newDevList = new ArrayList<String>();
        
        for (int i=0; i<cbArray.length; i++)
        {
            if (cbArray[i] == null) continue;
            if (cbArray[i].isSelected())
            {
                String devName = null;
                devName = cbToDevName.get(cbArray[i]);
                if (devName != null)
                {
                    newDevList.add(devName);
                }
            }
        }
        
        if (newDevList.isEmpty()) return; // don't remove all cells from the property
        if (newDevList.equals(oldDevList)) return; // no change in property is needed
        
        String[] strArray = newDevList.toArray( new String[newDevList.size()] );
        TangoDbUtil.getInstance().setDeviceProperty(HsmConstants.HSM_SUMMARY_DEVNAME, "HsmNames", strArray);
        
        if (hsmSummaryInit == null) return;
        
        try
        {
            Thread.sleep(500);
        }
        catch (InterruptedException ex) {}
        
        hsmSummaryInit.execute();        
    }
    
    private void refresh()
    {
        for (int i=0; i<cbArray.length; i++)
            cbArray[i].setSelected(false);
        String[]  devList = TangoDbUtil.getInstance().getDevicePropertyValues(HsmConstants.HSM_SUMMARY_DEVNAME, "HsmNames");
        
        for (int i=0; i<devList.length; i++)
        {
            if ( !(devNameToCb.containsKey(devList[i].toLowerCase())) )
                continue;
            
            JCheckBox cb = devNameToCb.get( devList[i].toLowerCase() );
            if (cb != null)
                cb.setSelected(true);
        }
    }
    
    

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        infoJPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cellListJPanel = new javax.swing.JPanel();
        cell1to8JPanel = new javax.swing.JPanel();
        c1cb = new javax.swing.JCheckBox();
        c2cb = new javax.swing.JCheckBox();
        c3cb = new javax.swing.JCheckBox();
        c4cb = new javax.swing.JCheckBox();
        c5cb = new javax.swing.JCheckBox();
        c6cb = new javax.swing.JCheckBox();
        c7cb = new javax.swing.JCheckBox();
        c8cb = new javax.swing.JCheckBox();
        cell9to16JPanel = new javax.swing.JPanel();
        c9cb = new javax.swing.JCheckBox();
        c10cb = new javax.swing.JCheckBox();
        c11cb = new javax.swing.JCheckBox();
        c12cb = new javax.swing.JCheckBox();
        c13cb = new javax.swing.JCheckBox();
        c14cb = new javax.swing.JCheckBox();
        c15cb = new javax.swing.JCheckBox();
        c16cb = new javax.swing.JCheckBox();
        cell17to24JPanel = new javax.swing.JPanel();
        c17cb = new javax.swing.JCheckBox();
        c18cb = new javax.swing.JCheckBox();
        c19cb = new javax.swing.JCheckBox();
        c20cb = new javax.swing.JCheckBox();
        c21cb = new javax.swing.JCheckBox();
        c22cb = new javax.swing.JCheckBox();
        c23cb = new javax.swing.JCheckBox();
        c24cb = new javax.swing.JCheckBox();
        cell25to32JPanel = new javax.swing.JPanel();
        c25cb = new javax.swing.JCheckBox();
        c26cb = new javax.swing.JCheckBox();
        c27cb = new javax.swing.JCheckBox();
        c28cb = new javax.swing.JCheckBox();
        c29cb = new javax.swing.JCheckBox();
        c30cb = new javax.swing.JCheckBox();
        c31cb = new javax.swing.JCheckBox();
        c32cb = new javax.swing.JCheckBox();
        cmdJPanel = new javax.swing.JPanel();
        refreshJButton = new javax.swing.JButton();
        dummyJPanel = new javax.swing.JPanel();
        updateJButton = new javax.swing.JButton();

        setTitle("Edit List of cells");
        getContentPane().setLayout(new java.awt.GridBagLayout());

        infoJPanel.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("List of cells used for global HSM commands");
        infoJPanel.add(jLabel1, new java.awt.GridBagConstraints());
        jLabel1.getAccessibleContext().setAccessibleName("List of cells used by All Cells commands");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(20, 5, 5, 5);
        getContentPane().add(infoJPanel, gridBagConstraints);

        cellListJPanel.setLayout(new java.awt.GridBagLayout());

        cell1to8JPanel.setLayout(new java.awt.GridBagLayout());

        c1cb.setText("C01");
        cbArray[0] = c1cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell1to8JPanel.add(c1cb, gridBagConstraints);

        c2cb.setText("C02");
        cbArray[1] = c2cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell1to8JPanel.add(c2cb, gridBagConstraints);

        c3cb.setText("C03");
        cbArray[2] = c3cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell1to8JPanel.add(c3cb, gridBagConstraints);

        c4cb.setText("C04");
        cbArray[3] = c4cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell1to8JPanel.add(c4cb, gridBagConstraints);

        c5cb.setText("C05");
        cbArray[4] = c5cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell1to8JPanel.add(c5cb, gridBagConstraints);

        c6cb.setText("C06");
        cbArray[5] = c6cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell1to8JPanel.add(c6cb, gridBagConstraints);

        c7cb.setText("C07");
        cbArray[6] = c7cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell1to8JPanel.add(c7cb, gridBagConstraints);

        c8cb.setText("C08");
        cbArray[7] = c8cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell1to8JPanel.add(c8cb, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.25;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        cellListJPanel.add(cell1to8JPanel, gridBagConstraints);

        cell9to16JPanel.setLayout(new java.awt.GridBagLayout());

        c9cb.setText("C09");
        cbArray[8] = c9cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell9to16JPanel.add(c9cb, gridBagConstraints);

        c10cb.setText("C10");
        cbArray[9] = c10cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell9to16JPanel.add(c10cb, gridBagConstraints);

        c11cb.setText("C11");
        cbArray[10] = c11cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell9to16JPanel.add(c11cb, gridBagConstraints);

        c12cb.setText("C12");
        cbArray[11] = c12cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell9to16JPanel.add(c12cb, gridBagConstraints);

        c13cb.setText("C13");
        cbArray[12] = c13cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell9to16JPanel.add(c13cb, gridBagConstraints);

        c14cb.setText("C14");
        cbArray[13] = c14cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell9to16JPanel.add(c14cb, gridBagConstraints);

        c15cb.setText("C15");
        cbArray[14] = c15cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell9to16JPanel.add(c15cb, gridBagConstraints);

        c16cb.setText("C16");
        cbArray[15] = c16cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell9to16JPanel.add(c16cb, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.25;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        cellListJPanel.add(cell9to16JPanel, gridBagConstraints);

        cell17to24JPanel.setLayout(new java.awt.GridBagLayout());

        c17cb.setText("C17");
        cbArray[16] = c17cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell17to24JPanel.add(c17cb, gridBagConstraints);

        c18cb.setText("C18");
        cbArray[17] = c18cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell17to24JPanel.add(c18cb, gridBagConstraints);

        c19cb.setText("C19");
        cbArray[18] = c19cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell17to24JPanel.add(c19cb, gridBagConstraints);

        c20cb.setText("C20");
        cbArray[19] = c20cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell17to24JPanel.add(c20cb, gridBagConstraints);

        c21cb.setText("C21");
        cbArray[20] = c21cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell17to24JPanel.add(c21cb, gridBagConstraints);

        c22cb.setText("C22");
        cbArray[21] = c22cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell17to24JPanel.add(c22cb, gridBagConstraints);

        c23cb.setText("C23");
        cbArray[22] = c23cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell17to24JPanel.add(c23cb, gridBagConstraints);

        c24cb.setText("C24");
        cbArray[23] = c24cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell17to24JPanel.add(c24cb, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.25;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        cellListJPanel.add(cell17to24JPanel, gridBagConstraints);

        cell25to32JPanel.setLayout(new java.awt.GridBagLayout());

        c25cb.setText("C25");
        cbArray[24] = c25cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell25to32JPanel.add(c25cb, gridBagConstraints);

        c26cb.setText("C26");
        cbArray[25] = c26cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell25to32JPanel.add(c26cb, gridBagConstraints);

        c27cb.setText("C27");
        cbArray[26] = c27cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell25to32JPanel.add(c27cb, gridBagConstraints);

        c28cb.setText("C28");
        cbArray[27] = c28cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell25to32JPanel.add(c28cb, gridBagConstraints);

        c29cb.setText("C29");
        cbArray[28] = c29cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell25to32JPanel.add(c29cb, gridBagConstraints);

        c30cb.setText("C30");
        cbArray[29] = c30cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell25to32JPanel.add(c30cb, gridBagConstraints);

        c31cb.setText("C31");
        cbArray[30] = c31cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell25to32JPanel.add(c31cb, gridBagConstraints);

        c32cb.setText("C32");
        cbArray[31] = c32cb;
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
        cell25to32JPanel.add(c32cb, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.25;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        cellListJPanel.add(cell25to32JPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 5, 10, 5);
        getContentPane().add(cellListJPanel, gridBagConstraints);

        cmdJPanel.setLayout(new java.awt.GridBagLayout());

        refreshJButton.setText("Refresh");
        refreshJButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                refreshJButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        cmdJPanel.add(refreshJButton, gridBagConstraints);

        javax.swing.GroupLayout dummyJPanelLayout = new javax.swing.GroupLayout(dummyJPanel);
        dummyJPanel.setLayout(dummyJPanelLayout);
        dummyJPanelLayout.setHorizontalGroup(
            dummyJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        dummyJPanelLayout.setVerticalGroup(
            dummyJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        cmdJPanel.add(dummyJPanel, gridBagConstraints);

        updateJButton.setText("Update");
        updateJButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                updateJButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        cmdJPanel.add(updateJButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        getContentPane().add(cmdJPanel, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void refreshJButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_refreshJButtonActionPerformed
    {//GEN-HEADEREND:event_refreshJButtonActionPerformed
        // TODO add your handling code here:
        refresh();
    }//GEN-LAST:event_refreshJButtonActionPerformed

    private void updateJButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_updateJButtonActionPerformed
    {//GEN-HEADEREND:event_updateJButtonActionPerformed
        // TODO add your handling code here:
        saveCellList();
    }//GEN-LAST:event_updateJButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(AllCellsConfigFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(AllCellsConfigFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(AllCellsConfigFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(AllCellsConfigFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AllCellsConfigFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox c10cb;
    private javax.swing.JCheckBox c11cb;
    private javax.swing.JCheckBox c12cb;
    private javax.swing.JCheckBox c13cb;
    private javax.swing.JCheckBox c14cb;
    private javax.swing.JCheckBox c15cb;
    private javax.swing.JCheckBox c16cb;
    private javax.swing.JCheckBox c17cb;
    private javax.swing.JCheckBox c18cb;
    private javax.swing.JCheckBox c19cb;
    private javax.swing.JCheckBox c1cb;
    private javax.swing.JCheckBox c20cb;
    private javax.swing.JCheckBox c21cb;
    private javax.swing.JCheckBox c22cb;
    private javax.swing.JCheckBox c23cb;
    private javax.swing.JCheckBox c24cb;
    private javax.swing.JCheckBox c25cb;
    private javax.swing.JCheckBox c26cb;
    private javax.swing.JCheckBox c27cb;
    private javax.swing.JCheckBox c28cb;
    private javax.swing.JCheckBox c29cb;
    private javax.swing.JCheckBox c2cb;
    private javax.swing.JCheckBox c30cb;
    private javax.swing.JCheckBox c31cb;
    private javax.swing.JCheckBox c32cb;
    private javax.swing.JCheckBox c3cb;
    private javax.swing.JCheckBox c4cb;
    private javax.swing.JCheckBox c5cb;
    private javax.swing.JCheckBox c6cb;
    private javax.swing.JCheckBox c7cb;
    private javax.swing.JCheckBox c8cb;
    private javax.swing.JCheckBox c9cb;
    private javax.swing.JPanel cell17to24JPanel;
    private javax.swing.JPanel cell1to8JPanel;
    private javax.swing.JPanel cell25to32JPanel;
    private javax.swing.JPanel cell9to16JPanel;
    private javax.swing.JPanel cellListJPanel;
    private javax.swing.JPanel cmdJPanel;
    private javax.swing.JPanel dummyJPanel;
    private javax.swing.JPanel infoJPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton refreshJButton;
    private javax.swing.JButton updateJButton;
    // End of variables declaration//GEN-END:variables

}
