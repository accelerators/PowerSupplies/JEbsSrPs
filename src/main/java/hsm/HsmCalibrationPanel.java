package hsm;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributePolledList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.DeviceFactory;
import fr.esrf.tangoatk.core.IErrorListener;
import fr.esrf.tangoatk.core.INumberScalar;
import fr.esrf.tangoatk.core.ISetErrorListener;
import fr.esrf.tangoatk.core.attribute.DevStateScalar;
import fr.esrf.tangoatk.core.attribute.NumberScalar;
import fr.esrf.tangoatk.core.attribute.StringScalar;
import fr.esrf.tangoatk.widget.attribute.NumberScalarTextEditor;
import fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import fr.esrf.tangoatk.widget.util.Splash;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import jebssrps.JEbsSrPsConstants;
import static jebssrps.JEbsSrPsConstants.C3_PS_CHANNEL_DEV_NAMES;
import static jebssrps.JEbsSrPsConstants.C4_PS_CHANNEL_DEV_NAMES;
import static jebssrps.JEbsSrPsConstants.EBS_PS_CHANNEL_DEV_NAMES;
import viewers.PSStateViewer;

/**
 *
 * @author pons
 */
public class HsmCalibrationPanel extends javax.swing.JPanel {
    
    private String          hsmDevName = null;
    private String          cellNbStr = null;
    
    private String[] psDevNames= null;
    private PSStateViewer[] psStates = null;
    private SimpleScalarViewer[] statumCurrents = null;
    private SimpleScalarViewer[] hsmCurrents = null;
    private NumberScalarTextEditor[] gains = null;
    private NumberScalarTextEditor[] offsets = null;
    private JButton[] calibSingleBtn = null;
    private GridBagConstraints gbc;
    // ok i don't understand why for the moment but when you restart the HSMAccess server. I don't restore all connection on state in calibration tabled
    private AttributeList     attl = new AttributePolledList();
    
    private final String CALIBRATION_CONFIRM_MESSAGE = "Calibration must be done without beam. Do you confirm the calibration ?";
    private final String CALIBRATION_CONFIRM_TITLE = "Calibration ?";

    /**
     * Creates new form HsmCalibrationPanel
     */
    public HsmCalibrationPanel(String  deviceName) {

        hsmDevName = deviceName;
        int  index = hsmDevName.lastIndexOf("/");
        cellNbStr = hsmDevName.substring(index + 2);
        
        attl.addErrorListener((IErrorListener)JEbsSrPsConstants.ERR_H);
        attl.addSetErrorListener((ISetErrorListener)ErrorPopup.getInstance());
        
        initComponents();

        titleLabel.setText("Cell " + cellNbStr);
        
        gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets.top = 2;
        gbc.insets.bottom = 2;
        gbc.insets.left = 2;
        gbc.insets.right = 2;
        
        JLabel h1 = new JLabel("Current");
        h1.setHorizontalAlignment(JLabel.CENTER);
        addTable(h1,1,0);
        JLabel h2 = new JLabel("HSM Current");
        h2.setHorizontalAlignment(JLabel.CENTER);
        addTable(h2,2,0);
        JLabel h3 = new JLabel("Gain");
        h3.setHorizontalAlignment(JLabel.CENTER);
        addTable(h3,3,0);
        JLabel h4 = new JLabel("Offset (mA)");
        h4.setHorizontalAlignment(JLabel.CENTER);
        addTable(h4,4,0);
    }
    
    private void initViewers() {
        
        if( psDevNames!=null )
            // Already initialised
            return;
                
        String[] psNames;
        if(cellNbStr.equals("03")) {
            psNames = C3_PS_CHANNEL_DEV_NAMES;
        } else if(cellNbStr.equals("04")) {
            psNames = C4_PS_CHANNEL_DEV_NAMES;            
        } else {
            psNames = EBS_PS_CHANNEL_DEV_NAMES;
        }

        Splash sw = new Splash();
        sw.setTitle("Calibration C" + cellNbStr);
        sw.setMaxProgress(psNames.length);
        sw.progress(0);
        sw.setVisible(true);

        psStates = new PSStateViewer[psNames.length];
        statumCurrents = new SimpleScalarViewer[psNames.length];
        hsmCurrents = new SimpleScalarViewer[psNames.length];
        gains = new NumberScalarTextEditor[psNames.length];
        offsets = new NumberScalarTextEditor[psNames.length];
        calibSingleBtn = new JButton[psNames.length];
        psDevNames = new String[psNames.length];
        
        for(int i=0;i<psNames.length;i++) {

           final String devName = psNames[i].replace("##", cellNbStr);           
           psDevNames[i] = devName;
           psStates[i] = new PSStateViewer(devName);           
           try {
             DevStateScalar model = (DevStateScalar)attl.add(devName+"/State");
             StringScalar cModel = (StringScalar)attl.add(devName+"/ConnectedName");
             psStates[i].setModel(model);
             psStates[i].setNameModel(cModel);
           } catch (ConnectionException e) {}
           gbc.weightx = 1.0;
           addTable(psStates[i],0,i+1);
           gbc.weightx = 0.0;

           statumCurrents[i] = new SimpleScalarViewer();
           statumCurrents[i].setAlarmEnabled(false);
           statumCurrents[i].setBackgroundColor(getBackground());
           try {
             INumberScalar model = (INumberScalar)attl.add(devName+"/Current");
             statumCurrents[i].setModel(model);
           } catch (ConnectionException e) {}
           addTable(statumCurrents[i],1,i+1,100);
           
           hsmCurrents[i] = new SimpleScalarViewer();
           hsmCurrents[i].setAlarmEnabled(false);
           hsmCurrents[i].setBackgroundColor(getBackground());
           try {
             INumberScalar model = (INumberScalar)attl.add(devName+"/HSMCurrent");
             hsmCurrents[i].setModel(model);
           } catch (ConnectionException e) {}
           addTable(hsmCurrents[i],2,i+1,100);
           
           gains[i] = new NumberScalarTextEditor();
           gains[i].setReadError("---");
           try {
             NumberScalar model = (NumberScalar)attl.add(devName+"/MOSPlateGain");
             gains[i].setModel(model);
           } catch (ConnectionException e) {}
           addTable(gains[i],3,i+1,50);
           
           offsets[i] = new NumberScalarTextEditor();
           offsets[i].setReadError("---");
           try {
             NumberScalar model = (NumberScalar)attl.add(devName+"/MOSPlateOffset");
             offsets[i].setModel(model);
           } catch (ConnectionException e) {}
           addTable(offsets[i],4,i+1,50);

           calibSingleBtn[i] = new JButton("Calibrate ...");
           calibSingleBtn[i].addActionListener((ActionEvent e) -> {
               if(JOptionPane.YES_OPTION != JOptionPane.showConfirmDialog(mainTablePanel,
                       CALIBRATION_CONFIRM_MESSAGE,
                       CALIBRATION_CONFIRM_TITLE,
                       JOptionPane.YES_NO_OPTION)){
                   return;
               }
               calibrate(devName);
           });
           addTable(calibSingleBtn[i],5,i+1,10);

           sw.progress(i);

        }
        
        sw.setVisible(false);
        
    }
    
    private void addTable(JComponent c,int x,int y) {
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.ipadx = 0;
        tablePanel.add(c,gbc);
    }
    
    private void addTable(JComponent c,int x,int y,int paddx) {
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.ipadx = paddx;
        tablePanel.add(c,gbc);
    }
    
    public void setVisible(boolean visible) {
        
        super.setVisible(visible);
        if(visible) {
            initViewers();
            attl.startRefresher();
        } else {
            attl.stopRefresher();
        }
        
    }
    
    private DeviceData getCalibArgs() {

        try {

            double lowCurrent = Double.parseDouble(lowCurrentText.getText());
            double lowWait = Double.parseDouble(waitLowText.getText());
            double highCurrent = Double.parseDouble(highCurrentText.getText());
            double highWait = Double.parseDouble(waitHighText.getText());
            
            if (highCurrent > 115) {
                JOptionPane.showMessageDialog(mainTablePanel, "High current exceed limit (115A)",
                        "Calibration", JOptionPane.ERROR_MESSAGE);
                return null;
            }
            if (highCurrent < lowCurrent) {
                JOptionPane.showMessageDialog(mainTablePanel, "High current must be greater than low current",
                        "Calibration", JOptionPane.ERROR_MESSAGE);
                return null;
            }
            if (lowWait > 30 || lowWait < 10) {
                JOptionPane.showMessageDialog(mainTablePanel, "Low current wait time out of bounds (10-30s)",
                        "Calibration", JOptionPane.ERROR_MESSAGE);
                return null;
            }
            if (highWait > 30 || highWait < 10) {
                JOptionPane.showMessageDialog(mainTablePanel, "High current wait time out of bounds (10-30s)",
                        "Calibration", JOptionPane.ERROR_MESSAGE);
                return null;
            }

            DeviceData argin = new DeviceData();
            double[] args = new double[4];
            args[0] = lowCurrent;
            args[1] = lowWait;
            args[2] = highCurrent;
            args[3] = highWait;
            argin.insert(args);
            return argin;

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(mainTablePanel, "Wrong calibration input parameters: " + e.getMessage(),
                    "Calibration", JOptionPane.ERROR_MESSAGE);
        } catch (DevFailed e) {
            JOptionPane.showMessageDialog(mainTablePanel, "Error: " + e.errors[0].desc,
                    "Calibration", JOptionPane.ERROR_MESSAGE);
        }

        return null;

    }
    
    private void calibrate(String devName) {
        
        DeviceData argin = getCalibArgs();
        if(argin==null) return;

        try {
                        
            DeviceProxy ds = DeviceFactory.getInstance().getDevice(devName);
            ds.command_inout("Calibrate",argin);            
            
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(mainTablePanel, "Wrong input: " + e.getMessage(),
                    "Calibration: " + devName, JOptionPane.ERROR_MESSAGE);
        } catch (ConnectionException e) {
            JOptionPane.showMessageDialog(mainTablePanel, "Error: " + e.getDescription(),
                    "Calibration: " + devName, JOptionPane.ERROR_MESSAGE);
        } catch (DevFailed e) {
            JOptionPane.showMessageDialog(mainTablePanel, "Error: " + e.errors[0].desc,
                    "Calibration: " + devName, JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
    private boolean sextuTo5A() {

        for (int i = 0; i < psDevNames.length; i++) {

            if (psDevNames[i].contains("sd") || psDevNames[i].contains("sf")) {

                try {
                    DeviceProxy ds = DeviceFactory.getInstance().getDevice(psDevNames[i]);
                    DeviceAttribute da = new DeviceAttribute("Current");
                    da.insert(5.0);
                    ds.write_attribute(da);
                } catch (ConnectionException e) {
                    JOptionPane.showMessageDialog(mainTablePanel, "Error when setting current: " + psDevNames[i] + "\n" + e.getDescription(),
                            "Calibration", JOptionPane.ERROR_MESSAGE);
                    return false;
                } catch (DevFailed e) {
                    JOptionPane.showMessageDialog(mainTablePanel, "Error when setting current: " + psDevNames[i] + "\n" + e.errors[0].desc,
                            "Calibration", JOptionPane.ERROR_MESSAGE);
                    return false;
                }

            }

        }

        return true;

    }

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        titlePanel = new javax.swing.JPanel();
        titleLabel = new javax.swing.JLabel();
        mainTablePanel = new javax.swing.JPanel();
        tableScrollPane = new javax.swing.JScrollPane();
        tablePanel = new javax.swing.JPanel();
        btnPanel = new javax.swing.JPanel();
        abortCalibrationButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lowCurrentText = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        highCurrentText = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        waitLowText = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        waitHighText = new javax.swing.JTextField();
        saveCalibrationButton = new javax.swing.JButton();
        allQButton = new javax.swing.JButton();
        allDQButton = new javax.swing.JButton();
        allSOButton = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        titleLabel.setFont(new java.awt.Font("sansserif", 1, 14)); // NOI18N
        titleLabel.setText("Cell ...");
        titlePanel.add(titleLabel);

        add(titlePanel, java.awt.BorderLayout.PAGE_START);

        mainTablePanel.setLayout(new java.awt.BorderLayout());

        tablePanel.setLayout(new java.awt.GridBagLayout());
        tableScrollPane.setViewportView(tablePanel);

        mainTablePanel.add(tableScrollPane, java.awt.BorderLayout.CENTER);

        add(mainTablePanel, java.awt.BorderLayout.CENTER);

        btnPanel.setLayout(new java.awt.GridBagLayout());

        abortCalibrationButton.setText("Abort calibration");
        abortCalibrationButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                abortCalibrationButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        btnPanel.add(abortCalibrationButton, gridBagConstraints);

        jLabel1.setText("Low current (A)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        btnPanel.add(jLabel1, gridBagConstraints);

        lowCurrentText.setText("5");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 20;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        btnPanel.add(lowCurrentText, gridBagConstraints);

        jLabel2.setText("High current (A)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        btnPanel.add(jLabel2, gridBagConstraints);

        highCurrentText.setText("100");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 20;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        btnPanel.add(highCurrentText, gridBagConstraints);

        jLabel3.setText("Waiting time (low) (s)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        btnPanel.add(jLabel3, gridBagConstraints);

        waitLowText.setText("30");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 20;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        btnPanel.add(waitLowText, gridBagConstraints);

        jLabel4.setText("Waiting time (high) (s)");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        btnPanel.add(jLabel4, gridBagConstraints);

        waitHighText.setText("30");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 20;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        btnPanel.add(waitHighText, gridBagConstraints);

        saveCalibrationButton.setText("Save to DB");
        saveCalibrationButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveCalibrationButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        btnPanel.add(saveCalibrationButton, gridBagConstraints);

        allQButton.setText("Calib. All Q ...");
        allQButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                allQButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        btnPanel.add(allQButton, gridBagConstraints);

        allDQButton.setText("Calib. All DQ ...");
        allDQButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                allDQButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        btnPanel.add(allDQButton, gridBagConstraints);

        allSOButton.setText("Calib. All SO ...");
        allSOButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                allSOButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        btnPanel.add(allSOButton, gridBagConstraints);

        add(btnPanel, java.awt.BorderLayout.SOUTH);
    }// </editor-fold>//GEN-END:initComponents

    private void abortCalibrationButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_abortCalibrationButtonActionPerformed

        for (int i = 0; i < psDevNames.length; i++) {

            try {

                DeviceProxy ds = DeviceFactory.getInstance().getDevice(psDevNames[i]);
                ds.command_inout("AbortCalibration");

            } catch (ConnectionException e) {
                JOptionPane.showMessageDialog(mainTablePanel, "Error: " + e.getDescription(),
                        "Calibration: " + psDevNames[i], JOptionPane.ERROR_MESSAGE);
            } catch (DevFailed e) {
                JOptionPane.showMessageDialog(mainTablePanel, "Error: " + e.errors[0].desc,
                        "Calibration: " + psDevNames[i], JOptionPane.ERROR_MESSAGE);
            }

        }
        
    }//GEN-LAST:event_abortCalibrationButtonActionPerformed

    private void saveCalibrationButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveCalibrationButtonActionPerformed

        for (int i = 0; i < psDevNames.length; i++) {

            try {

                DeviceProxy ds = DeviceFactory.getInstance().getDevice(psDevNames[i]);
                ds.command_inout("SaveCalibration");

            } catch (ConnectionException e) {
                JOptionPane.showMessageDialog(mainTablePanel, "Error: " + e.getDescription(),
                        "Calibration: " + psDevNames[i], JOptionPane.ERROR_MESSAGE);
            } catch (DevFailed e) {
                JOptionPane.showMessageDialog(mainTablePanel, "Error: " + e.errors[0].desc,
                        "Calibration: " + psDevNames[i], JOptionPane.ERROR_MESSAGE);
            }

        }
        
    }//GEN-LAST:event_saveCalibrationButtonActionPerformed

    private void allQButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allQButtonActionPerformed
        if(JOptionPane.YES_OPTION != JOptionPane.showConfirmDialog(mainTablePanel,
                CALIBRATION_CONFIRM_MESSAGE,
                CALIBRATION_CONFIRM_TITLE,
                JOptionPane.YES_NO_OPTION)){
            return;
        }
        
        DeviceData argin = getCalibArgs();
        if(argin==null) 
            return;
        
        if( !sextuTo5A() ) 
            return;
            
        for (int i = 0; i < psDevNames.length; i++) {

            if (psDevNames[i].contains("-qd") || 
                psDevNames[i].contains("-qf")) {

                try {
                    DeviceProxy ds = DeviceFactory.getInstance().getDevice(psDevNames[i]);
                    ds.command_inout("Calibrate",argin);            
                } catch (ConnectionException e) {
                    JOptionPane.showMessageDialog(mainTablePanel, "Calibration error: " + psDevNames[i] + "\n" + e.getDescription(),
                            "Calibration", JOptionPane.ERROR_MESSAGE);
                } catch (DevFailed e) {
                    JOptionPane.showMessageDialog(mainTablePanel, "Calibration error: " + psDevNames[i] + "\n" + e.errors[0].desc,
                            "Calibration", JOptionPane.ERROR_MESSAGE);
                }

            }

        }
        
    }//GEN-LAST:event_allQButtonActionPerformed

    private void allDQButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allDQButtonActionPerformed
        if(JOptionPane.YES_OPTION != JOptionPane.showConfirmDialog(this,
                CALIBRATION_CONFIRM_MESSAGE,
                CALIBRATION_CONFIRM_TITLE,
                JOptionPane.YES_NO_OPTION)){
            return;
        }
        
        
        DeviceData argin = getCalibArgs();
        if(argin==null) 
            return;
        
        if( !sextuTo5A() ) 
            return;
            
        for (int i = 0; i < psDevNames.length; i++) {

            if (psDevNames[i].contains("-dq")) {

                try {
                    DeviceProxy ds = DeviceFactory.getInstance().getDevice(psDevNames[i]);
                    ds.command_inout("Calibrate",argin);            
                } catch (ConnectionException e) {
                    JOptionPane.showMessageDialog(mainTablePanel, "Calibration error: " + psDevNames[i] + "\n" + e.getDescription(),
                            "Calibration", JOptionPane.ERROR_MESSAGE);
                } catch (DevFailed e) {
                    JOptionPane.showMessageDialog(mainTablePanel, "Calibration error: " + psDevNames[i] + "\n" + e.errors[0].desc,
                            "Calibration", JOptionPane.ERROR_MESSAGE);
                }

            }

        }
        
    }//GEN-LAST:event_allDQButtonActionPerformed

    private void allSOButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_allSOButtonActionPerformed
        if(JOptionPane.YES_OPTION != JOptionPane.showConfirmDialog(this,
                CALIBRATION_CONFIRM_MESSAGE,
                CALIBRATION_CONFIRM_TITLE,
                JOptionPane.YES_NO_OPTION)){
            return;
        }
        
        DeviceData argin = getCalibArgs();
        if(argin==null) 
            return;
        
        if( !sextuTo5A() ) 
            return;
            
        for (int i = 0; i < psDevNames.length; i++) {

            if (psDevNames[i].contains("-s") || 
                psDevNames[i].contains("-o")){

                try {
                    DeviceProxy ds = DeviceFactory.getInstance().getDevice(psDevNames[i]);
                    ds.command_inout("Calibrate",argin);            
                } catch (ConnectionException e) {
                    JOptionPane.showMessageDialog(mainTablePanel, "Calibration error: " + psDevNames[i] + "\n" + e.getDescription(),
                            "Calibration", JOptionPane.ERROR_MESSAGE);
                } catch (DevFailed e) {
                    JOptionPane.showMessageDialog(mainTablePanel, "Calibration error: " + psDevNames[i] + "\n" + e.errors[0].desc,
                            "Calibration", JOptionPane.ERROR_MESSAGE);
                }

            }

        }
    }//GEN-LAST:event_allSOButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton abortCalibrationButton;
    private javax.swing.JButton allDQButton;
    private javax.swing.JButton allQButton;
    private javax.swing.JButton allSOButton;
    private javax.swing.JPanel btnPanel;
    private javax.swing.JTextField highCurrentText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField lowCurrentText;
    private javax.swing.JPanel mainTablePanel;
    private javax.swing.JButton saveCalibrationButton;
    private javax.swing.JPanel tablePanel;
    private javax.swing.JScrollPane tableScrollPane;
    private javax.swing.JLabel titleLabel;
    private javax.swing.JPanel titlePanel;
    private javax.swing.JTextField waitHighText;
    private javax.swing.JTextField waitLowText;
    // End of variables declaration//GEN-END:variables

    private void popupWithoutBeam() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
