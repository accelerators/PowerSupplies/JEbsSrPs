/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hsm;

import jebssrps.*;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.DeviceFactory;
import fr.esrf.tangoatk.core.ICommand;
import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.jdraw.SynopticProgressListener;
import fr.esrf.tangoatk.widget.jdraw.TangoSynopticHandler;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import fr.esrf.tangoatk.widget.util.Splash;
import java.awt.GridBagConstraints;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.MissingResourceException;
import viewers.SrpsMainSynopticViewer;

/**
 *
 * @author kramer
 */
public class HsmMainWindow extends javax.swing.JFrame implements SynopticProgressListener {

    CommandList          cmdl = new CommandList();    
    ICommand             hsmOnCmd = null, hsmOffCmd = null, hsmStandbyCmd = null, hsmDisableCmd = null, hsmEnableCmd = null;
    Splash sw;
    
    /**
     * Creates new form EbsSrPs
     */
    public HsmMainWindow() {
        
        setTitle("JHsm  " + JEbsSrPsConstants.APPLI_VERSION_TAG);
       
        sw = new Splash();
        sw.setVisible(true);
        sw.setTitle("JHsm  " + JEbsSrPsConstants.APPLI_VERSION_TAG);
        sw.setCopyright("(c) ESRF 2021 - 2022");
        sw.setMessage("Connecting to Hsm devices ...");
        sw.setMaxProgress(100);
        sw.initProgress();
        
        initComponents();
                
        addSynoptic();
        
        cmdl.addErrorListener(ErrorPopup.getInstance());
        cmdl.addErrorListener(JEbsSrPsConstants.ERR_H);
        
        connectToHsmSummaryCommands();
        
        sw.setVisible(false);
        ATKGraphicsUtils.centerFrameOnScreen(this);
        pack();
        hsmSplitPane.setDividerLocation((int) 420);
        setVisible(true);
    }

    @Override
    public void progress(double d) {
        sw.progress((int)(d*100.0) );
    }

    private void addSynoptic()
    {  
        boolean  isHsm = true;
        
        SrpsMainSynopticViewer  synoptViewer = new SrpsMainSynopticViewer(hsmPanelPlaceHolder, isHsm);
        synoptViewer.setProgressListener(this);
        
        // Load the synoptic file                                
        try
        {
            synoptViewer.setErrorHistoryWindow(jebssrps.JEbsSrPsConstants.ERR_H);
            synoptViewer.setAutoZoom(true);
        }
        catch (Exception setErrwExcept)
        {
            System.out.println("Cannot set Error History Window for Synoptic");
        }

        InputStream jdFileInStream;
        jdFileInStream = jebssrps.JEbsSrPsConstants.class.getClassLoader().getResourceAsStream(hsm.HsmConstants.HSM_SYNOPTIC_FILE_NAME);

        if (jdFileInStream == null)
        {
            javax.swing.JOptionPane.showMessageDialog(
                    null, "Failed to get the inputStream for the synoptic file resource \n",
                    "Resource error",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
        
        java.io.InputStreamReader inStrReader = new InputStreamReader(jdFileInStream);
        try
        {
            synoptViewer.loadMainSynopticFromStream(inStrReader);
            synoptViewer.setToolTipMode(TangoSynopticHandler.TOOL_TIP_NAME);
        }
        catch (IOException ioex)
        {
            javax.swing.JOptionPane.showMessageDialog(
                        null, "Cannot find load the synoptic input stream reader.\n"
                        + " Application will abort ...\n"
                        + ioex,
                        "Resource access failed",
                        javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
        catch (MissingResourceException mrEx)
        {
            javax.swing.JOptionPane.showMessageDialog(
                        null, "Cannot parse the synoptic file.\n"
                        + " Application will abort ...\n"
                        + mrEx,
                        "Cannot parse the file",
                        javax.swing.JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }                       
        
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.fill = java.awt.GridBagConstraints.BOTH;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.insets = new java.awt.Insets(5, 5, 5, 5);
        hsmSynopticPlaceHolder.add(synoptViewer, gbc);
        
    }
    
    void connectToHsmSummaryCommands()
    {
        String  msgcmd = "Cannot connect to the command  : ";
        String  msg    = null;
        String  entityname = null;
        IEntity oneEntity = null;
               
        try
        {
            entityname = HsmConstants.HSM_SUMMARY_DEVNAME + "/On";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand)
            {
                hsmOnCmd = (ICommand) oneEntity;
            }
            
            entityname = HsmConstants.HSM_SUMMARY_DEVNAME + "/Off";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand)
            {
                hsmOffCmd = (ICommand) oneEntity;
            }
            
            entityname = HsmConstants.HSM_SUMMARY_DEVNAME + "/Standby";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand)
            {
                hsmStandbyCmd = (ICommand) oneEntity;
            }
            
            entityname = HsmConstants.HSM_SUMMARY_DEVNAME + "/Disable";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand)
            {
                hsmDisableCmd = (ICommand) oneEntity;
            }
            
            entityname = HsmConstants.HSM_SUMMARY_DEVNAME + "/Enable";
            msg = msgcmd + entityname;
            oneEntity = cmdl.add(entityname);
            if (oneEntity instanceof VoidVoidCommand)
            {
                hsmEnableCmd = (ICommand) oneEntity;
            }
        }
        catch (ConnectionException ex) {}
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        java.awt.GridBagConstraints gridBagConstraints;

        hsmSplitPane = new javax.swing.JSplitPane();
        hsmSynopticPlaceHolder = new javax.swing.JPanel();
        hsmPanelPlaceHolder = new javax.swing.JPanel();
        ebssrpsMenuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        exitJMenuItem = new javax.swing.JMenuItem();
        viewMenu = new javax.swing.JMenu();
        errorsMenuItem = new javax.swing.JMenuItem();
        diagMenuItem = new javax.swing.JMenuItem();
        HsmMenu = new javax.swing.JMenu();
        editCellsListJMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        hsmSplitPane.setOneTouchExpandable(true);

        hsmSynopticPlaceHolder.setPreferredSize(new java.awt.Dimension(450, 570));
        hsmSynopticPlaceHolder.setLayout(new java.awt.GridBagLayout());
        hsmSplitPane.setLeftComponent(hsmSynopticPlaceHolder);

        hsmPanelPlaceHolder.setPreferredSize(new java.awt.Dimension(600, 570));
        hsmPanelPlaceHolder.setLayout(new java.awt.GridBagLayout());
        hsmSplitPane.setRightComponent(hsmPanelPlaceHolder);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        getContentPane().add(hsmSplitPane, gridBagConstraints);

        fileMenu.setText("File");

        exitJMenuItem.setText("Quit");
        exitJMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                exitJMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitJMenuItem);

        ebssrpsMenuBar.add(fileMenu);

        viewMenu.setText("View");

        errorsMenuItem.setText("Error History ...");
        errorsMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                errorsMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(errorsMenuItem);

        diagMenuItem.setText("Diagnostics...");
        diagMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                diagMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(diagMenuItem);

        ebssrpsMenuBar.add(viewMenu);

        HsmMenu.setText("Hsm");

        editCellsListJMenuItem.setText("Edit List of cells ...");
        editCellsListJMenuItem.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                editCellsListJMenuItemActionPerformed(evt);
            }
        });
        HsmMenu.add(editCellsListJMenuItem);

        ebssrpsMenuBar.add(HsmMenu);

        setJMenuBar(ebssrpsMenuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitJMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitJMenuItemActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_exitJMenuItemActionPerformed

    private void errorsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_errorsMenuItemActionPerformed
        // TODO add your handling code here:
        ATKGraphicsUtils.centerFrame(rootPane, JEbsSrPsConstants.ERR_H);
        JEbsSrPsConstants.ERR_H.setVisible(true);
    }//GEN-LAST:event_errorsMenuItemActionPerformed

    private void diagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diagMenuItemActionPerformed
        // TODO add your handling code here:
        fr.esrf.tangoatk.widget.util.ATKDiagnostic.showDiagnostic();
    }//GEN-LAST:event_diagMenuItemActionPerformed

    private void editCellsListJMenuItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_editCellsListJMenuItemActionPerformed
    {//GEN-HEADEREND:event_editCellsListJMenuItemActionPerformed
        // TODO add your handling code here:
        ATKGraphicsUtils.centerFrame(this.getRootPane(), AllCellsConfigFrame.getInstance());
        AllCellsConfigFrame.getInstance().setVisible(true);       
    }//GEN-LAST:event_editCellsListJMenuItemActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(EbsSrPsMainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(EbsSrPsMainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(EbsSrPsMainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(EbsSrPsMainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                if (args.length > 0)
                {
                    if (args[0].equalsIgnoreCase("--trace"))
                    {
                        if (args.length == 1)
                            DeviceFactory.getInstance().setTraceMode(JEbsSrPsConstants.ATK_TRACE_LEVEL);
                        else
                        {
                            String  traceMskStr = args[1];
                            int     traceMask = -1;
                            try
                            {
                                traceMask = Integer.parseInt(traceMskStr);
                            }
                            catch (NumberFormatException ex) {}
                            if (traceMask == -1)
                                DeviceFactory.getInstance().setTraceMode(JEbsSrPsConstants.ATK_TRACE_LEVEL);
                            else
                                DeviceFactory.getInstance().setTraceMode(traceMask);
                        }
                    }
                }
                new HsmMainWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu HsmMenu;
    private javax.swing.JMenuItem diagMenuItem;
    private javax.swing.JMenuBar ebssrpsMenuBar;
    private javax.swing.JMenuItem editCellsListJMenuItem;
    private javax.swing.JMenuItem errorsMenuItem;
    private javax.swing.JMenuItem exitJMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JPanel hsmPanelPlaceHolder;
    private javax.swing.JSplitPane hsmSplitPane;
    private javax.swing.JPanel hsmSynopticPlaceHolder;
    private javax.swing.JMenu viewMenu;
    // End of variables declaration//GEN-END:variables

}
