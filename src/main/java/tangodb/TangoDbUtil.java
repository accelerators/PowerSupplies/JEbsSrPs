/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tangodb;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;

/**
 * Singleton class providing all utility methods to access Tango Data Base, 
 * get all instances of a server, register a new tango server,
 * start a new server ....etc.
 * @author poncet
 */

public class TangoDbUtil
{
    private static TangoDbUtil           self = null;
    private static Database              dbConnection = null;
    
    public static final String          TANGOHOST_NOTSET = "No TANGO_HOST";
    
    private String                      currentTH = TANGOHOST_NOTSET;


    private TangoDbUtil()
    {
        try
        {
            currentTH = ApiUtil.getTangoHost();
        }
        catch (DevFailed e)
        {
            System.out.println("TANGO_HOST not defined ...");
            currentTH = TANGOHOST_NOTSET;
        }
        
        if (currentTH.equalsIgnoreCase(TANGOHOST_NOTSET)) return;
        
        try
        {            
            connectToDB();
        }
        catch (DevFailed ex)
        {
            dbConnection = null;
        }
    }

    public static TangoDbUtil getInstance()
    {
        if (self == null)
            self = new TangoDbUtil();
        return self;
    }
    
    
    private static void connectToDB() throws DevFailed
    {
        if (dbConnection != null) return;
        dbConnection = ApiUtil.get_db_obj();
    }

    public boolean createDeviceProperties(String devName, DbDatum[] propData)
    {
        try
        {
            if (dbConnection == null) connectToDB();
        }
        catch (DevFailed df) {}

        if (dbConnection == null) return false;
        try
        {
            dbConnection.put_device_property(devName, propData);
        }
        catch (DevFailed ex)
        {
            return false;
        }
        return true;
    }

    public boolean setDeviceProperty(String devName, String propName, String[] propVal)
    {
        DbDatum[]   devProp = new DbDatum[1];

        devProp[0] = new DbDatum(propName, propVal);

        return (createDeviceProperties(devName, devProp));
    }

    public String getDevicePropertyValue(String deviceName, String propName)
    {
        try
        {
            if (dbConnection == null) connectToDB();
        }
        catch (DevFailed df)
        {
            return null;
        }

        if (dbConnection == null) return null;
        DbDatum propDbd = null;
        try
        {
            propDbd = dbConnection.get_device_property(deviceName, propName);
        }
        catch (DevFailed ex)
        {
            return null;
        }

        String propVal = null;
        propVal = propDbd.extractString();
        return propVal;
    }

    public String[] getDevicePropertyValues(String deviceName, String propName)
    {
        try
        {
            if (dbConnection == null) connectToDB();
        }
        catch (DevFailed df)
        {
            return null;
        }

        if (dbConnection == null) return null;
        DbDatum propDbd = null;
        try
        {
            propDbd = dbConnection.get_device_property(deviceName, propName);
        }
        catch (DevFailed ex)
        {
            return null;
        }

        String[] propVal = null;
        propVal = propDbd.extractStringArray();
        return propVal;
    }
    
}
