# Changelog
All notable changes to this project will be documented in this file.

The free versioning numbers Major.Minor is used.

The version tags are named TemplateProject-Major.Minor.

The release candidates are suffixed with the string -SNAPSHOT.

## [Unreleased]

# [8.25] - 08-08-2024

### added
* Add reset srds matrix command reset menu (srps)

# [8.24] - 11-06-2024

### added
* Add confirmation popup on all Calibration boutons

# [8.23] - 24-01-2024

### Changed
* fixe error in jdraw template for C3 and C4

# [8.22] - 13-12-2023

### Changed
* support rename of ps-spare to ps-sospare

# [8.21] - 27-11-2023

### Changed
* Confirm Message for commands "Disable" and "Hard Reboot"


## [8.20] - 22-11-2023

### Changed
* in HsmControlPanel, VoidVoidCommandViewer to ConfirmCommandViewer for commands Enable and Disable.
* Confirm Message for command Hard Reboot

## [8.19] - 18-10-2023

### Fixed
* jdraw HSC-template and specific template for C3 and C4
### Added
* About menu

## [8.18] - 12-10-2023

### Fixed
* fix tipo hsm reset confirmation message
* fixed some java17 compliance

## [8.17] - 11-10-2023

### Fixed
* fix tipo hsm reset confirmation message

## [8.16] - 26-09-2023

### Removed
* removed reset command for comecaboxes
### Changed
* changed label in calibration

## [8.15] - 14-09-2023

### Removed
* Init command in HsmControlPanel
### Changed
* Hard Reboot command in HsmControlPanel

## [8.14] - 2023-08-03

### Added
* Add TriggerPosition and decimationRate Attribute for swap current monitoring

## [8.13] - 2023-07-27

### Changed
* Improved axis on viewer "MosPlateDataViewer" now timestamped from HSM

## [1.1] - 201X-XX-XX

### Added
* A new viewer "MosPlateDataViewer" for HSM device server

### Changed
* For changes in existing functionality.

### Deprecated
* For soon-to-be removed features.

### Removed
* For now removed functionality/features/code.

### Fixed
* For any bug fixes.

### Security
* In case of vulnerabilities.




## [1.0] - 2018-08-10

### Added
* First release

