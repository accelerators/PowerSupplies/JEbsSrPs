# JEbsSrPs

The Java Gui application for monitoring the state of EBS Storage Ring Main Power Supplies.
These power supplies are installed in Hot Swap Cubicle and can be controlled by Hot Swap Manager.

## Cloning

```
git clone git@gitlab.esrf.fr:accelerators/PowerSupplies/JEbsSrPs.git
```

## Building and Installation

### Dependencies

* See pom.xml for full list of dependencies

#### Toolchain Dependencies

* Javac 7 or higher
* Maven

### Build

Instructions on building the project.

```bash
cd /segfs/tango/jclient/JEbsSrPs
mvn clean
mvn package
```

### RELEASE

Instructions on making a new release

```bash
mvn release:prepare
mvn release:perform
```

### Installation

### RELEASE

Instructions on making a new release

```bash
mvn release:prepare
mvn release:perform
```

### Installation


To run **(in CTRM)** :

```bash
source /operation/dserver/java/scripts/Common/JavaAppliStartup

APPLI_JAR=$APPLIHOME/JEbsSrPs.jar
APPLI_PACKAGE=jebssrps
APPLI_MAIN_CLASS=yourMainClass

CLASSPATH=$TANGO:$ATK:$ATKPANEL:$APPLI_JAR
export CLASSPATH

$JAVA -DTANGO_HOST=host:port $APPLI_PACKAGE.$APPLI_MAIN_CLASS $*
```

To run **(on rnice if possible)** :

```bash
source /segfs/tango/release/java/scripts/JavaAppliStartup

APPLI_JAR=$TANGO_APPLIHOME/JEbsSrPs.jar
APPLI_PACKAGE=jebssrps
APPLI_MAIN_CLASS=yourMainClass

CLASSPATH=$TANGO:$TANGOATK:$ATKPANEL:$APPLI_JAR
export CLASSPATH

$JAVA -DTANGO_HOST=host:port $APPLI_PACKAGE.$APPLI_MAIN_CLASS $*
```
